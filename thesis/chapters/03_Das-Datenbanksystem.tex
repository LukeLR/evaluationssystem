\section{Das Datenbanksystem}\raggedbottom\label{sec:datenbanksystem}
Kernpunkt der Anwendung ist die Erfassung und Verarbeitung von großen Datenmengen. In einem realistischen Szenario ist daher die Verwendung eines Datenbanksystems unabdingbar, und eine der wichtigsten Entscheidungen bei der Wahl der Technologien. Zu den Anforderungen an die Datenverwaltung zählt neben der Skalierbarkeit, um auch bei großen Datensätzen eine hohe Verarbeitungsgeschwindigkeit gewährleisten zu können, die Flexibilität, um den Anforderungen des Projektes gerecht werden zu können. Diese Voraussetzungen kann nur ein Datenbanksystem erfüllen.

Es existieren verschiedene Modelle für Datenbanksysteme. Die Wahl eines geeigneten Datenbanksystems ist wichtig für die Implementierung des Systems, da unterschiedliche Datenbankmodelle verschiedene Vor- und Nachteile bieten.

\subsection{Relationale Datenbanksysteme}
Obwohl ursprünglich bereits 1970 von Edgar Fred Codd bei IBM entwickelt und vorgestellt, \cite{Codd:1970:RMD:362384.362685} sind relationale Datenbanksysteme bis heute die populärste Technologie zur Datenverwaltung. Eine vom Entwicklerportal StackOverflow jährlich durchgeführte Umfrage unter Entwicklern zeigt, dass auch im Jahr 2018 mit MySQL, SQL Server und PostgreSQL drei Vertreter relationaler Datenbanksysteme die Top 3 der meistbenutzten Datenbanksoftware darstellen. \cite{StackOverflow_DeveloperSurvey:2018} Auch unter den meistgenutzten Programmiersprachen belegt in dieser Studie die Abfragesprache für relationale Datenbanken SQL den vierten Platz und ist damit die meistgenutzte Datenbankprogrammiersprache.

\begin{figure}[H]
\begin{center}
  \includegraphics[width=0.7\textwidth]{img/ER-Modell}
  \caption{ER-Modell zur Modellierung der Anforderungen an das Datenbanksystem}\label{fig:er_modell}
\end{center}
\end{figure}

Um eine Anwendung mittels eines relationalen Datenbanksystems umzusetzen, kann zur Modellierung das ER-Modell herangezogen werden. Dabei werden Objekte der Applikation als Entitäten aufgefasst, und ihre Beziehungen untereinander als Relationen dargestellt. Dieses ER-Modell kann dann anschließend in ein relationales Datenbankschema umgewandelt werden.

\subsubsection{Relationale Modellierung}
Die Berücksichtigung dieser und einiger weiterer Modellierungsentscheidungen bei der Erstellung eines ER-Modells könnten zu einem Ergebnis wie in \autoref{fig:er_modell} dargestellt führen.

\begin{table}[htb]
\begin{center}
\renewcommand{\arraystretch}{1.5}
\begin{tabular}{|p{0.2\textwidth}|p{0.8\textwidth}|}
\hline
\textbf{Relation} & \textbf{Attribute} \\
\hline
\hline
Benutzer & \underline{Benutzer-ID}, Anrede, Sprache, Vorname, Nachname, E-Mail, verifiziert, Passwort \\
\hline
Modul & \underline{Modul-ID}, Name \\
\hline
Veranstaltung & \underline{Veranstaltungs-ID}, \textoverline{Modul-ID}, Name, Typ, Semestertyp, Semesterjahr \\
\hline
Schlüssel & \underline{Schlüssel-ID}, \textoverline{Veranstaltungs-ID}, Schlüssel, Zeitstempel, Ablaufdatum \\
\hline
Fragesatz & \underline{Fragesatz-ID}, \textoverline{Veranstaltungs-ID}, Name \\
\hline
Frage & \underline{Fragen-ID}, \textoverline{Fragesatz-ID}, Titel, Beschreibung, Typ, Daten \\
\hline
Antwort & \underline{Antwort-ID}, \textoverline{Fragen-ID}, Antwort, Schlüssel, Zeitstempel \\
\hline
\hline
Frage-verknüpft-mit-Frage & \underline{\textoverline{Fragen-ID1}, \textoverline{Fragen-ID2}} \\
\hline
Zugriff-auf-Veranstaltung & \underline{\textoverline{Benutzer-ID}, \textoverline{Veranstaltungs-ID}}, Zugriffstyp \\
\hline
Zugriff-auf-Modul & \underline{\textoverline{Benutzer-ID}, \textoverline{Modul-ID}}, Zugriffstyp \\
\hline
Zugriff-auf-Fragesatz & \underline{\textoverline{Benutzer-ID}, \textoverline{Fragesatz-ID}}, Zugriffstyp \\
\hline
Modul-kommt-vor-in-Semester & \underline{\textoverline{Modul-ID}, Semestertyp, Semesterjahr}\\
\hline
\end{tabular}
\caption{Relationales Datenbankschema zum ER-Modell in \autoref{fig:er_modell}}\label{tbl:relationenschema}
\end{center}
\end{table}

Eine mögliche Darstellung eines relationalen Datenbankschemas ist, für jede Relation den Namen und eine Liste aus Attributen anzugeben. Markiert werden dabei Schlüsselattribute, die die Verknüpfungen unter den Relationen bilden: Primärschlüssel unter den Attributen werden beispielsweise unterstrichen, Sekundärschlüssel überstrichen. \autoref{tbl:relationenschema} zeigt, wie ein relationales Datenbanksystem entworfen werden könnte, dass das ER-Modell aus \autoref{fig:er_modell} umsetzt.

\subsection{Nachteile des relationalen Datenbankschemas}
Beim in \autoref{tbl:relationenschema} gezeigten Relationenschema werden 4 Relationen verwendet, um die Verknüpfungen zwischen den Fragen und die Zugriffsverwaltung auf Veranstaltungen, Module und Fragesätze zu verwalten. Eine weitere Relation ist erforderlich, um festzuhalten, in welchen Semestern ein Modul vorkommt. Es genügt nicht, dies implizit durch die Veranstaltungen festzuhalten, die einem Modul zugeordnet sind, da es während der Erstellung von neuen Veranstaltungen auch vorkommen kann, dass ein Modul in einem Semester eingetragen wird, zu dem noch keine Veranstaltungen zugeordnet sind.

Auch ist absehbar, dass die Relation \keyword{Antwort} bereits bei geringer Nutzung des Systems viele Datensätze enthalten wird. Dozierende legen zu der von ihnen verwalteten Lehrveranstaltung einen Fragesatz an, der eine beliebige Anzahl Fragen enthält, die von den Studierenden beantwortet werden. Der Studierende legt also durch seine Antworten einen Antwortsatz an, der in Abhängigkeit von der Anzahl der Fragen im Fragesatz auch eine beliebige Anzahl an Antworten enthält.

Im relationalen Datenbanksystem wäre die Anzahl der Spalten einer Relation für alle Datensätze einheitlich festgelegt, sodass es aufgrund der variablen Anzahl an Fragen nicht möglich ist, einen ganzen Antwortsatz als einen Datensatz abzulegen. Daher wird hier die Verwendung einer Relation erforderlich, die pro Zeile nur eine Antwort einer Frage zuordnet. Dies schränkt jedoch die Übersichtlichkeit und Performanz ein, da für die Rückmeldung jedes Studierenden mehrere Datensätze in der Relation der Antworten abgelegt werden müssten. Das starre Schema der relationalen Datenbank macht es unmöglich, unterschiedliche Spaltenanzahlen pro Datensatz zu definieren.

Die einzige Möglichkeit, die Zahl der Datensätze hier zu reduzieren, wäre die Definition der Relation mit einer größeren Anzahl an Spalten. Dies würde aber bei Datensätzen mit weniger Antworten als der festgelegten Spaltenzahl zu unnötigen Leerwerten im Datensatz führen, und Antwortsätze, die aus mehr Antworten bestehen, müssten nach wie vor ohne semantischen Bezug auf mehrere Datensätze aufgeteilt werden.

Auch wäre ein Ansatz denkbar, bei dem für jeden Fragesatz eine eigene Relation mit der korrekten Anzahl Spalten für die Anzahl Fragen im Fragesatz angelegt werden würde. Da aber jeder Lehrveranstaltung mehrere Fragesätze zugeordnet werden sollen, und das System eine Vielzahl an Lehrveranstaltungen verwalten soll, würde dies zu einer unübersichtlichen Menge an Relationen führen. Beide Ansätze bieten keine zufriedenstellende Lösung für das Problem.

\subsection{Dokumentbasierte Datenbanksysteme}
Die Verwaltung von Datensätzen in Relationen ermöglicht zwar eine weniger fehleranfällige Verarbeitung und stellt eine klare Struktur der Daten sicher, schränkt aber auch die Flexibilität in der Verwendung des Datenbanksystems ein. Diese Einschränkung versuchen nichtrelationale Datenbanksysteme, oft bezeichnet als strukturierte Datenspeicher (\glqq structured storage\grqq) \cite{Lakshman:2010:CDS:1773912.1773922} oder \glqq NoSQL\grqq\ als Akronym für \glqq Not only SQL\grqq\ (\glqq Nicht nur SQL\grqq) \cite{Edlich2011_intro} zu adressieren.

Grundlegend ist dabei der Verzicht auf festgelegte Tabellenschemata zugunsten von horizontaler Skalierung. Das heißt, anstelle -- wie im relationalen Datenbankmodell erforderlich -- die Attribute als Spalten einer Relation für alle Datensätze einheitlich festzulegen, definiert in nichtrelationalen Datenbankmodellen jeder Datensatz die verwendeten Attribute individuell.

Vertreter der dokumentbasierten Datenbanksysteme sind beispielsweise Graphdatenbanken, die Daten in Knoten ablegen und Verknüpfungen untereinander durch Kanten darstellen \cite{Edlich2011_graph}, Objektbasierte Datenbanken, die die Konzepte der Objektorientierung für Datenbankmodelle umsetzen \cite{Meier2003}, oder auch die dokumentbasierten Datenbanken. Diese zeichnen sich dadurch aus, dass Daten in Dokumenten strukturiert und in Sammlungen abgelegt werden. \cite{Trelle2014_intro}

Bei Verwendung von mehreren Sammlungen (englisch \glqq Collections\grqq) können dann, ähnlich wie durch Relationen beim relationalen Datenbankmodell, gleichartige Datensätze gruppiert werden. Anstelle von Spalten bestimmen die Attribute der einzelnen Dokumente, welche Daten in den Dokumenten gefunden werden können.

Dadurch ist es nicht nur möglich, die Beschränkung eines Datensatzes auf eine einheitliche Attributliste aufzuheben, auch die Atomarität der einzelnen Attribute wird aufgehoben. Mehrere Werte können z.B. durch Arrays innerhalb eines Attributes unstrukturiert verwaltet werden. Es ist aber genauso möglich, einem Attribut ein eigenes Dokument zuzuweisen, wodurch Attribut-Wert-Kombinationen über mehrere Ebenen umsetzbar werden.

Gleichzeitig erlaubt ein dokumentbasiertes Datenbanksystem auch, die Daten über mehrere Ebenen zu strukturieren. Während bei einem relationalen Datenbanksystem die tiefere Struktur der Daten durch mehrere Relationen umgesetzt wird, die durch Schlüsselattribute miteinander verknüpft werden, ermöglicht eine dokumentbasierte Struktur die Definition von mehreren Ebenen innerhalb eines Dokuments.

\subsubsection{Vorteile der dokumentbasierten Modellierung}
Diese Eigenschaften eines dokumentbasierten Datenbankmodells können auch in diesem Anwendungskontext als Vorteile zum Tragen kommen. Beispielsweise lässt sich die Zugriffsverwaltung auf Module, Lehrveranstaltungen und Fragesätze ohne zusätzliche Collections realisieren.

Stattdessen werden die Benutzer, die Lese- oder Schreibzugriff auf einen Datensatz haben, direkt als arraywertiges Attribut in demselben abgelegt. Konkret bedeutet dies, dass die Dokumente zu Modulen, Lehrveranstaltungen und Fragesätze jeweils um zwei Attribute \keyword{readAccess} und \keyword{writeAccess} erweitert werden. Darin wird jeweils eine Liste der Benutzer-IDs abgelegt, die die jeweilige Zugriffserlaubnis auf das Objekt haben.

Gleiches gilt für die Verknüpfung von Fragen untereinander. Hier wird ebenfalls keine zusätzliche Collection benötigt, sondern dem Datensatz der Frage ein Array hinzugefügt. Dieses enthält dann eine Liste mit IDs aus mit dieser Frage verknüpften Fragen.

Neben der Einsparung zusätzlicher Relationen oder Collections stellt dies auch einen Vorteil bei der Datenbankabfrage dar. So kann beim Aufrufen der Datensätze die Zugriffsberechtigung direkt überprüft werden, ohne wie im relationalen Datenbankmodell eine weitere Relation abfragen zu müssen. Auch kann direkt nach Datensätzen gesucht werden, auf die ein Nutzer Zugriff hat, ohne dafür die Relation der Datensätze mit der der Zugriffsverwaltung vereinigen zu müssen. Die verknüpften Fragen werden ebenfalls beim Abfragen des Fragedatensatzes mit zurückgeliefert, ohne weitere Datenbankabfragen durchführen zu müssen.

Da das dokumentbasierte Datenbankmodell ebenfalls erlaubt, tiefere Strukturen durch untergeordnete Dokumente zu realisieren, kann die Relation, welche die Zuordnung von Modulen zu Semestern nachhält, auf eine ähnliche Weise ersetzt werden. Obwohl zwei Informationen zur Zuordnung des Moduls zu einem Semester erforderlich sind (Typ des Semesters, z.B. Sommersemester, und Jahr), können diese zu einem Subdokument zusammengefasst werden. Der Datensatz des Moduls erhält dann ein weiteres Attribut, das in einem Array eine Liste aus Dokumenten enthält, die jeweils ein Semester beschreiben, in dem das Modul vorkommt.

Zur Beschreibung eines Dokuments im dokumentbasierten Datenbanksystem ist JSON als Datenformat üblich. In JSON werden Dokumente mit geschweiften Klammern eingeschlossen und Attribute mit Kommata aufgelistet. Der Wert eines jeweiligen Attributes folgt nach einem Doppelpunkt auf die Bezeichnung desselben, und kann neben einem atomaren Wert auch ein durch eckige Klammern gekennzeichnetes Array oder ein eigenes Dokument sein. Auch Objekte sind möglich, beispielsweise stellt \inlinecode{ObjectId("...")} den MongoDB-eigenen Primärschlüssel dar. Dieser ist eine 12-Byte-Hexadezimalzahl, die normalerweise in den Klammern angegeben wird, und hier nur aus Gründen der Übersichtlichkeit ausgelassen wurde. \autoref{lst:module-json} zeigt die Anwendung dieser Vorteile des dokumentbasierten Datenbankmodells am Beispiel eines Moduldatensatzes.

\begin{listing}[htb]
\begin{center}
\begin{mintdefault}
{
    "_id" : ObjectId("..."),
    "name" : "Beispielmodul",
    "readAccess" : [
        ObjectId("...")
    ],
    "writeAccess" : [
        ObjectId("...")
    ],
    "semesters" : [
        {
            "type" : "Summer",
            "year" : "2019"
        },
        {
            "type" : "Winter",
            "year" : "2019"
        }
    ]
}
\end{mintdefault}
\end{center}
\caption{Beispiel eines Moduldatensatzes in JSON}\label{lst:module-json}
\end{listing}   

Ebenfalls kann durch die Verwendung eines dokumentbasierten Datenbankmodells die Zahl der Datensätze zur Verwaltung der Antworten reduziert werden. Während im relationalen Datenbankmodell nur die Antwort auf eine Frage in einem Datensatz abgelegt werden kann, ermöglicht die horizontale Skalierbarkeit des dokumentbasierten Datenbanksystems, dass Antworten auf beliebig viele Fragen in einem Datensatz hinterlegt werden können.

So ist nur ein Datensatz erforderlich, um die Antworten eines Studenten auf einen gesamten Fragesatz zu speichern, unabhängig davon, aus wie vielen Fragen der Fragesatz besteht, oder welchen Typ die Fragen haben. Auch wenn die Skalierbarkeit moderner relationaler Datenbanksysteme sicherlich auch die Verwaltung von einem Datensatz pro Antwort ermöglichen würde, stellt der Bedarf von nur einem Datensatz für einen gesamten Antwortsatz einen Vorteil dar.

Die Vorteile des dokumentbasierten Datenbankmodells sind somit zwar in vielen Fällen hilfreich, sollten allerdings nicht unreflektiert eingesetzt werden. Manchmal ist auch nach wie vor eine dem relationalen Schema ähnliche Modellierung sinnvoller.

\subsection{MongoDB als Datenbankmanagementsystem}
Aus diesen Gründen fiel bei der Wahl eines Datenbankmodells die Entscheidung auf das dokumentbasierte Datenbankmodell. Dabei gibt es eine Vielzahl an Datenbanksystemen, die dieses Modell implementieren. Einige Beispiele sind Apache Jackrabbit, BaseX, CouchDB oder auch MongoDB.

MongoDB sticht dabei unter den Datenbanksystemen für das dokumentbasierte Datenbankmodell heraus. Mit einer Erstveröffentlichung aus 2009 \cite{dbengines_MongoDB} ist diese zwar eine recht junge Datenbanksoftware, die aber dennoch bereits jetzt eine breite Akzeptanz gefunden hat. So nimmt MongoDB in der 2018 durchgeführten Studie des Entwicklerportals Stack Overflow den vierten Platz in den meistgenutzten Datenbanksystemen ein. \cite{StackOverflow_DeveloperSurvey:2018} Auch im Vorjahr belegt MongoDB von derselben Studie bereits den fünften Platz. \cite{StackOverflow_DeveloperSurvey:2017} Gleichzeitig belegt MongoDB in beiden Jahren den ersten Platz unter den meistgewünschten Datenbanksystemen. Dies bezieht sich auf die Zahl der befragten Entwickler, die noch nicht mit der Technologie gearbeitet haben, es aber gerne würden. \cite{StackOverflow_DeveloperSurvey:2018}

Das Datenbanksystem MongoDB erfreut sich also großer Beliebtheit. Dies gibt einen guten Anhaltspunkt dazu, dass die Software trotz ihrer Modernität bereits genug Stabilität und Ausgereiftheit erreicht hat, um in produktiven Systemen eingesetzt zu werden. Auch wenn dieses Datenbanksystem und die dadurch definierte Abfragesprache noch weiterentwickelt wird, garantiert die breite Verwendung etablierte Schnittstellen, was eine praktikable und zukunftssichere Verwendung möglich macht. Dadurch kann von den Vorteilen des dokumentbasierten Datenbanksystems profitiert werden, ohne auf die Erprobtheit unter realen Umständen verzichten zu müssen, die etablierte Systeme relationaler Datenbanksysteme bieten würden.

\subsubsection{Einfache Datenbankabfragen}
Um auf die in der MongoDB abgelegten Datensätze zuzugreifen, wird eine JavaScript-ähnliche Abfragesprache verwendet. Anders als bei SQL kommen hier Objektorientierung und Funktionen zum Einsatz.

Eine normale Datenbankabfrage wird mit der \inlinecode{find()}-Methode auf dem Objekt der \term{collection} ausgeführt. Analog zu SQL's \inlinecode{WHERE}-Klausel können hierbei Suchkriterien und Projektionen übergeben werden, wobei beide als Dokumente mit Attributen und Werten formuliert werden. Sind die Werte des Suchkriteriums beispielsweise atomar, so wird auf Gleichheit überprüft, es können aber auch Operatoren als Werte übergeben werden.

\begin{listing}[htb]
\begin{center}
\begin{mintdefault}
> db.questions.find({title: "Testfrage 1"}, {description: 1})
    { "_id" : ObjectId("..."), "description" : "Beschreibung 1" }
    ...

> db.modules.find(
    {readAccess: {$size: 1}},
    {_id: 0, name: 0, writeAccess: 0, semesters: {$slice: -1}}).pretty()
	
    {
        "readAccess" : [
            ObjectId("...")
        ],
        "semesters" : [{
            "type" : "Winter",
            "year" : 2019
        }]
    }
    ...
\end{mintdefault}
\caption{Zwei Beispiele zur Demonstration der Datenbankabfrage}\label{lst:find_examples}
\end{center}
\end{listing}

\autoref{lst:find_examples} zeigt exemplarisch zwei Datenbankabfragen. Auch hier wurden die Primärschlüssel aus Gründen der Übersichtlichkeit ausgelassen. Erstere durchsucht die Sammlung der Fragen nach denen, deren Titel \glqq Testfrage 1\grqq\ lautet. Zurückgegeben werden aber nicht die gesamten Dokumente, sondern nur der Primärschlüssel \inlinecode{_id} und die Beschreibung, andere Attribute werden gefiltert. MongoDB versieht, sofern nicht anders angegeben, jedes Dokument mit einem datenbankweit eindeutigen Primärschlüssel, die auch immer mit ausgegeben werden, und explizit ausgeschlossen werden müssen. In der zweiten Abfrage wird die Verwendung von Operatoren demonstriert. Es wird nach Modulen gesucht, zu denen genau ein Nutzer Lesezugriff hat. Ausgegeben werden nur der Lesezugriff und das letzte Semester, in dem das Modul vorkommt.

Interessant wird besonders das Verhalten bei der Suche nach nicht-atomaren Attributen, beispielsweise Arrays. Hier wird standardmäßig nicht auf Gleichheit überprüft, sondern die Angabe eines Wertes als Suchkriterium entspricht dem Vorkommen des Suchkriteriums im Array. Anwendung findet dies zum Beispiel beim Filtern der Module nach Lesezugriff, so lassen sich mit
\betweenlinecode{db.questionsets.find({readAccess: ObjectId("...")})}
alle Dokumente finden, in denen die entsprechende Nutzerkennung im Array \inlinecode{readAccess} \textit{vorkommt}.

So kann ohne komplizierte Anweisungen direkt nach Dokumenten gefiltert werden, in denen ein Wert in einem Array enthalten ist. Das Standardverhalten der Abfragesprache genügt hier. Ähnlich wird mit Einfüge-, Aktualisierungs- und Lösch-Operationen verfahren.

\subsubsection{Aggregatfunktionen in MongoDB}
\begin{listing}[htb!]
\begin{center}
\mintdefaultinput[linenos]{img/groupAnswersetsByDate-aggregate.js}
\caption{Beispiel einer komplexen Datenbankabfrage durch Aggregatfunktionen}\label{lst:aggregate_example}
\end{center}
\end{listing}

Eine wichtige Eigenschaft von MongoDB ist aber die umfangreiche Programmierung durch die sogenannte Aggregatfunktion. Neben der Datenbankabfrage mittels der Methode \inlinecode{find()} kann dies auch durch die Methode \inlinecode{aggregate()} erfolgen. Diese ermöglicht darüber hinaus weitaus komplexere Abfragen, und bietet ähnliche Funktionen, wie auch die relationale Abfragesprache SQL.

Statt nur Suchkriterien und Projektionen als Parameter zu akzeptieren, nimmt die Aggregatfunktion eine Anweisungsabfolge (engl. \glqq Pipeline\grqq) entgegen. Diese setzt sich aus einer Abfolge von einzelnen Schritten (engl. \glqq Stages\grqq) zusammen, die die Dokumente der Abfrage auf unterschiedliche Weise modifizieren. Einheitlich ist aber, dass am Anfang der Pipeline alle Dokumente der Sammlung stehen, und jeder Schritt die Ausgabe des vorhergehenden Schrittes als Eingabe entgegennimmt. Einzelne Schritte können dann zum Beispiel das Filtern der Datensätze nach bestimmten Kriterien (ähnlich der \inlinecode{find()}-Methode), das Anreichern der Datensätze um Informationen aus anderen Sammlungen (analog zur \inlinecode{LEFT JOIN}-Anweisung aus SQL) oder das Gruppieren mehrerer Datensätze anhand bestimmter Kriterien sein.

Insbesondere letzteres stellt wichtige Funktionalitäten bereit, da durch das Gruppieren nicht nur ganze Dokumente in Arrays zusammengefasst werden können, sondern auch arithmetische Funktionen wie das Aufsummieren von Attributen oder das Zählen von Datensätzen durch diese Funktion ermöglicht werden. \autoref{lst:aggregate_example} zeigt ein Beispiel, wie die Funktion verschiedener Aggregatschritte kombiniert werden kann, um die Informationen bereits in der Datenbank aufzubereiten.

Dabei filtert der \inlinecode{$match}-Operator zunächst alle Veranstaltungen nach der korrekten ID und stellt Lesezugriff sicher. Die \inlinecode{$lookup}-Operation ergänzt die gefundene Veranstaltung um ein Array \inlinecode{questionsets}, welches die Dokumente aller Fragesätze enthält, deren Attribut \inlinecode{courseID} mit der ID der aktuellen Veranstaltung übereinstimmt. Anschließend wird dieses Array mittels \inlinecode{$unwind} aufgespalten. Diese Operation dupliziert die Veranstaltung, und ordnet jedem Duplikat je einen Fragesatz zu. %$

Dies ist erforderlich, um erneut mittels einer Kombination aus \inlinecode{$lookup} und \inlinecode{$unwind} zu jedem Fragesatz die abgegebenen Antwortsätze zuzuordnen. Am Ende von Zeile 19 stehen somit für jeden Antwortsatz ein Dokument in der Pipeline, dass zusätzlich zur Information des Antwortsatzes auch die Veranstaltungs- und Fragesatz-Informationen enthält.

Der Zeitstempel jedes Antwortsatzes wird nun in seine Bestandteile aufgespalten, um anschließend nach Tag, Monat und Jahr gruppieren zu können. Hierbei wird die Zahl der Dokumente auf je eines pro Datum reduziert, zu dem eine Antwort abgegeben wurde. Dieses trägt als einzige Information nur noch die Anzahl der Antworten, die an diesem Tag abgegeben wurden. Daraufhin kann anschließend erneut nach Monat und Jahr gruppiert werden, sodass ein Datensatz pro Monat entsteht, in dem Antworten abgegeben wurden. Dabei wird die Information, wie viele Antworten an jedem Tag des jeweiligen Monats abgegeben wurden, in einem Array \inlinecode{days} gesammelt. Dazu war zuvor nur das Umbenennen zweier Felder erforderlich.

So ermöglicht die Verwendung von Aggregatfunktionen analog zur Abfragesprache SQL das Zusammenfassen und Anreichern von Informationen, schon bevor diese an das System weitergegeben werden. Durch das Verknüpfen von Sammlungen innerhalb einer Datenbankabfrage können mehrere Abfragen zu einer zusammengefasst werden, was auch die Last für das Datenbanksystem verringert.