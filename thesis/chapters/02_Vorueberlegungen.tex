\section{Vorüberlegungen}\raggedbottom\label{sec:vorueberlegungen}
Die Entwicklung einer solchen Webanwendung ist umfangreich und bedarf daher einiger Vorüberlegungen. So soll neben der Funktionalität auch ein Augenmerk auf die einfache und effektive Verwendung der Software gelegt werden.

Um diese Ziele sachlich und objektiv betrachten zu können, gibt die Internationale Standardorganisation unter der Nummer ISO 9241 eine Norm \glqq Ergonomics of human-system interaction\grqq\ heraus, welche Leitfäden zur Mensch-Computer-Interaktion definiert. Um diese bei der Entwicklung des Systems zur Anwendung bringen zu können, ist es sinnvoll, einige der in dieser Norm angebrachten Definitionen herauszustellen und im Rahmen der Vorüberlegungen auszuwerten.

Die ISO-Norm 9241-210, vom Deutschen Institut für Normung adaptiert als DIN-EN ISO 9241-210, behandelt dabei den \glqq Prozess zur Gestaltung gebrauchstauglicher interaktiver Systeme\grqq. Darin wird der Begriff der Gebrauchstauglichkeit definiert als das \glqq Ausmaß, in dem ein System, ein Produkt oder eine Dienstleistung durch bestimmte Nutzer in einem bestimmten Nutzungskontext genutzt werden kann, um festgelegte Ziele effektiv, effizient und zufriedenstellend zu erreichen\grqq\ \cite{DIN_EN_ISO_9241-210:2011-01}. Der Nutzungskontext wird wiederum definiert als \glqq Benutzer, Arbeitsaufgaben, Ausrüstung (Hardware, Software und Materialien) sowie die physische und soziale Umgebung, in der das Produkt genutzt wird\grqq.

Diese Definitionen zeigen bereits auf, dass es für eine Entwicklung mit dem Ziel einer hohen Gebrauchstauglichkeit erforderlich ist, während der gesamten Entwicklungsphase die Anwendersituation zu berücksichtigen. Es wird von \glqq bestimmten Nutzern\grqq, einem \glqq bestimmten Nutzungskontext\grqq\ und von \glqq festgelegten Zielen\grqq\ gesprochen.

\subsection{Die Anwendersituation}
Eine Notwendigkeit zur Optimierung der Anwendung hinsichtlich der Gebrauchstauglichkeit für alle Benutzergruppen ist es, bereits vor der eigentlichen Entwicklung des Systems diese einzugrenzen und festzulegen.

Kernpunkt ist dabei, zu analysieren, wie die Software von der jeweiligen Benutzergruppe verwendet wird. Dazu gehört, welche Funktionen benötigt werden, und welche Anforderungen und Erwartungen dabei bestehen. Gleichzeitig muss auch mit einbezogen werden, welches Interesse seitens der Nutzergruppe besteht, die Anwendung einzusetzen. Im Wesentlichen lassen sich die Nutzer des Systems in diesem Fall in zwei Gruppen zusammenfassen.

\subsubsection{Das Lehrpersonal}
Die erste Gruppe ist dabei das Lehrpersonal der Universität, der Fakultät oder des Lehrstuhls, das die Anwendung einsetzt. Diese Personengruppe nutzt die Software zur Überprüfung und Sicherstellung der Lehrqualität.

Funktionen der Software, die dabei zum Einsatz kommen, sind das Anlegen und Verwalten von Vorlesungen und Übungsgruppen im System, das Erstellen von Fragesätzen zu Vorlesungen und Übungen, das Verteilen der Fragesätze an die Studierenden der Vorlesungen und Kurse, sowie zuletzt die Evaluation der daraus generierten Daten. Da der Einsatz der Anwendung dem Vorteil dieser Benutzergruppe dient, sollte im Allgemeinen ein Interesse an der Benutzung des Programmes existieren.

Dennoch soll die Nutzung der Anwendung auch im Sinne der Gebrauchstauglichkeit effektiv und effizient erfolgen. Die DIN EN ISO 9241-11 bezeichnet Effektivität als die \glqq Genauigkeit und Vollständigkeit, mit der Benutzer ein bestimmtes Ziel erreichen\grqq, während die Effizienz die Relation aus letzterer zum dafür eingesetzten Aufwand darstellt. \cite{DIN_EN_ISO_9241-210:2011-01}

Es spielt also im Sinne der Effizienz, und damit für die Gebrauchstauglichkeit, nicht nur die Tatsache, ob das Ziel vollständig erreicht werden kann, sondern auch der dazu erforderliche Aufwand eine Rolle. Um diesen möglichst gering zu halten, müssen die o.g. Funktionen und Anforderungen während des gesamten Entwicklungsprozesses berücksichtigt werden. Nur, wenn bei der Entwicklung der Anwendung berücksichtigt wird, wie der konkrete Anwendungsfall gestaltet ist, das heißt beispielsweise, in welcher Reihenfolge Funktionen angewendet werden oder welche häufiger genutzt werden als andere, können diese so in der Benutzeroberfläche platziert und strukturiert werden, dass die Verwendung für den Nutzer intuitiv erscheint.

Neben der Reduzierung des Aufwandes beim Anwenden des Systems verringert eine solche Strukturierung auch den Einarbeitungsaufwand in die Software. Ist die Verwendung der Anwendung intuitiv, sinkt der Zeitaufwand, der vor der eigentlichen Verwendung beispielsweise in das Lesen einer Dokumentation oder dem Nachschlagen von Anwendungshinweisen investiert werden muss. Dies steigert zusätzlich die Effizienz, und ist auch ein Ziel dieser Anwendungsentwicklung.

Auch die Übersichtlichkeit ist wichtiger Bestandteil zum effektiven Einsatz des Systems. Hierbei muss berücksichtigt werden, an welchen Stellen der Benutzeroberfläche mit welchen Informationsmengen umgegangen werden muss und wie diese gestaltet sind. Wichtige Überlegungen dabei sind, welche Informationen zum jeweiligen Zeitpunkt im Mittelpunkt stehen, welche Informationen zusammengefasst werden können, oder welche Gliederung zur Strukturierung der Informationen angemessen ist.

\subsubsection{Die Studierenden}
Die Studierenden bilden die zweite Nutzergruppe der Anwendung. Das Anwendungsszenario unterscheidet sich hierbei komplett von dem des Lehrpersonals. Diese nutzen die Software lediglich zum Ausfüllen der zugesandten Fragebögen. Daraus folgt, dass die Studierenden im Allgemeinen kein eigenes Interesse an der Nutzung der Software haben, sondern dies nur zu Gunsten der anderen Nutzergruppe tun.

Das Ausfüllen der Fragebögen stellt für die Studierenden einen Mehraufwand dar, der ihnen keinen direkten Vorteil bietet. Somit ist es insbesondere bei den von den Studierenden verwendeten Funktionen wichtig, die Verwendung der Applikation so einfach wie möglich zu gestalten. Dadurch kann die Hemmschwelle so weit wie möglich verringert werden, um möglichst viele Antworten zu erhalten. Ist das Beantworten der Fragen zu kompliziert, zu aufwändig, zu unübersichtlich oder der Zeitaufwand zu groß, würden nur wenige Studierende die Fragebögen ausfüllen. Dies erschwert die Evaluation und die daraus gewonnenen Erkenntnisse wären nicht repräsentativ.

\subsection{Modellierung}\label{subsec:modellierung}
Eine der Hauptentscheidungen beim Entwurf der Software ist die Überlegung, wie Vorlesungen und Kurse im System modelliert und strukturiert werden. Das Verwalten dieser Datensätze ist eine der Kernaufgaben des Systems und muss daher für den Benutzer intuitiv und übersichtlich sein.

\subsubsection*{Vorlesungen und Kurse}
Es sollen nicht nur zu Vorlesungen, sondern auch zu Übungsgruppen und Kursen Umfragen erstellt werden können. Gleichzeitig sollen die Fragebögen zu Vorlesungen von denen der Übungsgruppen getrennt werden können. Daher bietet es sich an, im System allgemein Lehrveranstaltungen zu betrachten. Eine Lehrveranstaltung ist entweder eine Vorlesung oder ein Kurs, wobei diese sich, abgesehen von der Bezeichnung, nicht voneinander unterscheiden. Kurse können dann wiederum zum Beispiel Übungsgruppen oder Tutorien sein, diese Kategorien müssen allerdings nicht weiter im Datensatz unterschieden werden. Im vorgesehenen Anwendungsszenario soll zu jedem Modul eine Vorlesung und eine beliebige Anzahl an Kursen angelegt werden können.

\subsubsection*{Fragen und Fragesätze}
Allen Veranstaltungen, sowohl Vorlesungen als auch Kursen, können anschließend Fragen zugeordnet werden, die von den Studierenden zu beantworten sind. Dabei ist das Ziel, dass alle Studierenden immer alle Fragen beantworten. Um eine Gliederung für die größere Menge an Fragen zu einer Veranstaltung zu schaffen, werden alle Fragen zunächst genau einem Fragesatz zugeordnet, der wiederum genau einer Veranstaltung zugeordnet ist.

\subsubsection*{Fragentypen}
Für die Evaluation sind Fragen verschiedenen Typs von Interesse. In manchen Fällen ist es wünschenswert, den Studierenden die Möglichkeit zu geben, beim Bearbeiten der Fragebögen ihr Feedback im Freitext abzugeben. Dadurch wird Raum für Lob oder Kritik geschaffen. Gleichzeitig bieten aber Fragetypen, die ein wertdiskretes Feedback erfordern, wie zum Beispiel eine Skala oder eine Multiple-Choice-Frage, bessere Evaluierungsmöglichkeiten. Diese erlauben, einen weitreichenderen Überblick über die Rückmeldungen zu bekommen und eine Übersicht über alle Antworten -- beispielsweise in einem Diagramm -- auf einen Blick darzustellen.

Daher bietet die Anwendung die Möglichkeit, Fragen verschiedenen Typs zu erstellen. Eine sinnvolle Auswahl hierbei erscheint einzeiliger und mehrzeiliger Freitext, Multiple-Choice-Fragen mit einer oder mit mehreren Antwortmöglichkeiten und Schieberegler zu sein.

\subsubsection*{Module}
Um Vorlesungen und eine Vielzahl an Kursen aber dennoch gemeinsam verwalten zu können, ist es sinnvoll, auch einen Datensatz für das Modul anzulegen, dem diese zugeordnet werden können. Beispielsweise wird so ein Modul \glqq Datenbanksysteme\grqq\ angelegt, dem dann sowohl die gleichnamige Vorlesung als auch alle Übungsgruppen zugeordnet werden können. Dies schafft Übersicht, da sowohl Vorlesung als auch Übungsgruppen, die zum selben Modul gehören, in einer Ansicht dargestellt werden können.

\subsubsection*{Unabhängige Auswertung von Übungsgruppen}
Eine weitere Anforderung war es, die Veranstaltungen, insbesondere Kurse, unabhängig voneinander auswerten zu können, um zum Beispiel auch Probleme in einzelnen Übungsgruppen ermitteln zu können. Wenn das Feedback von Studierenden einer Übungsgruppe überdurchschnittlich schlecht ist, würde dies möglicherweise in einer Auswertung über alle Übungsgruppen nicht auffallen. Können allerdings die Übungsgruppen einzeln ausgewertet werden, so würde ersichtlich, dass das Feedback einer Übungsgruppe deutlich von dem der anderen Übungsgruppen abweicht.

Dazu bietet es sich an, die Übungsgruppen im System, ähnlich der Realität, getrennt zu behandeln. So kann über die bereits bestehende Möglichkeit, einem Modul mehrere Veranstaltungen zuzuordnen, zwischen den Übungsgruppen unterschieden werden, ohne eine weitere Strukturebene einzuführen. Die Übungsgruppen können dann beispielsweise mit denselben Fragen versehen werden. Es ist aber genauso möglich, einzelne Fragen in den Übungsgruppen zu verändern, oder Fragen zu entfernen und zu ergänzen. Konkret bedeutet dies, gäbe es zum Beispiel zur Vorlesung \glqq Datenbanksysteme\grqq in einem Semester elf Übungsgruppen, würden dem gleichnamigen Modul für dieses Semester zwölf Veranstaltungen zugeordnet, eine Vorlesung und elf Übungsgruppen.

\subsubsection*{Semesterübergreifende Evaluation}
Neben der direkten Auswertung der Rückmeldungen im laufenden Semester ist ein weiteres Ziel der Anwendung, eine vergleichende Auswertung über mehrere Semester hinweg zu ermöglichen. Daher ist es erforderlich, die Datensätze verschiedener Vorlesungen und Übungsgruppen so zu verknüpfen, dass diese in der Auswertung zueinander in Verbindung gebracht werden können. Dies geschieht ebenfalls über den Datensatz des Moduls.

Während eine konkrete Veranstaltung, also zum Beispiel eine Vorlesung oder eine Übungsgruppe, immer nur in einem Semester platziert sein kann, ist das Modul semesterübergreifend. Dies bedeutet, dass das Modul der Vorlesung Einträge zu allen Semestern hat, in denen die Vorlesung angeboten wurde. Für jedes der Semester enthält das Modul dann eine Instanz der entsprechenden Vorlesung, sowie eventuell weitere Veranstaltungen für beispielsweise Übungsgruppen.

\subsubsection*{Verknüpfung von Fragen}
Um ein Modul semesterübergreifend zu evaluieren, ist es darüber hinaus erforderlich, die Antworten auf einzelne Fragen zueinander in Bezug zu setzen. Gab es beispielsweise im vergangenen Semester eine Skala, auf der die Studierenden die Struktur der Vorlesung bewerten sollten, so ist es interessant, die Werte des vorangegangenen Semesters mit denen des aktuellen Semesters zu vergleichen, oder statistische Auswertungen zu ermöglichen.

Da aber der Fragebogen einer Vorlesung nicht zwingend über mehrere Semester hinweg gleich bleibt, sondern eventuell Fragen ergänzt, entfernt oder verändert werden, genügt eine automatische Verknüpfung der Fragen über die Fragesätze nicht. Daher ist es erforderlich, dass Fragen individuell und auch manuell miteinander verknüpft werden können. So kann die Bewertungsskala zur Vorlesungsstruktur in einem anderen Fragesatz der Vorlesung auftauchen, anders positioniert oder umbenannt werden, ohne die Zuordnung zur selben Frage aus dem Vorsemester zu verhindern. Einander zugeordnete Fragen werden dann in der Auswertung miteinander in Verbindung gebracht.

\subsubsection*{Benutzerverwaltung}
Um den Zugang zum System und damit die Einsicht in die Ergebnisse und Modifikation der Datensätze vor Unbefugten zu schützen, ist es erforderlich, eine Zugangsverwaltung für Benutzer zu implementieren. Dazu werden im System die Zugänge über Benutzerkonten erteilt, die in der Anwendung erstellt werden können.

Wichtig ist auch, den Zugang zu den einzelnen Modulen und Veranstaltungen untereinander abzugrenzen. Nicht jeder Nutzer soll Zugang zu allen Datensätzen haben. Gleichzeitig soll es aber auch in einer zukünftigen Version der Anwendung möglich sein, den Zugang zu einigen Datensätzen mit mehreren Anwendern zu teilen. Dies ist wichtig, wenn der Datensatz nicht nur von Dozenten selbst, sondern auch von Mitarbeitern des Lehrstuhls verwaltet, oder Kursleitern ebenfalls Zugang zur Auswertung ihrer Übungsgruppen gegeben werden soll.

Um diese Anforderung zu erfüllen, wird im System jedem Datensatz individuell zugeordnet, welche Nutzer Zugriff auf diesen Datensatz haben. So ist ein Szenario denkbar, in dem der Dozent auch seinen Mitarbeitern oder Übungsgruppenleitern Zugang zu der Verwaltung des Moduls oder den Ergebnissen der Evaluation erteilen kann. Damit aber ebenfalls möglich ist, dass beispielsweise Übungsgruppenleiter ihre Auswertung einsehen, aber die Datensätze nicht bearbeiten können, wird im System zwischen Lese- und Schreibzugriff unterschieden.

\subsubsection*{Versenden der Fragesätze}
Wurden die Vorlesungen und Kurse im System angelegt und mit Fragesätzen und Fragen versehen, sollen diese den Studierenden zur Bearbeitung vorgelegt werden. Es soll aber nur den Studierenden, die tatsächlich eine Veranstaltung belegt haben, die Möglichkeit gegeben werden, diese zu bewerten.

Es muss also auch für die Studierenden eine Möglichkeit bestehen, sich gegenüber dem System zu authentifizieren. Eine denkbare Lösung für diese Anforderung wäre, ebenfalls für die Studierenden Nutzerkonten einzuführen. Dann müssten sich aber die Studierenden vor der Verwendung des Systems registrieren und eventuell E-Mail-Adresse und Passwort vergeben.

Da dies unnötigen Aufwand darstellt, und die Anwendung der Software insbesondere für die Studierenden möglichst einfach zu gestalten ist, werden für Studierende keine Nutzerkonten angelegt. Stattdessen können im System Zugangsschlüssel generiert werden, die genau einer Veranstaltung zugeordnet sind. Diese können den Studierenden entweder direkt im System per E-Mail mitgeteilt werden, oder zur Verteilung über andere Kommunikationswege als Liste heruntergeladen werden. Durch Verwendung in einem speziellen Zugangslink bieten diese Schlüssel den Studierenden dann Zugang zur Beantwortung der Fragen zur Veranstaltung.

\subsection{Wahl der Technologien}
Auch die Technologien, die bei der Software zum Einsatz kommen sollen, müssen sinnvoll gewählt werden. Gerade im Bereich der Webentwicklung ist ein Zusammenspiel aus mehreren Komponenten erforderlich. Der HTML-Code, der von einem Webserver ausgeliefert und von einem Präprozessor manipuliert wird, Stile und Skripte, die vom HTML-Code referenziert und im Client ausgefüllt werden, Bibliotheken für Client und Server, um Funktionen wie Multilingualität oder Diagramme bereitzustellen, müssen gut zusammenpassen.

Letztendlich werden Teile der Anwendung serverseitig, andere Teile clientseitig ausgeführt, und der Browser greift neben den Webseiten auf Schnittstellen zu, die der Server bereitstellt. Daher ist es wichtig, dass alle Teile des Systems zu den Anforderungen passen und gut aufeinander abgestimmt sind.

Aus allen Vorüberlegungen entstehen Anforderungen, deren Erfüllung im Folgenden beschrieben wird.