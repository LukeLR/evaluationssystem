db.courses.aggregate([
    { $match: {
        _id: ObjectId("5c72d70f62be830ea03ccfa2"),
        readAccess: ObjectId("5c31fc4062be8363a67c5ea2")
    }},
    { $lookup: {
        from: 'questionsets',
        localField: '_id',
        foreignField: 'courseID',
        as: 'questionsets'
    }},
    { $unwind: '$questionsets' },
    { $lookup: {
        from: 'answersets',
        localField: 'questionsets._id',
        foreignField: 'questionsetID',
        as: 'answersets'
    }},
    { $unwind: '$answersets' },
    { $addFields: {
        'answersets.timestamp': {
            $dateToParts: { date: '$answersets.timestamp' }
        }
    }},
    { $group: {
        _id: {
            day: '$answersets.timestamp.day',
            month: '$answersets.timestamp.month',
            year: '$answersets.timestamp.year'
        },
        count: { $sum: 1 }
    }},
    { $addFields: {
        'day.day': '$_id.day',
        'day.count': '$count'
    }},
    { $group: {
        _id: {
            month: '$_id.month',
            year: '$_id.year'
        },
        days: { $addToSet: '$day' }
    }}
])
