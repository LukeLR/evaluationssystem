# MongoDB Data Dumps
This folder contains data dumps of the underlying MongoDB Database.

## Restore
Restore a dump to a local running MongoDB instance using
```
mongorestore <path>
```
To restore the `allfeatures`-dump (located in `dump/allfeatures/`) therefore use:
```
mongorestore dump/allfeatures/
```
from the repository as working directory (or with an absolute path from somewhere else).

## Backup
To create a dump of a local running MongoDB instance, use
```
mongodump -d <database> -o <path>
```
For instance, to create a new version of the `allfeatures`-dump, use:
```
mongodump -d eva -o dump/allfeatures/
```
from the repository as the working directory (or with an absolute path from somewhere else).
