<?php
    namespace eva;

    require_once __DIR__ . "/../vendor/autoload.php";
    
    use PHPUnit\Framework\TestCase;
    use phpmock\phpunit\PHPMock;

    class AccountTest extends TestCase {
        use PHPMock;
        
        public function testCreateAccount(){
            $_POST["signupTitle"] = "Mr.";
            $_POST["signupLanguage"] = "en";
            $_POST["signupFirstName"] = "Firstname";
            $_POST["signupLastName"] = "Lastname";
            $_POST["signupEmail"] = "user@domain.de";
            $_POST["signupPassword"] = "Passwort123";

            require_once 'shared/php/handlers/handleSignUp.php';
            $sendVerificationMail = $this->getFunctionMock(__NAMESPACE__, "sendVerificationMail");
            $sendVerificationMail->expects($this->once());

            $validateSignUpData = $this->getFunctionMock(__NAMESPACE__, "validateSignUpData");
            $validateSignUpData->expects($this->once())->willReturn(true);
            
            handleSignUp();
        }
    }
?>
