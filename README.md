# Evaluationssystem

This is the bachelor thesis of Lukas Rose. The software is contained in the root of this repository, while the thesis itself resides under `/thesis/`. The presentation is located in `/thesis/presentation/`. 

## Installation

The only requirements are PHP and MongoDB. It is tested with Apache, though. Any PHP / Apache-Setup should be enough, but for larger installations, using php-fpm or similiar frameworks is recommended.

PHP Libraries are managed via `composer`, Javascript Libraries are managed by `npm`.

### PHP Setup

In order to use MongoDB from PHP, the MongoDB PHP Library and the MongoDB PHP Driver are required. Install the MongoDB PHP Driver first, preferred: use your distribution's package manager, e.g. the [Arch Linux Package](https://www.archlinux.org/packages/community/x86_64/php-mongodb/) or use PHP-Pear:

```
$ sudo pecl install mongodb
```

And add the extension to your `php.ini`-file:

```
extension=mongodb.so
```

The MongoDB PHP Library will be automatically installed by composer, just type `composer install` in the application folder:

```
$ composer install
Loading composer repositories with package information
Installing dependencies (including require-dev) from lock file
Package operations: 2 installs, 0 updates, 0 removals
  - Installing mongodb/mongodb (1.4.2): Downloading (100%)         
  - Installing rose/i18next-php (v0.1.1): Cloning 9b84b893e4 from cache
Generating autoload files
```

### Javascript requirements

