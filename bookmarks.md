# Bookmarks for eva-Project:
## ApexCharts:
- [Working with Data](https://apexcharts.com/docs/series/)
- [Basic Heatmap Chart](https://apexcharts.com/javascript-chart-demos/heatmap-charts/basic/)
- [Line Chart Zoomable Timeseries](https://apexcharts.com/javascript-chart-demos/line-charts/zoomable-timeseries/)
- [DateTime](https://apexcharts.com/docs/datetime/)
