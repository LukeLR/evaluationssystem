<!DOCTYPE html>
<html lang="de">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <meta charset="UTF-8"/>

    <?php require_once 'shared/php/header.php';?>
    
    <script type="text/javascript" src="shared/js/questionsetDesigner.js"></script>

    <?php
        Kopfwelt\i18next::init($_SESSION["lang"], 'locales/__lng__/');
        printf("<title>%s</title>", Kopfwelt\i18next::getTranslation('nav.answer'));
    ?>
</head>
<body>
    <!-- Navigationsleiste-->
    <?php require_once 'shared/php/navbar.php';?>
    
    <?php
        require_once 'shared/html/modals/deleteModuleModal.html';
        require_once 'shared/html/modals/deleteQuestionsetModal.html';
        
        $accessTokenCollection = (new MongoDB\Client)->eva->accessTokens;
        $accessTokenCursor = $accessTokenCollection->aggregate(
            [
                [
                    '$match' => [
                        "token" => $_GET["token"]
                    ]
                ], // Find the matching answer access token
                [
                    '$lookup' => [
                        'from' => 'courses',
                        'localField' => 'courseID',
                        'foreignField' => '_id',
                        'as' => 'course'
                    ]
                ], // Merge course information for this token into the document
                [
                    '$lookup' => [
                        'from' => 'questionsets',
                        'localField' => 'courseID',
                        'foreignField' => 'courseID',
                        'as' => 'questionset'
                    ],
                ], // Merge questionset information into the document
                [
                    '$addFields' => [
                        // Works similiar to project, but doesn't remove used fields
                        'questionsetCount' => [
                            '$size' => '$questionset'
                        ]
                    ]
                ], // Count questionsets
                [
                    '$unwind' => '$questionset'
                ], /* Create an individual document for each questionset in order
                    * to only load the relevand questionset on the answer page
                    */
                [
                    '$sort' => [
                        'questionset.name' => 1
                    ]
                ], // Sort by questionset name, ascending
                [
                    '$skip' => (int)$_GET["page"]
                ], // Skip as many documents in the pipeline as the URL parameter says
                [
                    '$limit' => 1
                ] /* Only return one document, containing the token, the course and the
                   * answer data.
                   */
            ]
        );

        foreach($accessTokenCursor as $atc){
            $accessToken = $atc;
        }

        if ($accessToken != null){
            $c = $accessToken["course"][0];
            $q = $accessToken["questionset"];
            $coursename = $c["name"];
            $questionsetname = $q["name"];
        } else {
            $coursename = Kopfwelt\i18next::getTranslation('error');
            $questionsetname = $coursename;
        }
    ?>
    
    <!--Seiteninhalt-->
    <div class="container">
        <div class="page-header pt-3">
            <h1><span class="translatable mr-3" data-i18n="answer.title"></span><?php echo $coursename?></h1>
        </div>
        
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><span class="translatable" data-i18n="nav.answer"></span></li>
                <li class="breadcrumb-item"><span class="translatable mr-1" data-i18n="answer.title"></span><?php echo $coursename?></li>
                <li class="breadcrumb-item"><?php echo $questionsetname?></li>
            </ol>
        </nav>
        
        <?php
            if ($c != NULL){
                require_once 'shared/php/helpers/databaseHelpers.php';
                
                list ($earliestYear, $latestYear) = getModuleTimespan($m["_id"]);
                $courseCount = getModuleCourseCount($m["_id"], "course");
                $lectureCount = getModuleCourseCount($m["_id"], "lecture");

                if ($c["type"] == "lecture"){
                    $ctype = Kopfwelt\i18next::getTranslation('modules.lecture');
                } else {
                    $ctype = Kopfwelt\i18next::getTranslation('modules.course');
                }

                $questionsetCount = getCourseQuestionsetCount($c["_id"]);
                
                printf('
                    <div class="card">
                        <div class="card-body">
                            <a class="card-title h5 full-card" href="courseDetails.php?id=%s">%s</a>
                            <p class="card-text small text-muted mt-4">%s</p>
                            
                            <p class="card-text"><span class="oi oi-clipboard mr-2"></span>%d <span class="translatable" data-i18n="modules.questionset_s"></span></p>
                        </div>
                    </div>
                ', $c["_id"], $c["name"], $ctype, $questionsetCount);

                printf('<h2 class="mt-3"><span class="translatable mr-2" data-i18n="answer.questionsetTitle"></span>%s</h2>', $q["name"]);

                printf('
                    <form id="questionsetForm" class="mt-4 mb-4 questionsetForm" method="post"></form>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            fetchQuestionset("%s", "#questionsetForm", "answer", fetchAnswerset, {questionsetID: "%s", answerToken: "%s"});
                        });
                    </script>
                ', $q["_id"], $q["_id"], $accessToken["token"]);

                echo '
                    <div class="d-flex justify-content-center mb-3">
                        <div class="btn-group" role="group" aria-label="Pagination">
                ';

                for ($i = 0; $i < $accessToken["questionsetCount"]; $i++){
                    $additionalClasses = "";
                    if($i == (int)$_GET["page"]){
                        $additionalClasses = 'active" aria_pressed="true';
                    }

                    printf('<a type="button" href="answer.php?token=%s&page=%d" class="btn btn-secondary %s">%d</a>', $_GET["token"], $i, $additionalClasses, $i+1);
                }

                echo '
                        </div>
                    </div>
                ';
            }
        ?>
</body>
</html>
