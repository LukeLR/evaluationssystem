<!DOCTYPE html>
<html lang="de">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <meta charset="UTF-8"/>

    <?php require_once 'shared/php/header.php';?>

    <script type="text/javascript" src="shared/js/evalCourse.js"></script>
    <script type="text/javascript" src="node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" href="node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" crossorigin="anonymous">
    <script type="text/javascript" src="shared/js/modalfunctions.js"></script>
    <script type="text/javascript" src="node_modules/apexcharts/dist/apexcharts.min.js" integrity="sha384-gfajW5wDwYIen9wz7EE6FER19xBIS7pO4tWvurHRGOkP7XgrbiCRjDO8qJ/pSuLh"></script>
    <script type="text/javascript" src="shared/js/questionsetDesigner.js"></script>

    <link rel="stylesheet" href="node_modules/bootstrap-table/dist/bootstrap-table.css" integrity="sha384-VIzh1Ja+oB2plAqU7rZfRHZk6LH62vJEhFRIiy0yzsP74o9OzBHqbmG4kGnD9g+n">
    <script type="text/javascript" src="node_modules/bootstrap-table/dist/bootstrap-table.js" integrity="sha384-95RC9BzLxgn4y0W4+dnPbMfzWuvS3YJlCVN9IvsOUYRIKYCbFpN2oiFV7jk1A8kH"></script>
    <script type="text/javascript" src="node_modules/bootstrap-table/dist/extensions/auto-refresh/bootstrap-table-auto-refresh.min.js" integrity="sha384-PZihb7RZ2aSl1tDYAWRKCtyntjrp3OhgCJcN28Km7BeUtAPcYVK0KqmoO/2rp14X"></script>
    <script type="text/javascript" src="node_modules/bootstrap-table/dist/extensions/copy-rows/bootstrap-table-copy-rows.min.js" integrity="sha384-YojN5CZuoPCToO57f2MtmHJ2jGgO7105/70nTaazszgkR1c6kj00MLWTZfAJD9/+"></script>

    <script type="text/javascript" src="node_modules/file-saver/dist/FileSaver.min.js" integrity="sha384-ofuMfx3py8NbOuAQe/b73gfvZpMEsK9JY6Vx/Xtz4D8I3olc//JCvsXmlUgrUp1b"></script>
    <script type="text/javascript" src="node_modules/xlsx/dist/xlsx.core.min.js" integrity="sha384-0G6Ug/7xGu1RE6wxSmYHRPOjGQzz7Sx4pPhOe5NXqOo3G0DxdW9/654cu29vcNAd"></script>
    <script type="text/javascript" src="node_modules/jspdf/dist/jspdf.min.js" integrity="sha384-l7aOEgTYxgJ0nn2MziQWZCvuvJ2PtNcP05R4QwEoHW+kIS1gFpzupcQ7WhAdRKuq"></script>
    <script type="text/javascript" src="node_modules/jspdf-autotable/dist/jspdf.plugin.autotable.min.js" integrity="sha384-uHoycMrIA6TVga3CtDz9oLYbcb1NdMPPM0e3E5L5xVXM2bcyOkVcm3qLLDzQuMPJ"></script>
    <script type="text/javascript" src="node_modules/es6-promise/dist/es6-promise.min.js" integrity="sha384-fC7YtPfGV2EQm+aUlzyCxbcjj5ZZhR/z9vaPNf4QB8fCakyU9BkxFLUcqULo4svK"></script>
    <script type="text/javascript" src="node_modules/html2canvas/dist/html2canvas.min.js" integrity="sha384-0Dexxdu91XA6EAF55SES0QWJdXos9L6zoNQ4X9kFQ5FeZk2dzRpcqgYDR56V7iGX"></script>
    <script type="text/javascript" src="node_modules/tableexport.jquery.plugin/tableExport.min.js" integrity="sha384-wg8dhCLNPSImSKgNI/kZOGPSQD4KUR/cMLQ6jcfs0WcTRgjh1v17QhXYbnyhJMsP"></script>
    <script type="text/javascript" src="node_modules/bootstrap-table/dist/extensions/print/bootstrap-table-print.min.js" integrity="sha384-klvtWMEQBgt8kmGvqJ5bdeRFgfamId/0TyfvZVyg3PhHP1hen+Ff//5gdDsDk0HS"></script>
    <!--<script type="text/javascript" src="node_modules/bootstrap-table/dist/extensions/export/bootstrap-table-export.edit.js"></script>-->
    <!--<script type="text/javascript" src="node_modules/bootstrap-table/dist/extensions/export/bootstrap-table-export.js" integrity="sha384-d8rtAP8tGPA0WFngQJJNfDL5usX2R14K3M/xJ0Z5vu9s42Bl1NBNeo/n/y9SEIeL"></script>-->
    <script type="text/javascript" src="node_modules/bootstrap-table/dist/extensions/export/bootstrap-table-export.min.js" integrity="sha384-e46bruRlsSdbRXH6I7YZTOWNBw5KNflsByDYI+ZEJrBRQi0xH0LmCSc2rA2WCPw5"></script>

    <link rel="stylesheet" href="shared/css/chart.css" integrity="sha384-D8991G+F0bj/Ewnc0RoMXo/XxMFw6T5KuVMVW5fgJz/x7vCOHBCMq1Jwuv+oyB2p">

    <!-- Required by bootstrap-table: fontAwesome -->
    <script type="text/javascript" src="img/fontawesome/all.js" integrity="sha384-Yrgsqqj/Vq9MEQCcEGXskQNRlpQ2qjO68xRmuJGh4gcuKEBO+pzd4y1GfZggwNbN"></script>
    
    <?php
        Kopfwelt\i18next::init($_SESSION["lang"], 'locales/__lng__/');
        printf("<title>%s</title>", Kopfwelt\i18next::getTranslation('nav.evalCourse'));
    ?>
</head>
<body>
    <!-- Navigationsleiste-->
    <?php require_once 'shared/php/navbar.php';?>
    
    <?php
        require_once 'shared/html/modals/deleteCourseModal.html';
        require_once 'shared/html/modals/deleteQuestionsetModal.html';
        require_once 'shared/html/modals/deleteTokenListModal.html';
        
        $courses = (new MongoDB\Client)->eva->courses;
        $courseCursor = $courses->aggregate(
            [
                [
                    '$match' => [
                        "_id" => new MongoDB\BSON\ObjectId($_GET["id"]),
                        "readAccess" => $_SESSION["_id"]
                    ]
                ],
                [
                    '$lookup' => [
                        'from' => 'modules',
                        'localField' => 'moduleID',
                        'foreignField' => '_id',
                        'as' => 'module'
                    ]
                ],
                [
                    '$lookup' => [
                        'from' => 'questionsets',
                        'localField' => '_id',
                        'foreignField' => 'courseID',
                        'as' => 'questionsets',
                    ]
                ]
            ]
        );

        foreach ($courseCursor as $cc){
            $c = $cc;
        }
        
        Kopfwelt\i18next::init($_SESSION["lang"], 'locales/__lng__/');
        if ($c != NULL){
            $name = $c["name"];
            $moduleID = $c["moduleID"];
        } else {
            $name = Kopfwelt\i18next::getTranslation('error');
        }

        $m = $c["module"][0];
    ?>
    
    <!--Seiteninhalt-->
    <div class="container">
        <div class="page-header pt-3">
            <div class="float-right">
                <a class="btn btn-warning" href="courseDetails.php?id=<?php echo $c["_id"]?>">
                    <span class="oi oi-pencil"></span>
                    <span class="translatable" data-i18n="evalCourse.editCourse"></span>
                </a>
            </div>
            <h1><span class="translatable mr-3" data-i18n="evalCourse.title"></span><?php echo $name?></h1>
            <p class="translatable text-muted" data-i18n="evalCourse.description"></p>
        </div>
        
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="home.php" class="translatable" data-i18n="nav.home"></a></li>
                <li class="breadcrumb-item"><a href="moduleDetails.php?id=<?php echo $m["_id"]?>"><span class="translatable mr-1" data-i18n="moduleDetails.title"></span><?php echo $m["name"]?></a></li>
                <li class="breadcrumb-item"><a href="courseDetails.php?id=<?php echo $c["_id"]?>"><span class="translatable mr-1" data-i18n="courseDetails.title"></span><?php echo $name?></a></li>
                <li class="breadcrumb-item"><a><span class="translatable mr-1" data-i18n="evalCourse.title"></span><?php echo $name?></a></li>
            </ol>
        </nav>
        
        <?php
            if ($c != NULL){
                require_once 'shared/php/helpers/databaseHelpers.php';
                
                list ($earliestYear, $latestYear) = getModuleTimespan($m["_id"]);
                $courseCount = getModuleCourseCount($m["_id"], "course");
                $lectureCount = getModuleCourseCount($m["_id"], "lecture");

                if ($c["type"] == "lecture"){
                    $ctype = Kopfwelt\i18next::getTranslation('modules.lecture');
                } else {
                    $ctype = Kopfwelt\i18next::getTranslation('modules.course');
                }

                $questionsetCount = getCourseQuestionsetCount($c["_id"]);
                $answersetCount = getCourseAnswersetCount($c["_id"]);
                
                printf('
                    <div class="card mb-3">
                        <div class="card-body">
                            <a class="full-card" href="courseDetails.php?id=%s"></a>
                            <h5 class="card-title full-card-title">%s</h5>
                            <p class="card-text small text-muted">%s</p>
                            
                            <p class="card-text"><span class="oi oi-clipboard mr-2"></span>%d <span class="translatable" data-i18n="modules.questionset_s"></span></p>
                            <p class="card-text"><span class="oi oi-check mr-2"></span>%d <span class="translatable" data-i18n="modules.answerset_s"></span></p>
                        </div>
                    </div>
                ', $c["_id"], $c["name"], $ctype, $questionsetCount, $answersetCount);
            } else {
                echo '
                <div class="alert alert-danger translatable" role="alert" data-i18n="courseDetails.error.text"></div>
                ';
            }
        ?>

        <h2 class="translatable" data-i18n="evalCourse.answersByTime.title"></h2>
        <p class="text-muted translatable" data-i18n="evalCourse.answersByTime.description"></p>

        <div class="card mb-3">
            <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs" id="answersByTimeTabs" role="tablist">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle translatable" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" data-i18n="evalCourse.answersByTime.selectQuestionset"></a>
                        <div class="dropdown-menu">
                            <?php
                                foreach($c["questionsets"] as $q){
                                    printf('<a class="dropdown-item answersByTimeTab" href="#answersByTime%s" id="answersByTime%s-tab" data-toggle="tab" role="tab" aria-controls="answersByTime%s" aria-selected="false" data-questionsetid="%s" data-questionsetname="%s" data-courseid="%s" data-coursename="%s">%s</a>', $q["_id"], $q["_id"], $q["_id"], $q["_id"], $q["name"], $c["_id"], $c["name"], $q["name"]);
                                }
                            ?>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active translatable answersByTimeTab" href="#answersByTimeAllQuestionsets" id="answersByTimeAllQuestionsets-tab" data-toggle="tab" role="tab" aria-controls="answersByTimeAllQuestionsets" aria-selected="true" data-i18n="evalCourse.answersByTime.allQuestionsets" data-questionsetid="noid" data-questionsetname="All" data-courseid="<?php echo $c["_id"]?>" data-coursename="<?php echo $c["name"]?>"></a>
                    </li>
                </ul>
            </div>
            <div class="tab-content" id="answersByTimeContent">
                <div class="tab-pane fade show active" id="answersByTimeAllQuestionsets" role="tabpanel" aria-labelledby="answersByTimeAllQuestionsets-tab">
                    <div class="card-body">
                        <h3 class="translatable" data-i18n="evalCourse.answersByTime.allQuestionsets"></h3>
                        <div id="answersByTimeAllChart"></div>
                    </div>
                </div>
                <?php
                    foreach($c["questionsets"] as $q){
                        printf('
                        <div class="tab-pane fade show" id="answersByTime%s" role="tabpanel" aria-labelledby="answersByTime%s-tab">
                            <div class="card-body">
                                <h3>%s</h3>
                                <div id="answersByTime%sChart"></div>
                            </div>
                        </div>', $q["_id"], $q["_id"], $q["name"], $q["_id"]);
                    }
                ?>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function(){
                $(".answersByTimeTab").click(function(eventObject){
                    questionsetID = eventObject.target.dataset.questionsetid;
                    questionsetName = eventObject.target.dataset.questionsetname;
                    courseID = eventObject.target.dataset.courseid;
                    courseName = eventObject.target.dataset.coursename;
                    if (questionsetID == "noid"){
                        getAnswersetsByQuestionsetAndDate("course", courseID, courseName);
                    } else {
                        getAnswersetsByQuestionsetAndDate("questionset", questionsetID, questionsetName);
                    }
                });
                $('#answersByTimeAllQuestionsets-tab').click();
            });
        </script>

        <h2 class="translatable mt-3" data-i18n="evalCourse.answersByQuestionset.title"></h2>
        <p class="translatable text-muted" data-i18n="evalCourse.answersByQuestionset.description"></p>
        
        <div class="accordion mb-3" id="questionsetAccordion">
            <?php
                foreach ($c["questionsets"] as $qs){
                    printf('
                    <div class="card">
                        <div class="card-header" id="questionset%sHeading">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed questionsetHeaderButton" type="button" data-toggle="collapse" data-target="#questionset%sCollapse" aria-expanded="false" aria-controls="questionset%sCollapse" data-questionsetid="%s" data-viewmode="eval">
                                %s
                                </button>
                            </h2>
                        </div>
                        <div id="questionset%sCollapse" class="collapse collapsed" aria-labelledby="questionset%sHeading" data-parent="#questionsetAccordion">
                            <div class="card-body">
                                <h5>%s</h5>
                                <p class="translatable text-muted" data-i18n="evalCourse.answersByQuestionset.questionsetdescription"></p>

                                <div id="questionset%sDiv" class="mt-4 mb-4 questionsetDiv"></div>
                            </div>
                        </div>
                    </div>
                    ', $qs["_id"], $qs["_id"], $qs["_id"], $qs["_id"], $qs["name"], $qs["_id"], $qs["_id"], $qs["name"], $qs["_id"]);
                }
            ?>
        </div>
    </div>
            <div class="toast" id="copiedToast" data-delay="2000" style="position: fixed; right: 10px; top: 10px;">
                <div class="toast-header">
                    <div class="rounded mr-2 bg-primary" style="position: relative; width: 25px; height: 25px;">
                        <span class="far fa-copy m-1" style="color: white; position: relative; width: 20px;"></span>
                    </div>
                    <strong class="mr-auto translatable" data-i18n="evalCourse.answersByQuestionset.table.copiedHeader"></strong>
                    <small class="text-muted translatable" data-i18n="evalCourse.answersByQuestionset.table.copiedTimestamp"></small>
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="toast-body translatable" data-i18n="evalCourse.answersByQuestionset.table.copiedText"></div>
            </div>
</body>
</html>
