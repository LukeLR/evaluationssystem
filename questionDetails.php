<!DOCTYPE html>
<html lang="de">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <meta charset="UTF-8"/>

    <?php require_once 'shared/php/header.php';?>

    <script type="text/javascript" src="shared/js/questionsetDesigner.js"></script>
    <script type="text/javascript" src="shared/js/linkQuestions.js"></script>
    <script type="text/javascript" src="shared/js/cardrotate.js"></script>
    
    <link rel="stylesheet" href="shared/css/cardrotate.css">
    
    <?php
        Kopfwelt\i18next::init($_SESSION["lang"], 'locales/__lng__/');
        printf("<title>%s</title>", Kopfwelt\i18next::getTranslation('nav.questionDetails'));
    ?>
</head>
<body>
    <!-- Navigationsleiste-->
    <?php require_once 'shared/php/navbar.php';?>
    
    <?php
        $questions = (new MongoDB\Client)->eva->questions;
        $questionCursor = $questions->aggregate(
            [
                [
                    '$match' => [
                        '_id' => new MongoDB\BSON\ObjectId($_GET["id"])
                    ]
                ],
                [
                    '$lookup' => [
                        'from' => 'questionsets',
                        'localField' => 'questionsetID',
                        'foreignField' => '_id',
                        'as' => 'questionset'
                    ]
                ],
                [
                    '$lookup' => [
                        'from' => 'courses',
                        'localField' => 'questionset.courseID',
                        'foreignField' => '_id',
                        'as' => 'course'
                    ]
                ],
                [
                    '$lookup' => [
                        'from' => 'modules',
                        'localField' => 'course.moduleID',
                        'foreignField' => '_id',
                        'as' => 'module'
                    ]
                ],
            ]
        );
        Kopfwelt\i18next::init($_SESSION["lang"], 'locales/__lng__/');

        foreach ($questionCursor as $qc){
            $q = $qc;
        }
        
        if ($q != NULL){
            $name = $q["title"];
        } else if ($deletedCourse == true) {
            $name = Kopfwelt\i18next::getTranslation('moduleDetails.deleted.title');
        } else {
            $name = Kopfwelt\i18next::getTranslation('error');
        }
    ?>
    
    <!--Seiteninhalt-->
    <div class="container">
        <div class="page-header pt-3">
            <h1><span class="translatable mr-3" data-i18n="questionDetails.title"></span><?php echo $name?></h1>
        </div>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="home.php" class="translatable" data-i18n="nav.home"></a></li>
                <li class="breadcrumb-item"><a href="moduleDetails.php?id=<?php echo $q["module"][0]["_id"]?>"><span class="translatable mr-1" data-i18n="moduleDetails.title"></span><?php echo $q["module"][0]["name"]?></a></li>
                <li class="breadcrumb-item"><a href="courseDetails.php?id=<?php echo $q["course"][0]["_id"]?>"><span class="translatable mr-1" data-i18n="courseDetails.title"></span><?php echo $q["course"][0]["name"]?></a></li>
                <li class="breadcrumb-item"><a href="questionDetails.php?id=<?php echo $q["_id"]?>"><span class="translatable mr-1" data-i18n="questionDetails.title"></span><?php echo $q["title"]?></a></li>
            </ol>
        </nav>

        <form id="questionDetailsForm" class="mt-4 mb-4 questionsetForm"></form>
        
        <?php
            if ($q != NULL){
                printf('
                    <script type="text/javascript">
                        i18next.on("initialized", function(){
                            container = createQuestion("preview", "%s", "%s", "%s", "%s", "%s", "%s", \'%s\');
                            formContainer.appendTo("#questionDetailsForm");
                        });
                    </script>
                ', $q["_id"], $q["type"], $q["questionsetID"], $q["title"], $q["description"], $q["position"], json_encode($q["data"]));
            }
        ?>

        <h2 class="translatable" data-i18n="questionDetails.links"></h2>
        <form id="linkedQuestionsForm" class="mt-4 mb-4 questionsetForm"></form>

        <?php
            foreach($q["linkedQuestions"] as $lq){
                $lq = $questions->findOne(["_id" => $lq]);
                printf('
                    <script type="text/javascript">
                        i18next.on("initialized", function(){
                            container = createQuestion("link", "%s", "%s", "%s", "%s", "%s", "%s", \'%s\');
                            formContainer.appendTo("#linkedQuestionsForm");
                        });
                    </script>
                ', $lq["_id"], $lq["type"], $lq["questionsetID"], $lq["title"], $lq["description"], $lq["position"], json_encode($lq["data"]));
            }
        ?>

        <h2 class="translatable" data-i18n="questionDetails.addLink.title"></h2>
        <div class="card-columns">
            <?php
                $moduleCollection = (new MongoDB\Client)->eva->modules;
                $modules = $moduleCollection->find(["readAccess" => $_SESSION["_id"]], ['sort' => ['name' => 1]]);

                require_once 'shared/php/helpers/databaseHelpers.php';
                
                foreach ($modules as $m){
                    list ($earliestYear, $latestYear) = getModuleTimespan($m["_id"]);
                    $courseCount = getModuleCourseCount($m["_id"], "course");
                    $lectureCount = getModuleCourseCount($m["_id"], "lecture");
                    $questionsetCount = getModuleQuestionsetCount($m["_id"]);
                    
                    printf('
                        <div class="rotate" id="module%srotate">
                            <div class="card" id="module%scard" style="height: 370px">
                                <div class="face front" id="module%sfront">
                                    <div class="card-body">
                                        <a class="card-title h5 full-card" href="moduleDetails.php?id=%s">%s</a>
                                        <p class="text-muted small mt-4 translatable" data-i18n="modules.module"></p>
                                        
                                        <p class="card-text"><span class="oi oi-book mr-2"></span>%d <span class="translatable" data-i18n="modules.lecture_s"></span></p>
                                        <p class="card-text"><span class="oi oi-laptop mr-2"></span>%d <span class="translatable" data-i18n="modules.course_s"></span></p>
                                        <p class="card-text"><span class="oi oi-clipboard mr-2"></span>%d <span class="translatable" data-i18n="modules.questionset_s"></span></p>
                                        <p class="card-text"><span class="oi oi-check mr-2"></span>%d <span class="translatable" data-i18n="modules.answerset_s"></span></p>
                                        <p class="card-text"><span class="oi oi-calendar mr-2"></span>%d-%d</p>
                                    </div>
                                </div>
                                <div class="face back" id="module%sback" style="display: none">
                                    <div class="card-body">
                                        <h4 class="card-title">%s</h4>
                                        <form method="post" name="newLink">
                                            <div class="form-group">
                                                <h5 class="translatable" data-i18n="questionDetails.addLink.chooseCourse.title"></h5>
                                                <select class="form-control" name="courseSelect" id="module%scourseselect" data-moduleid="%s">
                                                    <option value="default" class="translatable" data-i18n="questionDetails.addLink.chooseCourse.dropdown" selected></option>
                    ', $m["_id"], $m["_id"], $m["_id"], $m["_id"], $m["name"], $lectureCount, $courseCount, $questionsetCount, $m["answersets"], $earliestYear, $latestYear, $m["_id"], $m["name"], $m["_id"], $m["_id"], $m["_id"]);

                    $courseCollection = (new MongoDB\Client)->eva->courses;
                    $courses = $courseCollection->find(["moduleID" => $m["_id"], "readAccess" => $_SESSION["_id"]]);

                    foreach($courses as $c){
                        printf('
                                                    <option value="%s">%s (%s, %s %d)</option>
                        ', $c["_id"], $c["name"], $c["type"], $c["semester"]["type"], $c["semester"]["year"]);
                    }

                    printf('
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <h5 class="translatable" data-i18n="questionDetails.addLink.chooseQuestionset.title"></h5>
                                                <select class="form-control" name="questionsetSelect" id="module%squestionsetselect" data-moduleid="%s">
                                                    <option value="default" class="translatable" data-i18n="questionDetails.addLink.chooseQuestionset.dropdown" selected></option>
                    ', $m["_id"], $m["_id"], $m["_id"]);

                    printf('
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <h5 class="translatable" data-i18n="questionDetails.addLink.chooseQuestion.title"></h5>
                                                <select class="form-control" name="questionSelect" id="module%squestionselect" data-moduleid="%s">
                                                    <option value="default" class="translatable" data-i18n="questionDetails.addLink.chooseQuestion.dropdown" selected></option>
                    ', $m["_id"], $m["_id"], $m["_id"]);

                    printf('
                                                </select>
                                            </div>
                                            <input type="hidden" id="question" name="question" value="%s">
                                            <div class="float-right">
                                                <button type="submit" id="linkQuestionButton" name="linkQuestionButton" class="btn btn-primary"><span class="oi oi-link-intact mr-2"></span><span class="translatable" data-i18n="questionDetails.addLink.button"></span></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                            $(document).ready(function(){
                                addRotateHandler("#module%srotate", "#module%scard", "module%sfront", "module%sback");
                                addRotateStopper("#module%srotate", "#module%scourseselect");
                                $("#module%scourseselect").change(loadQuestionsets);
                                $("#module%squestionsetselect").change(loadQuestions);
                            });
                        </script>
                    ', $q["_id"], $m["_id"], $m["_id"], $m["_id"], $m["_id"], $m["_id"], $m["_id"], $m["_id"], $m["_id"]);
                }
            ?>
        </div>
</body>
</html>
