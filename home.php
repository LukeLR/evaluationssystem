<!DOCTYPE html>
<html lang="de">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <meta charset="UTF-8"/>

    <?php require_once 'shared/php/header.php';?>
    
    <script type="text/javascript" src="shared/js/cardrotate.js"></script>
    <link rel="stylesheet" href="shared/css/cardrotate.css">
    
    <?php
        Kopfwelt\i18next::init($_SESSION["lang"], 'locales/__lng__/');
        printf("<title>%s</title>", Kopfwelt\i18next::getTranslation('nav.modules'));
    ?>
</head>
<body>
    <!-- Navigationsleiste-->
    <?php require_once 'shared/php/navbar.php';?>
    
    <!--Seiteninhalt-->
    <div class="container">
        <div class="page-header pt-3">
            <h1 class="translatable" data-i18n="nav.modules"></h1>
        </div>
        
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a class="translatable" data-i18n="nav.modules"></a></li>
            </ol>
        </nav>
        
        <div class="card-columns">
            <div class="rotate" id="addCardRotate">
                <div class="card" id="addCard" style="height: 300px">
                    <div class="face front" id="addModuleFront">
                        <div class="d-flex justify-content-center" style="height: 300px">
                            <img src="img/add.svg" alt="add" width="100">
                        </div>
                    </div>
                    <div class="face back" id="addModuleBack" style="display: none">
                        <div class="card-body mb-4">
                            <h5 class="modal-title translatable" id="newModuleModalLabel" data-i18n="modules.newModule.title"></h5>
                            <p class="text-muted small translatable" id="newModuleDescription" data-i18n="modules.newModule.description"></p>
                            <form method="post" name="newModule">
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <input type="text" class="form-control translatable" id="newModuleName" name="newModuleName" data-i18n="[placeholder]modules.newModule.name">
                                    </div>
                                </div>
                                <div class="float-right">
                                    <button type="submit" id="newModuleButton" name="newModuleButton" class="btn btn-primary translatable" data-i18n="create"></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
            <?php
                $moduleCollection = (new MongoDB\Client)->eva->modules;
                $modules = $moduleCollection->find(["readAccess" => $_SESSION["_id"]], ['sort' => ['name' => 1]]);

                require_once 'shared/php/helpers/databaseHelpers.php';
                
                foreach ($modules as $m){
                    list ($earliestYear, $latestYear) = getModuleTimespan($m["_id"]);
                    $courseCount = getModuleCourseCount($m["_id"], "course");
                    $lectureCount = getModuleCourseCount($m["_id"], "lecture");
                    $questionsetCount = getModuleQuestionsetCount($m["_id"]);
                    $answersetCount = getModuleAnswersetCount($m["_id"]);
                    
                    printf('
                        <div class="card">
                            <div class="card-body">
                                <a class="full-card" href="moduleDetails.php?id=%s"></a>
                                <h5 class="card-title full-card-title">%s</h5>
                                <p class="text-muted small translatable" data-i18n="modules.module"></p>
                                
                                <p class="card-text"><span class="oi oi-book mr-2"></span>%d <span class="translatable" data-i18n="modules.lecture_s"></span></p>
                                <p class="card-text"><span class="oi oi-laptop mr-2"></span>%d <span class="translatable" data-i18n="modules.course_s"></span></p>
                                <p class="card-text"><span class="oi oi-clipboard mr-2"></span>%d <span class="translatable" data-i18n="modules.questionset_s"></span></p>
                                <p class="card-text"><span class="oi oi-check mr-2"></span>%d <span class="translatable" data-i18n="modules.answerset_s"></span></p>
                                <p class="card-text"><span class="oi oi-calendar mr-2"></span>%d-%d</p>
                            </div>
                        </div>
                    ', $m["_id"], $m["name"], $lectureCount, $courseCount, $questionsetCount, $answersetCount, $earliestYear, $latestYear);
                }
            ?>
        </div>
    </div>
</body>
</html>
