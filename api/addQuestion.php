<?php
    require_once __DIR__ . "/../vendor/autoload.php";
    require_once __DIR__ . '/../shared/php/helpers/questionManagementHelpers.php';
    session_start();

    echo addQuestion($_POST["questionID"], new MongoDB\BSON\ObjectId($_POST["questionsetID"]), $_POST["title"], $_POST["description"], $_POST["type"], (int)$_POST["position"], $_POST["additionalData"]);
?>
