<?php
    require_once __DIR__ . "/../vendor/autoload.php";
    require_once __DIR__ . '/../shared/php/helpers/questionManagementHelpers.php';
    session_start();

    echo deleteQuestionLink(new MongoDB\BSON\ObjectId($_POST["questionID"]), new MongoDB\BSON\ObjectId($_POST["linkID"]));
?>
