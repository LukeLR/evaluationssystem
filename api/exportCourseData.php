<?php
    require_once __DIR__ . "/../vendor/autoload.php";
    session_start();

    $courses = (new MongoDB\Client)->eva->courses;
    $courseID = new MongoDB\BSON\ObjectID($_POST["exportDataCourseID"]);

    $aggregateQuery = [
        [
            '$match' => [
                "_id" => $courseID,
                "writeAccess" => $_SESSION["_id"]
            ]
        ], // Find the matching course
        [
            '$lookup' => [
                'from' => 'questionsets',
                'localField' => '_id',
                'foreignField' => 'courseID',
                'as' => 'questionsets'
            ]
        ], // Join questionset data into the document
        [
            '$lookup' => [
                'from' => 'modules',
                'localField' => 'moduleID',
                'foreignField' => '_id',
                'as' => 'module'
            ]
        ], // Join module data into the document
        [
            '$addFields' => [
                'module' => [
                    '$filter' => [
                        'input' => '$module',
                        'as' => 'module',
                        'cond' => [
                            '$in' => [
                                $_SESSION["_id"],
                                '$$module.readAccess'
                            ]
                        ]
                    ]
                ],
                'questionsets' => [
                    '$filter' => [
                        'input' => '$questionsets',
                        'as' => 'questionset',
                        'cond' => [
                            '$in' => [
                                $_SESSION["_id"],
                                '$$questionset.readAccess'
                            ]
                        ]
                    ]
                ]
            ]
        ], /* Filter joined module and questionset documents to only display
            * those the user has read access to
            */
        [
            '$unwind' => '$questionsets'
        ], // Create one document per questionset
        [
            '$lookup' => [
                'from' => 'questions',
                'localField' => 'questionsets._id',
                'foreignField' => 'questionsetID',
                'as' => 'questions'
            ]
        ], // Find data of all questions and join into the questionset document
        [
            '$sort' => [
                'questionsets.name' => 1
            ] // TODO: Also sort questions by position
        ], // Sort all the questionset documents by name
        [
            '$group' => [
                '_id' => '$_id',
                'moduleID' => ['$first' => '$moduleID'],
                'name' => ['$first' => '$name'],
                'type' => ['$first' => '$type'],
                'semester' => ['$first' => '$semester'],
                'readAccess' => ['$first' => '$readAccess'],
                'writeAccess' => ['$first' => '$writeAccess'],
                'tokenLists' => ['$first' => '$tokenLists'],
                'module' => ['$first' => '$module'],
                'questionsets' => [
                    '$push' => '$questionsets'
                ],
                'questions' => [
                    '$push' => '$questions'
                ],
                'answersets' => [
                    '$push' => '$answersets'
                ]
            ]
        ] /* Group to only return document containing arrays for the
           * questionsets, the questions and the answersets, if included
           * in the export
           */
    ];

    if (isset($_POST["includeAnswers"])){
        array_splice($aggregateQuery, 6, 0, [[
            '$lookup' => [
                'from' => 'answersets',
                'localField' => 'questionsets._id',
                'foreignField' => 'questionsetID',
                'as' => 'answersets'
            ]
        ]]);
    }
    
    $courseCursor = $courses->aggregate($aggregateQuery);

    foreach($courseCursor as $cc){
        $course = $cc;
    }

    if($course != null){
        header('Content-disposition: attachment; filename=courseData-' . strftime("%Y-%m-%d_%H-%M-%S", time()) . '.json');
        header('Content-type: text/plain');

        echo json_encode($course, JSON_PRETTY_PRINT);
    } else {
        echo "An unkown error occurred. Please try again later.";
    }
?>
