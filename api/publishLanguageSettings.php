<?php
    require_once __DIR__ . "/../vendor/autoload.php";
    session_start();
    $_SESSION["lang"] = $_POST["lang"];
    $collection = (new MongoDB\Client)->eva->users;
    $document = $collection->findOne(['mail' => $_SESSION["mail"]]);
    if($document != NULL){
        $updateOneResult = $collection->updateOne(['_id' => $document['_id']], ['$set' => ["lang" => $_SESSION["lang"]]]);
    }
?>
