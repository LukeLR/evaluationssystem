<?php
    require_once __DIR__ . "/../vendor/autoload.php";
    session_start();
    require_once __DIR__ . '/../settings.php';

    $courses = (new MongoDB\Client)->eva->courses;
    $courseID = new MongoDB\BSON\ObjectID($_GET["courseID"]);
    $course = $courses->findOne(["_id" => $courseID, "writeAccess" => $_SESSION["_id"]]);

    if($course != null){
        header('Content-disposition: attachment; filename=accessTokens-' . strftime("%Y-%m-%d_%H-%M-%S", (int)$_GET["timestamp"]) . '.txt');
        header('Content-type: text/plain');

        $accessTokens = (new MongoDB\Client)->eva->accessTokens;
        $tokenCursor = $accessTokens->find(
            [
                "courseID" => $course["_id"],
                "timestamp" => (int)$_GET["timestamp"]
            ]
        );

        foreach ($tokenCursor as $token){
            echo $GLOBALS['external_url'] . "/answer.php?token=" . $token["token"] . "\n";
        }
    }
?>
