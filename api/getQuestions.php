<?php
    if ($_POST["questionsetID"] != "default"){
        require_once __DIR__ . "/../vendor/autoload.php";
        session_start();
    
        $questionCollection = (new MongoDB\Client)->eva->questions;
        $questionCursor = $questionCollection->find(["questionsetID" => new MongoDB\BSON\ObjectId($_POST["questionsetID"])],['sort' => ["position" => 1]]);
    
        $questions = [];
    
        foreach ($questionCursor as $q){
            $q["questionID"] = $q["_id"]->__toString();
            unset($q["_id"]);
            $q["questionsetID"] = $q["questionsetID"]->__toString();
            array_push($questions, $q);
        }
    
        echo json_encode($questions);
    } else {
        echo json_encode([]);
    }
?>
