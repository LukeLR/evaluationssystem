<?php
    require_once __DIR__ . "/../vendor/autoload.php";
    require_once __DIR__ . '/../shared/php/helpers/questionManagementHelpers.php';
    session_start();

    echo deleteQuestion(new MongoDB\BSON\ObjectId($_POST["questionID"]));
?>
