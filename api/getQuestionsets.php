<?php
    if ($_POST["courseID"] != "default"){
        require_once __DIR__ . "/../vendor/autoload.php";
        session_start();
        
        $questionsetCollection = (new MongoDB\Client)->eva->questionsets;
        $questionsetCursor = $questionsetCollection->find(["courseID" => new MongoDB\BSON\ObjectId($_POST["courseID"])]);

        $questionsets = [];

        foreach ($questionsetCursor as $q){
            $q["_id"] = $q["_id"]->__toString();
            $q["courseID"] = $q["courseID"]->__toString();
            array_push($questionsets, $q);
        }

        echo json_encode($questionsets);
    } else {
        echo json_encode([]);
    }
?>
