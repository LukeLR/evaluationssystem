<?php
    require_once __DIR__ . "/../vendor/autoload.php";
    session_start();

    $answersets = (new MongoDB\Client)->eva->answersets;
    $questions = (new MongoDB\Client)->eva->questions;
    $answerset = $answersets->findOne(
        [
            'questionsetID' => new MongoDB\BSON\ObjectID($_POST["questionsetID"]),
            'token' => $_POST["answerToken"]
        ]
    );

    $answersetArray = [];

    foreach(array_keys($answerset->getArrayCopy()) as $a){
        // Filter the $_POST-array for valid questionIDs and answers
        try {
            $questionID = new MongoDB\BSON\ObjectID($a);
            $question = $questions->findOne(["_id" => $questionID]);
            if ($question != null){
                array_push($answersetArray, ['questionID' => $a, 'answer' => $answerset[$a]]);
            }
        } catch (MongoDB\Driver\Exception\InvalidArgumentException $e){
            
        }
    }

    $returnAnswerset = [
        "questionsetID" => $answerset["questionsetID"],
        "token" => $answerset["token"],
        "answers" => $answersetArray
    ];

    echo json_encode($returnAnswerset);
?>
