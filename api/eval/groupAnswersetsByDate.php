<?php
    require_once __DIR__ . "/../../vendor/autoload.php";
    session_start();

    $courseID = new MongoDB\BSON\ObjectID($_POST["courseID"]);
    $results = [];

    $courses = (new MongoDB\Client)->eva->courses;
    $answersetGroupCursor = $courses->aggregate(
        [
            [
                '$match' => [
                    '_id' => $courseID,
                    'readAccess' => $_SESSION["_id"]
                ]
            ], // Filter for matching questionset IDs, ensure read access
            [
                '$lookup' => [
                    'from' => 'questionsets',
                    'localField' => '_id',
                    'foreignField' => 'courseID',
                    'as' => 'questionsets'
                ]
            ], // Merge questionsets into course documents
            [
                '$unwind' => '$questionsets'
            ], // Create a document for each questionset
            [
                '$lookup' => [
                    'from' => 'answersets',
                    'localField' => 'questionsets._id',
                    'foreignField' => 'questionsetID',
                    'as' => 'answersets'
                ]
            ], // Merge answersets into course / questionset documents
            [
                '$unwind' => '$answersets'
            ], // Create a document for each answerset
            [
                '$addFields' => [
                    'answersets.timestamp' => [
                        '$dateToParts' => [
                            'date' => '$answersets.timestamp'
                        ]
                    ]
                ]
            ], // Split Date for grouping by day, month and year
            [
                '$group' => [
                    '_id' => [
                        'day' => '$answersets.timestamp.day',
                        'month' => '$answersets.timestamp.month',
                        'year' => '$answersets.timestamp.year'
                    ],
                    'count' => [
                        '$sum' => 1
                    ]
                ]
            ], // Count documents for each combination of day, month and year
            [
                '$addFields' => [
                    'day.day' => '$_id.day',
                    'day.count' => '$count'
                ]
            ], /* Rename fields so they form a subdocument for later
                * grouping into an array.
                */
            [
                '$group' => [
                    '_id' => [
                        'month' => '$_id.month',
                        'year' => '$_id.year'
                    ],
                    'days' => [
                        '$addToSet' => '$day'
                    ]
                ]
            ] /* Group all documents so only one document is returned
               * containing all the query data as an array.
               */
        ]
    );

    foreach ($answersetGroupCursor as $agc){
        array_push($results, $agc);
    }

    echo json_encode($results);
?>
