<?php
    /* This script will be called from bootstrap-table automatically,
     * so make sure that the returned data format is compliant to
     * bootstrap-table's requirements. There are examples for a correct
     * data output format available online:
     * 
     * Without server side pagination:
     *   https://github.com/wenzhixin/bootstrap-table-examples/blob/master/json/data1.json
     * With server side pagination:
     *   https://github.com/wenzhixin/bootstrap-table-examples/blob/master/json/data2.json
     */
    
    require_once __DIR__ . "/../../vendor/autoload.php";
    session_start();

    if ($_POST["order"] == "asc") {
        $order = 1;
    } else {
        $order = -1;
    }

    $questionID = new MongoDB\BSON\ObjectID($_POST["questionID"]);

    $answersets = (new MongoDB\Client)->eva->answersets;
    $answerCursor = $answersets->aggregate(
        [
            [
                '$match' => [
                    $_POST["questionID"] => [
                        '$ne' => null
                    ]
                ]
            ], /* Find all answer documents that contain an answer to the
                * requested question. The complexity is neccessary, since
                * the questionID is a key, not a value in the answerset
                * documents.
                */
            [
                '$project' => [
                    '_id' => 0,
                    'answer.timestamp' => '$timestamp',
                    'answer.answer' => '$' . $_POST["questionID"]
                ]
            ], /* Only include the timestamp and the answer to the requested
                * question in the documents, strip all other fields.
                * Rename the answer to answer, so we don't need the questionID
                * as the key name for retrieving the answer anymore.
                */
            [
                '$group' => [
                    '_id' => null,
                    'rows' => [
                        '$addToSet' => '$answer'
                    ],
                    'total' => [
                        '$sum' => 1
                    ]
                ]
            ], /* Create a single document containing all answers to the
                * question as an array and count answers as well.
                */
            [
                '$unwind' => '$rows'
            ], // Create an individual document per answer for sorting
            [
                '$sort' => [
                    'rows.' . $_POST["sort"] => $order
                ]
            ], // Sort by the selected table field
            [
                '$skip' => (int)$_POST["offset"]
            ], // Skip number of documents (for pagination)
            [
                '$limit' => (int)$_POST["limit"]
            ], // Limit Number of documents (for pagination)
            [
                '$group' => [
                    '_id' => null,
                    'rows' => [
                        '$push' => '$rows'
                    ],
                    'total' => [
                        '$first' => '$total'
                    ]
                ]
            ] /* Group documents back into a single dataset with an array
               * containing the rows
               */
        ]
    );

    foreach ($answerCursor as $agc){
        $results = $agc;
    }

    echo json_encode($results);
?>
