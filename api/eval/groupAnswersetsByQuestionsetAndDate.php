<?php
    require_once __DIR__ . "/../../vendor/autoload.php";
    session_start();

    $questionsetID = new MongoDB\BSON\ObjectID($_POST["questionsetID"]);
    $results = [];

    $questionsets = (new MongoDB\Client)->eva->questionsets;
    $answersetGroupCursor = $questionsets->aggregate(
        [
            [
                '$match' => [
                    '_id' => $questionsetID,
                    'readAccess' => $_SESSION["_id"]
                ]
            ], // Filter for matching questionset IDs, ensure read access
            [
                '$lookup' => [
                    'from' => 'answersets',
                    'localField' => '_id',
                    'foreignField' => 'questionsetID',
                    'as' => 'answersets'
                ]
            ], // Merge answersets into questionset documents
            [
                '$unwind' => '$answersets'
            ], // Create a document for each answerset
            [
                '$addFields' => [
                    'answersets.timestamp' => [
                        '$dateToParts' => [
                            'date' => '$answersets.timestamp'
                        ]
                    ]
                ]
            ], // Split Date for grouping by day, month and year
            [
                '$group' => [
                    '_id' => [
                        'day' => '$answersets.timestamp.day',
                        'month' => '$answersets.timestamp.month',
                        'year' => '$answersets.timestamp.year'
                    ],
                    'count' => [
                        '$sum' => 1
                    ]
                ]
            ], // Count documents for each combination of day, month and year
            [
                '$addFields' => [
                    'day.day' => '$_id.day',
                    'day.count' => '$count'
                ]
            ], // Put day and count into a subdocument for later grouping
            [
                '$group' => [
                    '_id' => [
                        'month' => '$_id.month',
                        'year' => '$_id.year'
                    ],
                    'days' => [
                        '$addToSet' => '$day'
                    ]
                ]
            ] /* Group all the documents by month and year as the key.
               * This creates one document per month/year combination,
               * containing an array that holds documents which provide
               * information on the count of answers on that given day
               * in that month.
               */
        ]
    );

    foreach ($answersetGroupCursor as $agc){
        array_push($results, $agc);
    }

    echo json_encode($results);
?>
