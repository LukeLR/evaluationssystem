<?php
    require_once __DIR__ . "/../../vendor/autoload.php";
    session_start();

    $questionID = new MongoDB\BSON\ObjectID($_POST["questionID"]);

    // Get information about question and all linked questions
    $questions = (new MongoDB\Client)->eva->questions;
    $questionCursor = $questions->aggregate(
        [
            [
                '$match' => [
                    '_id' => $questionID
                ]
            ], // Find specified question.
            [
                '$addFields' => [
                    'linkedQuestions' => [
                        '$setUnion' => [
                            '$linkedQuestions',
                            [$questionID]
                        ]
                    ]
                ]
            ], /* Add question itsetlf to array of linkedQuestions,
                * so that we just have to loop over that array to
                * get information on all linked questions and the
                * original question as well, instead of having to
                * treat the original question differently.
                */
            [
                '$unwind' => [
                    'path' => '$linkedQuestions',
                    'preserveNullAndEmptyArrays' => true
                ]
            ], // Get an individual document for each linked question.
            [
                '$lookup' => [
                    'from' => 'questions',
                    'localField' => 'linkedQuestions',
                    'foreignField' => '_id',
                    'as' => 'linkedQuestion'
                ]
            ], // Load the data for each question into it's document.
            [
                '$unwind' => [
                    'path' => '$linkedQuestion',
                    'preserveNullAndEmptyArrays' => true
                ]
            ], /* The above lookup is querying for the question _id,
                * therefore it should only return one document. Still,
                * returned documents by a lookup are always kept in an
                * array, so this second unwind stage removes that
                * unneccessary array.
                */
            [
                '$lookup' => [
                    'from' => 'questionsets',
                    'localField' => 'linkedQuestion.questionsetID',
                    'foreignField' => '_id',
                    'as' => 'linkedQuestionQuestionset'
                ]
            ], // Load the questionset the question belongs to.
            [
                '$lookup' => [
                    'from' => 'courses',
                    'localField' => 'linkedQuestionQuestionset.courseID',
                    'foreignField' => '_id',
                    'as' => 'linkedQuestionCourse'
                ]
            ], // Load the course the questionset belongs to.
            [
                '$match' => [
                    'linkedQuestionQuestionset.readAccess' => $_SESSION["_id"]
                ]
            ], /* Filter questions to only return those the user has
                * read access to. The filtering must be done on the
                * questionset, since questions itself don't store
                * access data (for storage optimization).
                */
            [
                '$project' => [
                    'linkedQuestion.linkedQuestions' => 0,
                    'linkedQuestion.position' => 0,
                    'linkedQuestion.questionsetID' => 0
                ]
            ], // Filter out some information.
            [
                '$project' => [
                    'linkedQuestion.question' => '$linkedQuestion',
                    'linkedQuestion.semester' => '$linkedQuestionCourse.semester'
                ]
            ], // Filter out more information and rename fields.
            // Get rid of unneccessary arrays that should contain only one element:
            [
                '$unwind' => [
                    'path' => '$linkedQuestion',
                    'preserveNullAndEmptyArrays' => true
                ]
            ],
            [
                '$unwind' => [
                    'path' => '$semester',
                    'preserveNullAndEmptyArrays' => true
                ]
            ],
            [
                '$unwind' => [
                    'path' => '$linkedQuestion.semester',
                    'preserveNullAndEmptyArrays' => true
                ]
            ],
            [
                '$sort' => [
                    'linkedQuestion.semester.year' => 1,
                    'linkedQuestion.semester.type' => 1
                ]
            ], // Sort questions by semester (type and year)
            [
                '$group' => [
                    '_id' => '$_id',
                    'linkedQuestions' => [
                        '$push' => '$linkedQuestion'
                    ]
                ]
            ] /* Push only the linked question data into an array, return
               * only one document containing the array.
               */
        ]
    );

    foreach ($questionCursor as $qc){
        $results = $qc;
    }

    $answersets = (new MongoDB\Client)->eva->answersets;

    for ($i = 0; $i < sizeof($results["linkedQuestions"]); $i++){
        $result = [
            'total' => 0,
            'answers' => []
        ];
        $questionID = $results["linkedQuestions"][$i]["question"]["_id"]->__toString();
        $answersetCursor = $answersets->aggregate(
            [
                [
                    '$match' => [
                        $questionID => [
                            '$ne' => null
                        ]
                    ]
                ],
                [
                    '$project' => [
                        $questionID => 1
                    ]
                ],
                [
                    '$unwind' => '$' . $questionID
                ],
                [
                    '$group' => [
                        '_id' => '$' . $questionID,
                        'count' => [
                            '$sum' => 1
                        ]
                    ]
                ],
                [
                    '$project' => [
                        'answer._id' => '$_id',
                        'answer.count' => '$count'
                    ]
                ],
                [
                    '$group' => [
                        '_id' => $questionID,
                        'answers' => [
                            '$push' => '$answer'
                        ],
                        'total' => [
                            '$sum' => '$answer.count'
                        ]
                    ]
                ]
            ]
        );

        foreach ($answersetCursor as $ac){
            $result = $ac;
        }

        $results["linkedQuestions"][$i]["answers"] = $result;

    }

    echo json_encode($results, JSON_PRETTY_PRINT);
?>
