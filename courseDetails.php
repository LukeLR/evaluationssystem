<!DOCTYPE html>
<html lang="de">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <meta charset="UTF-8"/>

    <?php require_once 'shared/php/header.php';?>

    <script type="text/javascript" src="node_modules/node-forge/dist/forge.min.js" integrity="sha384-++Uhlw152cbdh4HNKY+/cvIodC2/zzy6mQajIwMjEDpK1ha8U3Gmiyqfl+AZGKUP"></script>
    <script type="text/javascript" src="shared/js/questionsetDesigner.js"></script>
    <script type="text/javascript" src="node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" href="node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" crossorigin="anonymous">
    <script type="text/javascript" src="shared/js/modalfunctions.js"></script>

    <?php
        Kopfwelt\i18next::init($_SESSION["lang"], 'locales/__lng__/');
        printf("<title>%s</title>", Kopfwelt\i18next::getTranslation('nav.courseDetails'));
    ?>
</head>
<body>
    <!-- Navigationsleiste-->
    <?php require_once 'shared/php/navbar.php';?>
    
    <?php
        require_once 'shared/html/modals/deleteCourseModal.html';
        require_once 'shared/html/modals/deleteQuestionsetModal.html';
        require_once 'shared/html/modals/deleteTokenListModal.html';
        
        $courses = (new MongoDB\Client)->eva->courses;
        $courseCursor = $courses->aggregate(
            [
                [
                    '$match' => [
                        "_id" => new MongoDB\BSON\ObjectId($_GET["id"]),
                        "readAccess" => $_SESSION["_id"]
                    ]
                ], // Find matching courses
                [
                    '$lookup' => [
                        'from' => 'accessTokens',
                        'localField' => '_id',
                        'foreignField' => 'courseID',
                        'as' => 'accessTokens'
                    ]
                ], // Join access token data into course
                [
                    '$unwind' => [
                        'path' => '$accessTokens',
                        'preserveNullAndEmptyArrays' => true
                    ]
                ], // Unwind to get one dataset per access token
                [
                    '$addFields' => [
                        'courseID' => '$_id'
                    ]
                ], // Rename _id to courseID
                [
                    '$group' => [
                        '_id' => '$accessTokens.timestamp',
                        'accessTokenExpires' => ['$first' => '$accessTokens.expires'],
                        'courseID' => ['$first' => '$courseID'],
                        'moduleID' => ['$first' => '$moduleID'],
                        'name' => ['$first' => '$name'],
                        'type' => ['$first' => '$type'],
                        'semester' => ['$first' => '$semester'],
                        'readAccess' => ['$first' => '$readAccess'],
                        'writeAccess' => ['$first' => '$writeAccess'],
                        'accessTokenCount' => ['$sum' => 1]
                    ]
                ], /* Group access token per access token set (identified by timestamp),
                    * and count them.
                    */
                [
                    '$addFields' => [
                        'accessTokens.timestamp' => '$_id',
                        'accessTokens.count' => '$accessTokenCount',
                        'accessTokens.expires' => '$accessTokenExpires',
                        '_id' => '$courseID'
                    ]
                ], /* Create access token subdocument containing all relevant
                    * information about the access token list grouped in the
                    * previous step, for grouping into an array in the next
                    * step
                    */
                [
                    '$group' => [
                        '_id' => '$_id',
                        'moduleID' => ['$first' => '$moduleID'],
                        'name' => ['$first' => '$name'],
                        'type' => ['$first' => '$type'],
                        'semester' => ['$first' => '$semester'],
                        'readAccess' => ['$first' => '$readAccess'],
                        'writeAccess' => ['$first' => '$writeAccess'],
                        'accessTokens' => ['$addToSet' => '$accessTokens']
                    ]
                ], /* Group all resulting documents into a single document
                    * containing the course information and an array of
                    * all access token informations
                    */
                [
                    '$lookup' => [
                        'from' => 'modules',
                        'localField' => 'moduleID',
                        'foreignField' => '_id',
                        'as' => 'module'
                    ]
                ] // Join module information into the course document
            ]
        );

        foreach ($courseCursor as $cc){
            $c = $cc;
        }
        
        Kopfwelt\i18next::init($_SESSION["lang"], 'locales/__lng__/');
        if ($c != NULL){
            $name = $c["name"];
            $moduleID = $c["moduleID"];
            $m = $c["module"][0];
        } else if ($_SESSION["deletedCourse"] == $_GET["id"]) {
            $name = Kopfwelt\i18next::getTranslation('courseDetails.deleted.title');
            $moduleID = $_SESSION["oldModuleID"];
            $modules = (new MongoDB\Client)->eva->modules;
            $m = $modules->findOne([
                '_id' => $moduleID,
                'readAccess' => $_SESSION["_id"]
            ]);
        } else {
            $name = Kopfwelt\i18next::getTranslation('error');
        }
    ?>
    
    <!--Seiteninhalt-->
    <div class="container">
        <div class="page-header pt-3">
            <div class="float-right">
                <a class="btn btn-success" href="evalCourse.php?id=<?php echo $_GET["id"]?>">
                    <span class="oi oi-bar-chart"></span>
                    <span class="translatable" data-i18n="courseDetails.viewEvaluation"></span>
                </a>
            </div>
            <h1><span class="translatable mr-3" data-i18n="courseDetails.title"></span><?php echo $name?></h1>
            <p class="translatable text-muted" data-i18n="courseDetails.description"></p>
        </div>
        
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="home.php" class="translatable" data-i18n="nav.home"></a></li>
                <li class="breadcrumb-item"><a href="moduleDetails.php?id=<?php echo $m["_id"]?>"><span class="translatable mr-1" data-i18n="moduleDetails.title"></span><?php echo $m["name"]?></a></li>
                <li class="breadcrumb-item"><a><span class="translatable mr-1" data-i18n="courseDetails.title"></span><?php echo $name?></a></li>
            </ol>
        </nav>
        
        <?php
            if ($c != NULL){
                require_once 'shared/php/helpers/databaseHelpers.php';
                
                list ($earliestYear, $latestYear) = getModuleTimespan($m["_id"]);
                $courseCount = getModuleCourseCount($m["_id"], "course");
                $lectureCount = getModuleCourseCount($m["_id"], "lecture");

                if ($c["type"] == "lecture"){
                    $ctype = Kopfwelt\i18next::getTranslation('modules.lecture');
                } else {
                    $ctype = Kopfwelt\i18next::getTranslation('modules.course');
                }

                $questionsetCount = getCourseQuestionsetCount($c["_id"]);
                $answersetCount = getCourseAnswersetCount($c["_id"]);
                
                printf('
                    <div class="card">
                        <div class="card-body">
                            <a class="full-card" href="courseDetails.php?id=%s"></a>
                            <h5 class="card-title full-card-title">%s</h5>
                            <p class="card-text small text-muted">%s</p>
                            
                            <p class="card-text"><span class="oi oi-clipboard mr-2"></span>%d <span class="translatable" data-i18n="modules.questionset_s"></span></p>
                            <p class="card-text"><span class="oi oi-check mr-2"></span>%d <span class="translatable" data-i18n="modules.answerset_s"></span></p>
                        </div>
                    </div>
                ', $c["_id"], $c["name"], $ctype, $questionsetCount, $answersetCount);

                echo '
                    <h2 class="translatable mt-3" data-i18n="courseDetails.questionsets.title"></h2>
                    <p class="translatable text-muted" data-i18n="courseDetails.questionsets.description"></p>
                    
                    <div class="accordion mb-3" id="questionsetAccordion">
                        <div class="card">
                            <div class="card-header" id="addQuestionsetHeading">
                                <h2 class="mb-0">
                                    <button class="btn btn-link translatable" type="button" data-toggle="collapse" data-target="#addQuestionsetCollapse" aria-expanded="true" aria-controls="addQuestionsetCollapse" data-i18n="courseDetails.questionsets.addQuestionset.title"></button>
                                </h2>
                            </div>
                        
                            <div id="addQuestionsetCollapse" class="collapse show" aria-labelledby="addQuestionsetHeading" data-parent="#questionsetAccordion">
                                <div class="card-body">
                                    <form method="post" name="addQuestionset">
                                        <p class="small text-muted m-0 translatable" data-i18n="courseDetails.questionsets.addQuestionset.description"></p>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <input type="text" class="form-control translatable" id="addQuestionsetName" name="addQuestionsetName" data-i18n="[placeholder]courseDetails.questionsets.addQuestionset.name">
                                            </div>
                                        </div>
                                        <div class="float-right">
                                            <button type="submit" id="addQuestionsetButton" name="addQuestionsetButton" class="btn btn-primary translatable mb-3" data-i18n="create"></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        ';

                        $questionsetCollection = (new MongoDB\Client)->eva->questionsets;
                        $questionsets = $questionsetCollection->find(
                            [
                                "courseID" => new MongoDB\BSON\ObjectId($_GET["id"]),
                                "readAccess" => $_SESSION["_id"]
                            ],
                            [
                                "sort" => [
                                    "name" => 1
                                ]
                            ]
                        );

                        foreach ($questionsets as $qs){
                            printf('
                            <div class="card">
                                <div class="card-header" id="questionset%sHeading">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed questionsetHeaderButton" type="button" data-toggle="collapse" data-target="#questionset%sCollapse" aria-expanded="false" aria-controls="questionset%sCollapse" data-questionsetid="%s" data-viewmode="view">
                                        %s
                                        </button>
                                    </h2>
                                </div>
                                <div id="questionset%sCollapse" class="collapse collapsed" aria-labelledby="questionset%sHeading" data-parent="#questionsetAccordion">
                                    <div class="card-body">
                                        <h5>%s</h5>
                                        <p class="translatable text-muted" data-i18n="courseDetails.questionsets.questionsetdescription"></p>

                                        <div class="dropdown" id="addElementDropdown" style="transform: none">
                                            <button class="btn btn-primary dropdown-toggle" type="button" id="addElementDropdownButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="oi oi-plus mr-3"></span><span class="translatable" data-i18n="courseDetails.questionsets.addElement"></span></button>
                                            <div class="dropdown-menu" aria-labelledby="addElementDropdownButton">
                                                <a class="dropdown-item translatable addQuestionButton" href="#" data-i18n="courseDetails.questionsets.elements.textfield" data-questionsetid="%s" data-questiontype="textfield"></a>
                                                <a class="dropdown-item translatable addQuestionButton" href="#" data-i18n="courseDetails.questionsets.elements.textarea" data-questionsetid="%s" data-questiontype="textarea"></a> 
                                                <a class="dropdown-item translatable addQuestionButton" href="#" data-i18n="courseDetails.questionsets.elements.multiplechoice" data-questionsetid="%s" data-questiontype="multiplechoice"></a>
                                                <a class="dropdown-item translatable addQuestionButton" href="#" data-i18n="courseDetails.questionsets.elements.multiplechoicematrix" data-questionsetid="%s" data-questiontype="multiplechoicematrix"></a>
                                                <a class="dropdown-item translatable addQuestionButton" href="#" data-i18n="courseDetails.questionsets.elements.multipleselect" data-questionsetid="%s" data-questiontype="multipleselect"></a>
                                                <a class="dropdown-item translatable addQuestionButton" href="#" data-i18n="courseDetails.questionsets.elements.slider" data-questionsetid="%s" data-questiontype="slider"></a>
                                            </div>
                                        </div>
                                        
                                        <form id="questionset%sForm" class="mt-4 mb-4 questionsetForm"></form>
                                        <div class="float-right">
                                            <button id="deleteQuestionset%sButton" type="button" class="btn btn-danger mb-3" data-toggle="modal" data-target="#deleteQuestionsetModal"><span class="oi oi-trash mr-2"></span><span class="translatable" data-i18n="courseDetails.questionsets.delete.button"></span></button>
                                        </div>
                                        <script type="text/javascript">
                                            passQuestionsetID("#deleteQuestionset%sButton", "%s");
                                        </script>
                                    </div>
                                </div>
                            </div>
                            ', $qs["_id"], $qs["_id"], $qs["_id"], $qs["_id"], $qs["name"], $qs["_id"], $qs["_id"], $qs["name"], $qs["_id"], $qs["_id"], $qs["_id"], $qs["_id"], $qs["_id"], $qs["_id"], $qs["_id"], $qs["_id"], $qs["_id"], $qs["_id"], $qs["_id"]);
                        }
            
                echo '
                    <script type="text/javascript"></script>
                    </div>
                    <!-- Send out questionsets -->
                    
                    <h2 class="translatable mt-3" data-i18n="courseDetails.sendQuestionsets.title"></h2>
                    <p class="translatable text-muted" data-i18n="courseDetails.sendQuestionsets.description"></p>
                    
                    <div class="card mb-2">
                        <div class="card-header">
                            <ul class="nav nav-tabs card-header-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active translatable" id="sendEmailsTab" data-toggle="tab" href="#sendEmails" role="tab" aria-controls="home" aria-selected="true" data-i18n="courseDetails.sendQuestionsets.sendEmails.title"></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link translatable" id="generateLinksTab" data-toggle="tab" href="#generateLinks" role="tab" aria-controls="home" data-i18n="courseDetails.sendQuestionsets.generateLinks.title"></a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <!-- Send Tokens via E-Mail -->
                            
                            <div class="card-body tab-pane fade show active" id="sendEmails" role="tabpanel" aria-labelledby="sendEmailsTab">
                                <p class="text-muted translatable" data-i18n="courseDetails.sendQuestionsets.sendEmails.description"></p>
                                <p class="text-muted translatable small" data-i18n="courseDetails.sendQuestionsets.sendEmails.privacy"></p>
                                
                                <form method="post" enctype="multipart/form-data">
                                    <div class="form-row justify-content-center">
                                        <div class="input-group form-group col-sm-12 col-md-12 col-lg-6">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text translatable" data-i18n="courseDetails.sendQuestionsets.sendEmails.uploadFile"></div>
                                            </div>
                                            <input type="file" class="form-control-sm" id="uploadEmailList" name="uploadEmailList">
                                        </div>
                                        
                                        <div class="input-group form-group col-sm-12 col-md-12 col-lg-4">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text translatable" data-i18n="courseDetails.sendQuestionsets.sendEmails.expiration"></div>
                                            </div>
                                            <input type="text" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" id="sendEmailsExpiration" name="sendEmailsExpiration">
                                        </div>
                                    </div>

                                    <div class="form-row justify-content-end">
                                        <button type="submit" class="btn btn-primary" id="sendEmailsSubmitButton" name="sendEmailsSubmitButton">
                                            <span class="oi oi-envelope-closed mr-2"></span>
                                            <span class="translatable" data-i18n="courseDetails.sendQuestionsets.sendEmails.button"></span>
                                        </button>
                                    </div>
                                </form>
                            </div>

                            <!-- Generate Token Lists -->
                            
                            <div class="card-body tab-pane fade" id="generateLinks" role="tabpanel" aria-labelledby="generateLinksTab">
                                <p class="text-muted translatable" data-i18n="courseDetails.sendQuestionsets.generateLinks.description"></p>
                                
                                <!--Stacking context needed here, because otherwise the calendar popover is behind the token list download cards-->
                                <form method="post" style="position: relative; z-index: 2;">
                                    <div class="form-row">
                                        <div class="input-group form-group col-md-8">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text translatable" data-i18n="courseDetails.sendQuestionsets.generateLinks.number"></div>
                                            </div>
                                            <input class="form-control" type="number" id="generateLinksNumber" name="generateLinksNumber">
                                        </div>
                                        <div class="input-group form-group col-md-4">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text translatable" data-i18n="courseDetails.sendQuestionsets.generateLinks.expiration"></div>
                                            </div>
                                            <input type="text" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" id="generateLinksExpiration" name="generateLinksExpiration">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary translatable float-right mb-2" id="generateLinksSubmitButton" name="generateLinksSubmitButton" data-i18n="submit"></button>
                                </form>
                                <div class="card-columns float-left" style="position: relative; z-index: 1;">
                ';

                // Display Token Lists available for Download
                foreach($c["accessTokens"] as $ac){
                    if ($ac["timestamp"] == null || $ac["expires"] == null) continue;
                    
                    // TODO: Make cards wider, less columns (requires editing bootstrap custom in SASS)
                    printf('
                                    <div class="card">
                                        <div class="card-body">
                                            <a class="full-card" style="z-index: 999;" href="api/getTokenList.php?courseID=%s&timestamp=%d"></a>
                                            <h5 class="card-title full-card-title translatable" data-i18n="courseDetails.sendQuestionsets.tokenList.download"></h5>
                                            
                                            <p class="card-text"><div><span class="oi oi-calendar mr-1"></span><span class="translatable mr-1" data-i18n="courseDetails.sendQuestionsets.tokenList.generationDate"></span></div><span>%s</span></p>
                                            <p class="card-text"><div><span class="oi oi-clock mr-1"></span><span class="translatable mr-1" data-i18n="courseDetails.sendQuestionsets.tokenList.expirationDate"></span></div><span>%s</span></p>
                                            <p class="card-text"><span class="oi oi-clipboard mr-1"></span><span class="translatable mr-1" data-i18n="courseDetails.sendQuestionsets.tokenList.linkCount"></span><span>%d</span></p>
                                        </div>
                                        <button type="button" style="position: relative; z-index: 1000;" class="btn btn-danger float-right mr-2 mb-2 deleteTokenListButton" id="deleteTokenListButton" name="deleteTokenListButton" data-toggle="modal" data-target="#deleteTokenListModal" data-courseid="%s" data-tokenlistid="%s">
                                            <span class="oi oi-trash"></span>
                                        </button>
                                    </div>
                    ', $c["_id"], $ac["timestamp"], strftime("%d-%m-%Y %H:%M:%S", $ac["timestamp"]), strftime("%d-%m-%Y %H:%M:%S", $ac["expires"]), $ac["count"], $c["_id"], $ac["timestamp"]);
                }
                
                printf('
                                </div>
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        $(document).on("click", ".deleteTokenListButton", function(eventObject){
                                            console.log(eventObject.target.dataset);
                                            $("#deleteTokenListCourseID").val(eventObject.target.dataset.courseid);
                                            $("#deleteTokenListID").val(eventObject.target.dataset.tokenlistid);
                                        });
                                    });
                                </script>
                            </div>
                        </div>
                    </div>

                    <!-- Export Data -->
                    <h2 class="translatable mt-3" data-i18n="courseDetails.exportData.title"></h2>
                    <p class="translatable text-muted" data-i18n="courseDetails.exportData.description"></p>

                    <form method="post" action="api/exportCourseData.php">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="includeAnswers" id="includeAnswers">
                            <label class="form-check-label translatable" for="includeAnswers" data-i18n="courseDetails.exportData.includeAnswers"></label>
                        </div>
                        <input type="hidden" value="%s" name="exportDataCourseID">
                        <div class="row justify-content-end">
                            <button type="submit" class="btn btn-primary mb-3" name="exportDataButton" id="exportDataButton">
                                <span class="oi oi-data-transfer-download mr-2"></span>
                                <span class="translatable" data-i18n="courseDetails.exportData.button"></span>
                            </button>
                        </div>
                    </form>

                    <hr>

                    <!-- Import Data -->
                    <h2 class="translatable mt-3" data-i18n="courseDetails.importData.title"></h2>
                    <p class="translatable text-muted" data-i18n="courseDetails.importData.description"></p>

                    <form method="post" enctype="multipart/form-data">
                        <div class="form-row justify-content-center">
                            <input type="file" name="importData" id="importData">
                        </div>
                        
                        <div class="form-row justify-content-end">
                            <button type="submit" class="btn btn-primary" id="importDataSubmitButton" name="importDataSubmitButton">
                                <span class="oi oi-data-transfer-upload mr-2"></span>
                                <span class="translatable" data-i18n="courseDetails.importData.button"></span>
                            </button>
                        </div>
                    </form>

                    <hr>

                    <h2 class="translatable" data-i18n="courseDetails.delete.pageTitle"></h2>
                    <p class="translatable text-muted" data-i18n="courseDetails.delete.pageDescription"></p>
                    
                    <div class="row justify-content-end">
                        <button type="button" class="btn btn-danger mb-3" data-toggle="modal" data-target="#deleteCourseModal">
                            <span class="oi oi-trash mr-2"></span>
                            <span class="translatable" data-i18n="courseDetails.delete.button"></span>
                        </button>
                    </div>
                ', $c["_id"]);
                        
            } else if ($_SESSION["deletedCourse"] == $_GET["id"]) {
                printf('
                    <div class="alert alert-success" role="alert">%s</div>
                ', sprintf(Kopfwelt\i18next::getTranslation('courseDetails.deleted.text'), $_SESSION["deletedQuestionsetCount"], $_SESSION["deletedAnswerTokenCount"]));
            } else {
                echo '
                <div class="alert alert-danger translatable" role="alert" data-i18n="courseDetails.error.text"></div>
                ';
            }
        ?>
</body>
</html>
