<?php
    require_once __DIR__ . '/../html/modals/loginModal.html';
    require_once __DIR__ . '/../html/modals/logoutModal.html';
    require_once __DIR__ . '/../html/modals/resetPasswordRequestModal.html';
    require_once __DIR__ . '/../html/modals/signupModal.html';
    require_once __DIR__ . '/../html/modals/welcomeModal.html';
    require_once __DIR__ . '/../html/modals/newPasswordModal.html';
    require_once __DIR__ . '/../html/modals/editProfileModal.html';
    //require_once __DIR__ . '/../html/modals/duplicateAccountModal.html';
?>
<nav class="navbar navbar-expand-lg navbar-light bg-warning"><!-- TODO: Printing navbars-->
    <a class="navbar-brand" href="home.php">Evaluationssystem</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item translatable <?php urlactive("home.php");?>">
                <a class="nav-link" href="home.php" data-i18n="nav.home">
                    <?php urlscreenreader("home.php");?>
                </a>
            </li>
        </ul>
        <div>
            <?php
            if ($_SESSION["authenticated"]){
                // We are logged in!
                printf('
                    <span class="dropdown"> <!--TODO: Fix offset of menu caused by usage of span-->
                        <a class="btn btn-info dropdown-toggle" type="button" id="profileDropdownButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="translatable" data-i18n="nav.welcome"></span>
                            <span>%s %s!</span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item btn btn-info translatable" id="logoutButton" data-toggle="modal" data-target="#logoutModal" data-i18n="nav.logout.button"></a>
                            <a class="dropdown-item btn btn-info translatable" id="editProfileButton" data-i18n="nav.editProfile.button"></a>
                        </div>
                    </span>
                ', $_SESSION["first"], $_SESSION["last"]);
            } else {
                // Not logged in
                echo '
                    <button type="button" class="btn btn-success translatable" data-toggle="modal" data-target="#loginModal" data-i18n="nav.login.button"></button>
                    <span class="navbar-text translatable" data-i18n="or"></span>
                    <button type="button" class="btn btn-info translatable" data-toggle="modal" data-target="#signupModal" data-i18n="nav.signup.button"></button>
                ';
            }
            ?>
            <div class="btn-group float-sm-right float-md-right float-lg-none float-xl-none" role="group" aria-label="Language selector">
                <button class="btn btn-primary language-button" type="button" id="languageDEButton" alt="de">
                    <img src="img/de.svg" width="30" alt="de">
                </button>
                <button class="btn btn-secondary language-button" type="button" id="languageENButton" alt="en">
                    <img src="img/en.svg" width="30" alt="en">
                </button>
            </div>
        </div>
    </div>
</nav>
