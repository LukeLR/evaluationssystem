<?php
    function urlactive($url){
        if (strpos($_SERVER['REQUEST_URI'], $url) !== false) {
            echo "active";
        }
    }
    function urlscreenreader($url){
        if (strpos($_SERVER['REQUEST_URI'], $url) !== false) {
            echo '<span class="sr-only">(aktuell)</span>';
        }
    }
?>
