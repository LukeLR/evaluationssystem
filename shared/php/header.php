<?php
    session_start();
    if (!isset($_SESSION["authenticated"])){
        require_once 'helpers/databaseHelpers.php';
        handleNewSession();
    }
    
    require_once __DIR__ . '/../html/imports.php';
    require_once __DIR__ . '/../../settings.php';
    require_once 'navigation.php';
    require_once 'cleanup.php';
    
    require_once __DIR__ . "/../../vendor/autoload.php";

    setcookie("lang", $_SESSION["lang"], time()+3600);
        
    //Event dispatcher
    if (isset($_POST['loginButton'])){
        require_once 'handlers/handleLogin.php';
        handleLogin();
    } else if (isset($_POST['logoutButton'])){
        require_once 'handlers/handleLogout.php';
        handleLogout();
    } else if (isset($_POST['signupButton'])){
        require_once 'handlers/handleSignUp.php';
        handleSignUp();
    } else if (isset($_POST["resetPasswordRequestButton"])){
        require_once 'handlers/handlePasswordResetRequest.php';
        handlePasswordResetRequest();
    } else if (isset($_POST["newPasswordButton"])){
        require_once 'handlers/handleNewPassword.php';
        handleNewPassword();
    } else if (isset($_POST["editProfileButton"])){
        require_once 'handlers/handleEditProfile.php';
        handleEditProfile();
    } else if (isset($_POST["newModuleButton"])){
        require_once 'handlers/handleNewModule.php';
        handleNewModule();
    } else if (isset($_POST["addSemesterButton"])){
        require_once 'handlers/handleAddSemester.php';
        handleAddSemester();
    } else if (isset($_POST["deleteSemesterButton"])){
        require_once 'handlers/handleDeleteSemester.php';
        handleDeleteSemester();
    } else if (isset($_POST["deleteModuleButton"])){
        require_once 'handlers/handleDeleteModule.php';
        handleDeleteModule();
    } else if (isset($_POST["addCourseButton"])){
        require_once 'handlers/handleAddCourse.php';
        handleAddCourse();
    } else if (isset($_POST["addQuestionsetButton"])){
        require_once 'handlers/handleAddQuestionset.php';
        handleAddQuestionset();
    } else if (isset($_POST["deleteQuestionsetButton"])){
        require_once 'handlers/handleDeleteQuestionset.php';
        handleDeleteQuestionset();
    } else if (isset($_POST["linkQuestionButton"])){
        require_once 'handlers/handleLinkQuestion.php';
        handleLinkQuestion();
    } else if (isset($_POST["deleteCourseButton"])){
        require_once 'handlers/handleDeleteCourse.php';
        handleDeleteCourse();
    } else if (isset($_POST["generateLinksSubmitButton"])){
        require_once 'handlers/handleGenerateLinks.php';
        handleGenerateLinks();
    } else if (isset($_POST["deleteTokenListButton"])){
        require_once 'handlers/handleDeleteTokenList.php';
        handleDeleteTokenList();
    } else if (isset($_POST["questionsetSubmitButton"])){
        require_once 'handlers/handleSubmitQuestionset.php';
        handleSubmitQuestionset();
    } else if (isset($_POST["importDataSubmitButton"])){
        require_once 'handlers/handleImportData.php';
        handleImportData();
    } else if (isset($_POST["sendEmailsSubmitButton"])){
        require_once 'handlers/handleSendEmails.php';
        handleSendEmails();
    } else if (isset($_GET["resetPasswordToken"])){
        require_once 'handlers/handlePasswordReset.php';
        handlePasswordReset();
    } else if (isset($_GET["verificationToken"])){
        require_once 'handlers/handleVerifyEmail.php';
        handleVerifyEmail();
    } else {
        #handleNewSession();
    }
    
    startCleanup();
?>
