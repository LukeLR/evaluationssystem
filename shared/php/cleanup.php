<?php
    function startCleanup($checktime = true){
        if ($checktime){
            $collection = (new MongoDB\Client)->eva->meta;
            $document = $collection->findOne(['lastcleanup' => ['$exists' => 'true']], ['sort' => ['lastcleanup' => -1]]);
            
            if ($document['lastcleanup'] + $GLOBALS['cleanupTime'] < time()){
                cleanup();
                $collection->updateOne(['lastcleanup' => ['$exists' => 'true']], ['$set' => ['lastcleanup' => time()]], ['upsert' => true]);
            }
        } else {
            cleanup();
        }
    }
    
    function cleanup(){
        cleanupResetPasswordToken();
    }
    
    function cleanupResetPasswordToken(){
        $collection = (new MongoDB\Client)->eva->passwordreset;
        $document = $collection->deleteMany(['timestamp' => ['$lt' => time() - $GLOBALS['passwordResetTokenLifetime']]]);
    }
    
    function cleanupEmailVerificationToken(){
        $collection = (new MongoDB\Client)->eva->emailverification;
        $document = $collection->deleteMany(['timestamp' => ['$lt' => time() - $GLOBALS['emailVerificationTokenLifetime']]]);
    }
?>
