<?php
    require_once 'tokenHelpers.php';
    
    function sendPasswordResetMail($mail){
        if (!isset($document["lang"])){
            $document["lang"] = "en";
        }
        Kopfwelt\i18next::init($document["lang"], 'locales/__lng__/');
        $subject = Kopfwelt\i18next::getTranslation('mail.passwordreset.subject');
        $text = Kopfwelt\i18next::getTranslation('mail.passwordreset.body');
        //$headers = "From: user@example.de"; //Override default setting from settings.php
        $headers = $GLOBALS['headers'];
        $text = $text . "\n" . $GLOBALS['external_url'] . "/home.php?resetPasswordToken=" . genPasswordResetToken($mail);
        mail($mail, $subject, $text, $headers);
    }
    
    function sendVerificationMail($oldmail, $newmail){
        if (!isset($document["lang"])){
            $document["lang"] = "en";
        }
        Kopfwelt\i18next::init($document["lang"], 'locales/__lng__/');
        $subject = Kopfwelt\i18next::getTranslation('mail.verification.subject');
        $text = Kopfwelt\i18next::getTranslation('mail.verification.body');
        //$headers = "From: user@example.de"; //Override default setting from settings.php
        $headers = $GLOBALS['headers'];
        $text = $text . "\n" . $GLOBALS['external_url'] . "/home.php?verificationToken=" . genEmailVerificationToken($oldmail, $newmail);
        mail($newmail, $subject, $text, $headers);
    }

    function sendAnswerAccessMail($salutation, $lastname, $mail, $coursename, $token){
        if (!isset($document["lang"])){
            $document["lang"] = "en";
        }
        Kopfwelt\i18next::init($document["lang"], 'locales/__lng__/');
        $subject = sprintf(Kopfwelt\i18next::getTranslation('mail.answeraccess.subject'), $coursename);
        $text = sprintf(Kopfwelt\i18next::getTranslation('mail.answeraccess.body'), $salutation, $lastname, $coursename, $GLOBALS['external_url'] . "/answer.php?token=" . $token);
        //$headers = "From: user@example.de"; //Override default setting from settings.php
        $headers = $GLOBALS['headers'];
        mail($mail, $subject, $text, $headers);
    }
?>
