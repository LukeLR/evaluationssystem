<?php
    function verifyResetPasswordToken($resetPasswordToken){
        $pwcollection = (new MongoDB\Client)->eva->passwordreset;
        $pwdocument = $pwcollection->findOne(['token' => $resetPasswordToken]);
        if ($pwdocument != NULL) {
            if ($pwdocument['timestamp'] + $GLOBALS['passwordResetTokenLifetime'] < time()) {
                return NULL;
            } else {
                return $pwdocument;
            }
        } else {
            return NULL;
        }
    }
    
    function verifyEmailVerificationToken($emailVerificationToken){
        $pwcollection = (new MongoDB\Client)->eva->emailverification;
        $pwdocument = $pwcollection->findOne(['token' => $emailVerificationToken]);
        if ($pwdocument != NULL) {
            if ($pwdocument['timestamp'] + $GLOBALS['emailVerificationTokenLifetime'] < time()) {
                return NULL;
            } else {
                return $pwdocument;
            }
        } else {
            return NULL;
        }
    }

    function genAnswerAccessToken($courseID, $timestamp, $expires){
        $token = bin2hex(random_bytes(50));
        $collection = (new MongoDB\Client)->eva->accessTokens;
        $insertOneResult = $collection->insertOne([
            "courseID" => $courseID,
            "token" => $token,
            "timestamp" => $timestamp,
            "expires" => $expires
        ]);
            return $token;
    }

    function genPasswordResetToken($mail){
        $token = bin2hex(random_bytes(50));
        $collection = (new MongoDB\Client)->eva->passwordreset;
        $insertOneResult = $collection->insertOne([
            "mail" => $mail,
            "token" => $token,
            "timestamp" => time()
        ]);
        return $token;
    }
    
    function genEmailVerificationToken($oldmail, $newmail){
        $token = bin2hex(random_bytes(50));
        $collection = (new MongoDB\Client)->eva->emailverification;
        $insertOneResult = $collection->insertOne([
            "oldmail" => $oldmail,
            "newmail" => $newmail,
            "token" => $token,
            "timestamp" => time()
        ]);
        return $token;
    }
?>
