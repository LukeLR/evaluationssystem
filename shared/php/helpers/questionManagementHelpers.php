<?php
    function addQuestion($questionID, $questionsetID, $title, $description, $type, $position, $data){
        $questionCollection = (new MongoDB\Client)->eva->questions;

        if ($questionID == "noid"){
            $insertQuestionResult = $questionCollection->insertOne(
                [
                    "questionsetID" => $questionsetID,
                    "title" => $title,
                    "description" => $description,
                    "type" => $type,
                    "position" => $position,
                    "data" => $data,
                    "linkedQuestions" => []
                ]
            );
            return $insertQuestionResult->getInsertedId();
        } else {
            $upsertQuestionResult = $questionCollection->updateOne(
                [
                    "_id" => new MongoDB\BSON\ObjectId($questionID)
                ],
                [
                    '$set' => [
                        "questionsetID" => $questionsetID,
                        "title" => $title,
                        "description" => $description,
                        "type" => $type,
                        "position" => $position,
                        "data" => $data
                    ]
                ]
            );
            return "unchanged";
        }
    }

    function deleteQuestionLink($questionID, $linkedQuestionID, $resetCounter = true){
        if ($resetCounter){
            require_once 'counterHelper.php';
            resetCounters("questionLink");
        }
        
        $_SESSION["deletedQuestionLinkCount"] += deleteQuestionLinkDatabase($questionID, $linkedQuestionID);
        $_SESSION["deletedQuestionLinkCount"] += deleteQuestionLinkDatabase($linkedQuestionID, $questionID);

        if ($resetCounter){
            require_once __DIR__ . '/../helpers/loggingHelper.php';
            logDeletion("questionLink", $questionID->__toString() . "-" . $linkedQuestionID->__toString());
        }

        return $_SESSION["deletedQuestionLinkCount"];
    }

    function deleteQuestionLinkDatabase($questionID, $linkedQuestionID){
        $questionCollection = (new MongoDB\Client)->eva->questions;
        $updateQuestionResult = $questionCollection->updateOne(
            [
                "_id" => $questionID
            ],
            [
                '$pull' => [
                    'linkedQuestions' => $linkedQuestionID
                ]
            ]
        );
    
        return $updateQuestionResult->getModifiedCount();
    }

    function deleteQuestion($questionID, $resetCounter = true){
        if ($resetCounter){
            require_once 'counterHelper.php';
            resetCounters("question");
        }
        
        $questionCollection = (new MongoDB\Client)->eva->questions;
        $question = $questionCollection->findOne(["_id" => $questionID]);

        foreach ($question["linkedQuestions"] as $linkedQuestionID){
            deleteQuestionLink($questionID, $linkedQuestionID, false);
        }
        
        $deleteQuestionResult = $questionCollection->deleteOne(["_id" => $questionID]);
    
        $_SESSION["deletedQuestionCount"] += $deleteQuestionResult->getDeletedCount();

        if ($resetCounter){
            require_once __DIR__ . '/../helpers/loggingHelper.php';
            logDeletion("question", $questionID->__toString());
        }

        return $_SESSION["deletedQuestionCount"];
    }

    function deleteAllQuestions($questionsetID, $resetCounter = true){
        if ($resetCounter){
            require_once 'counterHelper.php';
            resetCounters("question");
        }
        
        $questions = (new MongoDB\Client)->eva->questions;
        $questionCursor = $questions->find(
            [
                "questionsetID" => $questionsetID
            ]
        );

        foreach ($questionCursor as $qc){
            deleteQuestion($qc["_id"], false);
        }

        if ($resetCounter){
            require_once __DIR__ . '/../helpers/loggingHelper.php';
            logDeletion("questionset", $questionsetID->__toString());
        }
    }
?>
