<?php
    function resetCounters($highestLevel){
        switch($highestLevel){
            /* The break statements in each switch case are ommited on purpose,
             * so when processing the switch block, the execution falls through
             * all lower cases to reset all lower counters as well.
             */
            case "module": $_SESSION["deletedModuleCount"] = 0;
            case "semester": $_SESSION["deletedSemesterCount"] = 0;
            case "course": $_SESSION["deletedCourseCount"] = 0;
            case "answerToken": $_SESSION["deletedAnswerTokenCount"] = 0;
            case "questionset": $_SESSION["deletedQuestionsetCount"] = 0;
            case "question": $_SESSION["deletedQuestionCount"] = 0;
            case "questionLink": $_SESSION["deletedQuestionLinkCount"] = 0; break;
            default: break;
        }
    }
?>
