<?php
    function readFromDatabase($mail){
        $collection = (new MongoDB\Client)->eva->users;
        $document = $collection->findOne(['mail' => $mail]);
        $_SESSION["_id"] = $document["_id"];
        $_SESSION["first"] = $document["first"];
        $_SESSION["last"] = $document["last"];
        $_SESSION["title"] = $document["title"];
        $_SESSION["lang"] = $document["lang"];
        $_SESSION["mail"] = $document["mail"];
        $_SESSION["mailverified"] = $document["mailverified"];
        $_SESSION["authenticated"] = true;
    }
    
    function writeToDatabase(){
        $collection = (new MongoDB\Client)->eva->users;
        $document = $collection->findOne(['mail' => $_SESSION["mail"]]);
        if ($document != NULL){
            $updateArray["first"] = $_SESSION["first"];
            $updateArray["last"] = $_SESSION["last"];
            $updateArray["title"] = $_SESSION["title"];
            $updateArray["lang"] = $_SESSION["lang"];
            $updateArray["mail"] = $_SESSION["mail"];
            $updateArray["mailverified"] = $_SESSION["mailverified"];
            $updateOneResult = $collection->updateOne(['_id' => $document['_id']], ['$set' => $updateArray]);
        } else {
            # No matches
        }
    }

    function handleNewSession(){
        $_SESSION["_id"] = "guest";
        $_SESSION["first"] = "";
        $_SESSION["last"] = "";
        $_SESSION["title"] = "";
        $_SESSION["lang"] = "en";
        $_SESSION["mail"] = "";
        $_SESSION["mailverified"] = "";
        $_SESSION["authenticated"] = "";
    }

    function getModuleTimespan($moduleID){
        $earliestYear = 0;
        $latestYear = 0;
        
        $moduleCollection = (new MongoDB\Client)->eva->modules;
        $timespanCursor = $moduleCollection->aggregate(
            [
                [
                    '$match' => [
                        '_id' => $moduleID
                    ]
                ],
                [
                    '$project' => [
                        'earliestYear' => [
                            '$min' => '$semesters.year'
                        ],
                        'latestYear' => [
                            '$max' => '$semesters.year'
                        ]
                    ]
                ]
            ]
        );
        foreach ($timespanCursor as $cc){
            /* $timespanCursor should only contain one item, but using a foreach loop seems to be the
             * easiest way to get an individual mongoDB document out of a cursor object
             */
            $earliestYear = $cc["earliestYear"];
            $latestYear = $cc["latestYear"];
        }

        return array($earliestYear, $latestYear);
    }

    function getModuleCourseCount($moduleID, $courseType){
        $courseCount = 0;
        
        $courseCollection = (new MongoDB\Client)->eva->courses;
        $courseCountCursor = $courseCollection->aggregate(
            [
                [
                    '$match' => [
                        'moduleID' => $moduleID,
                        'type' => $courseType,
                        'readAccess' => $_SESSION["_id"]
                    ]
                ],
                [
                    '$group' => [
                        '_id' => '$moduleID',
                        'count' => ['$sum' => 1]
                    ]
                ]
            ]
        );

        foreach ($courseCountCursor as $cc){
            /* $courseCountCursor should only contain one item, but using a foreach loop seems to be the
             * easiest way to get an individual mongoDB document out of a cursor object
             */
            $courseCount = $cc["count"];
        }
        return $courseCount;
    }

    function getModuleQuestionsetCount($moduleID){
        $questionsetCount = 0;
        
        $courseCollection = (new MongoDB\Client)->eva->courses;
        $questionsetCountCursor = $courseCollection->aggregate(
            [
                [
                    '$match' => [
                        'moduleID' => $moduleID,
                        'readAccess' => $_SESSION["_id"]
                    ]
                ],
                [
                    '$lookup' => [
                        'from' => 'questionsets',
                        'localField' => '_id',
                        'foreignField' => 'courseID',
                        'as' => 'questionsets'
                    ]
                ],
                [
                    '$match' => [
                        'questionsets._id' => [
                            '$exists' => true
                        ]
                    ]
                ],
                [
                    '$project' => [
                        'count' => [
                            '$size' => '$questionsets'
                        ]
                    ]
                ],
                [
                    '$group' => [
                        '_id' => '$moduleID',
                        'count' => ['$sum' => '$count']
                    ]
                ]
            ]
        );

        foreach ($questionsetCountCursor as $qcc){
            /* $courseCountCursor should only contain one item, but using a foreach loop seems to be the
             * easiest way to get an individual mongoDB document out of a cursor object
             */
            $questionsetCount = $qcc["count"];
        }
        return $questionsetCount;
    }

    function getCourseQuestionsetCount($courseID){
        $questionsetCount = 0;

        $questionsetCollection = (new MongoDB\Client)->eva->questionsets;
        $questionsetCountCursor = $questionsetCollection->aggregate(
            [
                [
                    '$match' => [
                        'courseID' => $courseID,
                        'readAccess' => $_SESSION["_id"]
                    ]
                ],
                [
                    '$group' => [
                        '_id' => '$courseID',
                        'count' => ['$sum' => 1]
                    ]
                ]
            ]
        );

        foreach ($questionsetCountCursor as $qcc){
            /* $courseCountCursor should only contain one item, but using a foreach loop seems to be the
             * easiest way to get an individual mongoDB document out of a cursor object
             */
            $questionsetCount = $qcc["count"];
        }
        
        return $questionsetCount;
    }

    function getCourseAnswersetCount($courseID){
        $answersetCount = 0;
        
        $questionsets = (new MongoDB\Client)->eva->questionsets;
        $answersetCountCursor = $questionsets->aggregate(
            [
                [
                    '$match' => [
                        'courseID' => $courseID,
                        'readAccess' => $_SESSION["_id"]
                    ]
                ], // Find all questionsets that belong to that course
                [
                    '$lookup' => [
                        'from' => 'answersets',
                        'localField' => '_id',
                        'foreignField' => 'questionsetID',
                        'as' => 'answersets'
                    ]
                ], // Add all answersets to each questionset
                [
                    '$unwind' => '$answersets'
                ], /* Instead of an array with multiple answersets per questionset,
                    * unwinding duplicates the entries creating duplicate questionset
                    * documents with one answerset each
                    */
                [
                    '$group' => [
                        '_id' => '_id',
                        'answersetCount' => ['$sum' => 1]
                    ]
                ] /* Re-group all documents by the questionsetID, counting how many
                   * duplicates of each questionset document exist (equal to the
                   * number of answersets these questionset document had, since
                   * they have been unwinded in the previous stage
                   */
            ]
        );

        foreach ($answersetCountCursor as $acc){
            // Sum up all answerset counts for each questionset
            $answersetCount += $acc["answersetCount"];
        }

        return $answersetCount;
    }

    function getModuleAnswersetCount($moduleID){
        $answersetCount = 0;

        $courses = (new MongoDB\Client)->eva->courses;
        $answersetCountCursor = $courses->aggregate(
            [
                [
                    '$match' => [
                        'moduleID' => $moduleID,
                        'readAccess' => $_SESSION["_id"]
                    ]
                ], // Find all modules that belong to that course
                [
                    '$lookup' => [
                        'from' => 'questionsets',
                        'localField' => '_id',
                        'foreignField' => 'courseID',
                        'as' => 'questionsets'
                    ]
                ], // Find all questionsets that belong to each course
                [
                    '$unwind' => '$questionsets'
                ], /* Unwind the questionset array, so instead of having
                    * an array with multiple questionsets per course, create
                    * as many duplicates of each course as there are
                    * questionsets belonging to it, with one questionset
                    * each
                    */
                [
                    '$lookup' => [
                        'from' => 'answersets',
                        'localField' => 'questionsets._id',
                        'foreignField' => 'questionsetID',
                        'as' => 'answersets'
                    ]
                ], // Add all answersets to each questionset
                [
                    '$unwind' => '$answersets'
                ], /* Instead of an array with multiple answersets per questionset,
                    * unwinding duplicates the entries creating duplicate questionset
                    * documents with one answerset each
                    */
                [
                    '$group' => [
                        '_id' => '_id',
                        'answersetCount' => ['$sum' => 1]
                    ]
                ] /* Re-group all documents by the questionsetID, counting how many
                   * duplicates of each questionset document exist (equal to the
                   * number of answersets these questionset document had, since
                   * they have been unwinded in the previous stage
                   */
            ]
        );

        foreach ($answersetCountCursor as $acc){
            // Sum up all answerset counts for each questionset
            $answersetCount += $acc["answersetCount"];
        }

        return $answersetCount;
    }
?>
