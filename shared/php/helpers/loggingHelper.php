<?php
    function logDeletion($level, $toplevelID){
        $logString = "Deleted " . $level . "_" . $toplevelID . ":";
        switch ($level){
            /* The break statements in each switch case are ommited on purpose,
             * so when processing the switch block, the execution falls through
             * all lower cases so all lower counters are logged as well.
             */
            case "module": $logString .= " modules: " . $_SESSION["deletedModuleCount"];
            case "semester": $logString .= " semesters: " . $_SESSION["deletedSemesterCount"];
            case "course": $logString .= " courses: " . $_SESSION["deletedCourseCount"];
            case "answerToken": $logString .= " answerTokens: " . $_SESSION["deletedAnswerTokenCount"];
            case "questionset": $logString .= " questionsets: " . $_SESSION["deletedQuestionsetCount"];
            case "question": $logString .= " questions: " . $_SESSION["deletedQuestionCount"];
            case "questionLink": $logString .= " questionLinks: " . $_SESSION["deletedQuestionLinkCount"]; break;
            default: break;
        }
        error_log($logString);
    }
?>
