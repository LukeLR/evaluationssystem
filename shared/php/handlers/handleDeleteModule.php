<?php
    require_once 'handleDeleteCourse.php';

    function handleDeleteModule(){
        deleteModule(new MongoDB\BSON\ObjectId($_GET["id"]), true);
    }
    
    function deleteModule($moduleID, $resetCounter = false){
        if ($resetCounter){
            require_once __DIR__ . '/../helpers/counterHelper.php';
            resetCounters("module");
        }
        
        $modules = (new MongoDB\Client)->eva->modules;
        $deleteModuleResult = $modules->deleteOne([
            "_id" => $moduleID,
            "writeAccess" => $_SESSION["_id"]
        ]);
        
        if($deleteModuleResult->getDeletedCount() == 1){
            $_SESSION["deletedModuleCount"] += 1;
            
            $courses = (new MongoDB\Client)->eva->courses;
            $courseCursor = $courses->find([
                "moduleID" => $moduleID,
                "writeAccess" => $_SESSION["_id"]
            ]);

            foreach($courseCursor as $cc){
                deleteCourse($cc["_id"], false);
            }
            
            $_SESSION["deletedModule"] = $_GET["id"];
        }

        if ($resetCounter){
            require_once __DIR__ . '/../helpers/loggingHelper.php';
            logDeletion("module", $moduleID->__toString());
        }
    }
?>
