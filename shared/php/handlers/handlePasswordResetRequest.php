<?php
    require_once __DIR__ . '/../helpers/eMailHelpers.php';
    require_once __DIR__ . '/../../html/modals/resetPasswordConfirmModal.html';
    require_once __DIR__ . '/../../html/modals/resetPasswordErrorModal.html';
    
    function handlePasswordResetRequest(){
        $collection = (new MongoDB\Client)->eva->users;
        $document = $collection->findOne(['mail' => $_POST["resetPasswordRequestEmail"]]);
        if ($document != NULL) {
            // E-Mail Address provided is valid
            sendPasswordResetMail($_POST["resetPasswordRequestEmail"]);
            echo '
            <script type="text/javascript">
                showModal("#resetPasswordConfirmModal");
            </script>
            ';
        } else {
            echo '
            <script type="text/javascript">
                showModal("#resetPasswordErrorModal");
            </script>
            ';
        }
    }
?>
