<?php
    function handleLinkQuestion(){
        linkQuestion(new MongoDB\BSON\ObjectId($_POST["question"]), new MongoDB\BSON\ObjectId($_POST["questionSelect"]));
    }

    function linkQuestion($firstQuestion, $secondQuestion){
        linkQuestionDatabase($firstQuestion, $secondQuestion);
        linkQuestionDatabase($secondQuestion, $firstQuestion);
    }

    function linkQuestionDatabase($firstQuestion, $secondQuestion){
        $questions = (new MongoDB\Client)->eva->questions;
        $updateQuestionResult = $questions->updateOne(
            [
                "_id" => $firstQuestion
            ],
            [
                '$addToSet' => [
                    'linkedQuestions' => $secondQuestion
                ]
            ]
        );
    }
?>
