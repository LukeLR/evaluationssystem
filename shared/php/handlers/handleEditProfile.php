<?php
    require_once __DIR__ . '/../helpers/accountDataHelpers.php';
    require_once __DIR__ . '/../helpers/databaseHelpers.php';
    require_once __DIR__ . '/../../html/modals/unknownErrorModal.html';
    require_once __DIR__ . '/../../html/modals/editProfileSuccessModal.html';
    require_once __DIR__ . '/../../html/modals/verifyEmailModal.html';
    
    function handleEditProfile(){
        $updateArray = [];
        if (validateSignUpData($_POST)){
            $collection = (new MongoDB\Client)->eva->users;
            $document = $collection->findOne(['mail' => $_SESSION["mail"]]);
            if ($document != NULL){
                $updateArray['title'] = $_POST['editProfileTitle'];
                $updateArray['lang'] = $_POST['editProfileLanguage'];
                $updateArray['first'] = $_POST['editProfileFirstName'];
                $updateArray['last'] = $_POST['editProfileLastName'];
                
                # Check if password has changed
                if ($_POST['editProfilePassword'] != "") {
                    # Password has changed
                    if (validatePassword($_POST['editProfilePassword'], $_POST['editProfilePasswordConfirm'])){
                        $updateArray['pwd'] = hashPassword($_POST["editProfilePassword"]);
                    }
                }
                
                # Check if Email has changed
                if ($_POST['editProfileEmail'] != $document['mail']){
                    # E-Mail has changed
                    sendVerificationMail($document['mail'], $_POST['editProfileEmail']);
                    $updateArray['mailverified'] = false;
                }
                
                # Atomically apply changes
                $updateOneResult = $collection->updateOne(['_id' => $document['_id']], ['$set' => $updateArray]);
                
                if ($updateOneResult->getMatchedCount() == 1){
                    readFromDatabase($document['mail']); # Refresh data in $_SESSION
                    
                    if ($_POST['editProfileEmail'] != $document['mail']){
                        echo '
                        Test!
                        <script type="text/javascript">
                            showModal("#verifyEmailModal");
                            $(document).ready(function() {
                            });
                        </script>
                        ';
                    }
                    echo '
                    <script type="text/javascript">
                        showModal("#editProfileSuccessModal");
                    </script>
                    ';
                } else {
                    # Unknown error occured
                    echo '
                    <script type="text/javascript">
                        showModal("#unknownErrorModal");
                    </script>
                    ';
                }
            } else {
                # User Account not found
                echo '
                error!
                <script type="text/javascript">
                    showModal("#unknownErrorModal");
                </script>
                ';
            }
        }
    }
?>
