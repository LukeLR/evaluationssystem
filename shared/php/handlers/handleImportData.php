<?php
    function handleImportData(){
        require_once 'handleAddQuestionset.php';
        require_once 'handleLinkQuestion.php';
        require_once __DIR__ . '/../helpers/questionManagementHelpers.php';
        
        if (is_uploaded_file($_FILES['importData']['tmp_name'])){
            $contents = file_get_contents($_FILES['importData']['tmp_name']);
            $contents = utf8_encode($contents);
            $contents = str_replace('$oid', 'oid', $contents);
            $json = json_decode($contents);

            // Add questionsets
            foreach($json->questionsets as $questionset){
                $currentID = $questionset->_id->oid;
                $newQuestionsetID = addQuestionset(new MongoDB\BSON\ObjectId($_GET["id"]), $questionset->name, $_SESSION["_id"], $_SESSION["_id"]);

                // Add questions
                foreach ($json->questions as $questionarray){
                    foreach($questionarray as $question){
                        $oldID = $question->questionsetID->oid;

                        if ($oldID == $currentID){
                            // Question belongs to the currently processed questionset
                            $oldQuestionID = new MongoDB\BSON\ObjectID($question->_id->oid);
                            $newQuestionID = addQuestion("noid", $newQuestionsetID, $question->title, $question->description, $question->type, $question->position, $question->data);
                            linkQuestion($newQuestionID, $oldQuestionID);
                        }
                    }
                }
            }
        }
    }
?>
