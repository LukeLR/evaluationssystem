<?php
    function handleAddCourse(){
        $courses = (new MongoDB\Client)->eva->courses;
        $insertCourseResult = $courses->insertOne(
            [
                "moduleID" => new MongoDB\BSON\ObjectId($_GET["id"]),
                "name" => $_POST["addCourseName"],
                "type" => $_POST["addCourseCourseType"],
                "semester" => [
                    "type" => $_POST["addCourseSemesterType"],
                    "year" => (int)$_POST["addCourseSemesterYear"]
                ],
                "readAccess" => [
                    $_SESSION["_id"]
                ],
                "writeAccess" => [
                    $_SESSION["_id"]
                ]
            ]
        );
    }
?>
