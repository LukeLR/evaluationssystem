<?php
    require_once __DIR__ . '/../helpers/verifyTokenHelpers.php';
    require_once __DIR__ . '/../../html/modals/passwordResetTokenExpireModal.html';
    require_once __DIR__ . '/../../html/modals/unknownErrorModal.html';
    require_once __DIR__ . '/../../html/modals/verifyEmailSuccessModal.html';
    
    function handleVerifyEmail(){
        $pwdocument = verifyEmailVerificationToken($_GET['verificationToken']);
        if ($pwdocument != NULL) {
            $usercollection = (new MongoDB\Client)->eva->users;
            $userdocument = $usercollection->findOne(['mail' => $pwdocument['oldmail']]);
            if ($userdocument != NULL){
                # Update E-Mail
                $updateResult = $usercollection->updateOne(
                    ['mail' => $pwdocument['oldmail']],
                    ['$set' => ['mailverified' => true,
                    'mail' => $pwdocument['newmail']]]
                );
                if ($updateResult->getModifiedCount() != 1){
                    echo '
                    <script type="text/javascript">
                        showModal("#unknownErrorModal");
                    </script>
                    ';
                } else {
                    $_SESSION["mail"] = $pwdocument['newmail'];
                    echo '
                    <script type="text/javascript">
                        showModal("#verifyEmailSuccessModal");
                    </script>
                    ';
                }
            } else {
                # No account with the mail address of the password reset token found
                echo '
                <script type="text/javascript">
                    showModal("#unknownErrorModal");
                </script>
                ';
            }
        } else {
            # Email verification token provided by user is unknown
            echo '
            <script type="text/javascript">
                showModal("#passwordResetTokenExpireModal");
            </script>
            ';
        }
    }
?>
