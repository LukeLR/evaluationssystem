<?php
    function handleAddSemester(){
        $modules = (new MongoDB\Client)->eva->modules;
        $updateModuleResult = $modules->updateOne(
            ["_id" => new MongoDB\BSON\ObjectId($_GET["id"]), "writeAccess" => $_SESSION["_id"]],
            [
                '$addToSet' => [
                    "semesters" => [
                        '$each' => [["type" => $_POST["addSemesterSemester"], "year" => (int)$_POST["addSemesterYear"]]],
                    ],
                ]
            ]
        );
        $sortModuleResult = $modules->updateOne(
            ["_id" => new MongoDB\BSON\ObjectId($_GET["id"]), "writeAccess" => $_SESSION["_id"]],
            [
                '$push' => [
                    "semesters" => [
                        '$each' => [],
                        '$sort' => [
                            'year' => 1,
                            'type' => 1
                        ]
                    ],
                ]
            ]
        );
    }
?>
