<?php
    require_once __DIR__ . '/../helpers/databaseHelpers.php';
    require_once __DIR__ . '/../../html/modals/goodbyeModal.html';
    
    function handleLogout(){
        handleNewSession();
        echo '
        <script type="text/javascript">
            showModal("#goodbyeModal");
        </script>
        ';
    }
?>
