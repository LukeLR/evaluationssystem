<?php
    require_once __DIR__ . '/../helpers/questionManagementHelpers.php';

    function handleDeleteQuestionset(){
        deleteQuestionset(new MongoDB\BSON\ObjectId($_POST["deleteQuestionsetID"]), new MongoDB\BSON\ObjectId($_GET["id"]), true);
    }

    function deleteQuestionset($questionsetID, $courseID, $resetCounter = true){
        if ($resetCounter){
            require_once __DIR__ . '/../helpers/counterHelper.php';
            resetCounters("questionset");
        }

        deleteAllQuestions($questionsetID, false);
        
        $questionsets = (new MongoDB\Client)->eva->questionsets;
        $deleteQuestionsetResult = $questionsets->deleteOne(
            [
                "courseID" => $courseID,
                "_id" => $questionsetID,
                "writeAccess" => $_SESSION["_id"]
            ]
        );

        $_SESSION["deletedQuestionsetCount"] += 1;

        if ($resetCounter){
            require_once __DIR__ . '/../helpers/loggingHelper.php';
            logDeletion("questionset", $questionsetID->__toString());
        }
    }
?>
