<?php
    function handleSubmitQuestionset(){
        $accessTokens = (new MongoDB\Client)->eva->accessTokens;
        $questions = (new MongoDB\Client)->eva->questions;
        $accessToken = $accessTokens->findOne(["token" => $_GET["token"]]);

        $answersetArray = ["timestamp" => new MongoDB\BSON\UTCDateTime(time()*1000)];

        foreach(array_keys($_POST) as $p){
            // Filter the $_POST-array for valid questionIDs and answers
            try {
                $questionID = new MongoDB\BSON\ObjectID($p);
                $question = $questions->findOne(["_id" => $questionID]);
                if ($question != null){
                    $answersetArray[$p] = $_POST[$p];
                }
            } catch (MongoDB\Driver\Exception\InvalidArgumentException $e){
                
            }
        }

        if ($accessToken != null){
            // Access token is valid
            $answersets = (new MongoDB\Client)->eva->answersets;

            $_POST["questionsetID"] = new MongoDB\BSON\ObjectID($_POST["questionsetID"]);

            $findAnswersetResult = $answersets->updateOne(
                [
                    "token" => $_GET["token"],
                    "questionsetID" => $_POST["questionsetID"],
                ],
                [
                    '$set' => $answersetArray
                ],
                [
                    'upsert' => true
                ]
            );
        }
    }
?>
