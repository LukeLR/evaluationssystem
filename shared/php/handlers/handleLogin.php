<?php
    require_once __DIR__ . '/../helpers/accountDataHelpers.php';
    require_once __DIR__ . '/../helpers/databaseHelpers.php';
    require_once __DIR__ . '/../../html/modals/loginModal.html';
    require_once __DIR__ . '/../../html/modals/verifyEmailModal.html';
    
    function handleLogin(){
        if (validateLoginData($_POST)){
            $collection = (new MongoDB\Client)->eva->users;
            $document = $collection->findOne(['mail' => $_POST["loginEmail"]]);
            if ($document != NULL) {
                if (password_verify($_POST["loginPassword"], $document["pwd"])){
                    readFromDatabase($document['mail']);
                    if ($_SESSION["mailverified"]){
                        echo '
                        <script type="text/javascript">
                            showModal("#welcomeModal");
                        </script>
                        ';
                    } else {
                        echo '
                        <script type="text/javascript">
                            showModal("#verifyEmailModal");
                        </script>
                        ';
                    }
                    echo '
                    <script type="text/javascript">
                        $(document).ready(function(){
                            i18next.changeLanguage("' . $_SESSION["lang"] . '").then(localize);
                        });
                    </script>
                    ';
                } else {
                    echo '
                    <script type="text/javascript">
                        $(document).ready(function(){
                            shakeLoginModal();
                        });
                    </script>
                    ';
                }
            } else {
                echo '
                <script type="text/javascript">
                    shakeLoginModal();
                </script>
                ';
            }
        }
    }
?>
