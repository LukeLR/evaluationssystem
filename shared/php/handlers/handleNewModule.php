<?php
    function handleNewModule(){
        $modules = (new MongoDB\Client)->eva->modules;
        $insertModuleResult = $modules->insertOne([
            "name" => $_POST["newModuleName"],
            "readAccess" => [$_SESSION["_id"]],
            "writeAccess" => [$_SESSION["_id"]]
        ]);
    }
?>
