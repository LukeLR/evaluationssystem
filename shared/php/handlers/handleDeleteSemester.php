<?php
    function handleDeleteSemester(){
        deleteSemester(new MongoDB\BSON\ObjectId($_GET["id"]), $_POST["deleteSemesterType"], $_POST["deleteSemesterYear"]);
    }

    function deleteSemester($moduleID, $semesterType, $semesterYear, $resetCounter = true){
        if ($resetCounter){
            require_once __DIR__ . '/../helpers/counterHelper.php';
            resetCounters("semester");
        }
        
        $modules = (new MongoDB\Client)->eva->modules;
        $updateModuleResult = $modules->updateOne(
            [
                "_id" => $moduleID,
                "writeAccess" => $_SESSION["_id"]],
            [
                '$pullAll' => [
                    "semesters" => [
                        [
                            "type" => $semesterType,
                            "year" => (int)$semesterYear
                        ]
                    ],
                ]
            ]
        );

        if ($updateModuleResult->getModifiedCount() == 1){
            $_SESSION["deletedSemesterCount"] += 1;
            require_once 'handleDeleteCourse.php';
            
            $courses = (new MongoDB\Client)->eva->courses;
            $courseCursor = $courses->find([
                "moduleID" => new MongoDB\BSON\ObjectId($_GET["id"]),
                "writeAccess" => $_SESSION["_id"],
                "semester.type" => $semesterType,
                "semester.year" => $semesterYear
            ]);

            foreach ($courseCursor as $cc){
                deleteCourse($cc["_id"], false);
            }
        }

        if ($resetCounter){
            require_once __DIR__ . '/../helpers/loggingHelper.php';
            logDeletion("semester", $moduleID->__toString() . "-" . $semesterType . $semesterYear);
        }
    }
?>
