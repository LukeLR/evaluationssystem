<?php
    require_once __DIR__ . '/../helpers/tokenHelpers.php';
    
    function handleGenerateLinks(){
        $courseID = new MongoDB\BSON\ObjectId($_GET["id"]);
        $timestamp = time();
        $expires = strtotime($_POST["generateLinksExpiration"]);

        $courses = (new MongoDB\Client)->eva->courses;
        $course = $courses->findOne(["_id" => $courseID, "writeAccess" => $_SESSION["_id"]]);

        if ($course != null){
            // Course exists and user as write access to it
            for ($i = 0; $i < (int)$_POST["generateLinksNumber"]; $i++){
                genAnswerAccessToken($courseID, $timestamp, $expires);
            }
        }
    }
?>
