<?php
    require_once __DIR__ . '/../helpers/tokenHelpers.php';
    require_once __DIR__ . '/../../html/modals/passwordResetTokenExpireModal.html';
    require_once __DIR__ . '/../../html/modals/unknownErrorModal.html';
    
    function handlePasswordReset(){
        $pwdocument = verifyResetPasswordToken($_GET['resetPasswordToken']);
        if ($pwdocument != NULL) {
            $usercollection = (new MongoDB\Client)->eva->users;
            $userdocument = $usercollection->findOne(['mail' => $pwdocument["mail"]]);
            if ($userdocument != NULL) {
                echo '
                <script type="text/javascript">
                    showNewPasswordModal("' . $pwdocument["token"] . '");
                </script>
                ';
            } else {
                # No account with the mail address of the password reset token found
                echo '
                <script type="text/javascript">
                    showModal("#unknownErrorModal");
                </script>
                ';
            }
        } else {
            # Password reset token provided by user is unknown
            echo '
            <script type="text/javascript">
                showModal("#passwordResetTokenExpireModal");
            </script>
            ';
        }
    }
?>
