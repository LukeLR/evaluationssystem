<?php
    function handleDeleteCourse(){
        deleteCourse(new MongoDB\BSON\ObjectId($_POST["courseID"]), true);
    }

    function deleteCourse($courseID, $resetCounter = true){
        if ($resetCounter){
            require_once __DIR__ . '/../helpers/counterHelper.php';
            resetCounters("course");
        }
        
        $courses = (new MongoDB\Client)->eva->courses;
        $course = $courses->findOne(["_id" => $courseID]);

        //Backup moduleID for display / breadcrumb on deleted course page
        $_SESSION["oldModuleID"] = $course["moduleID"];

        $deleteCourseResult = $courses->deleteOne(["_id" => $courseID]);

        if ($deleteCourseResult->getDeletedCount() == 1){
            require_once 'handleDeleteQuestionset.php';
            
            $_SESSION["deletedCourseCount"] += 1;
            $_SESSION["deletedCourse"] = $_POST["courseID"];
            
            $questionsets = (new MongoDB\Client)->eva->questionsets;
            $questionsetCursor = $questionsets->find(["courseID" => $courseID]);

            foreach ($questionsetCursor as $qc){
                deleteQuestionset($qc["_id"], $courseID, false);
            }
            
            $accessTokens = (new MongoDB\Client)->eva->accessTokens;
            $deleteTokenResult = $accessTokens->deleteMany(["courseID" => $courseID]);

            $_SESSION["deletedAnswerTokenCount"] += $deleteTokenResult->getDeletedCount();
        }

        if ($resetCounter){
            require_once __DIR__ . '/../helpers/loggingHelper.php';
            logDeletion("course", $courseID->__toString());
        }
    }
?>
