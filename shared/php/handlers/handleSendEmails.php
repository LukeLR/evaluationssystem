<?php
    require_once __DIR__ . '/../helpers/tokenHelpers.php';
    require_once __DIR__ . '/../helpers/eMailHelpers.php';
    
    function handleSendEmails(){
        if (is_uploaded_file($_FILES['uploadEmailList']['tmp_name'])){
            $courseID = new MongoDB\BSON\ObjectID($_GET["id"]);
            $courses = (new MongoDB\Client)->eva->courses;
            $course = $courses->findOne(["_id" => $courseID, "writeAccess" => $_SESSION["_id"]]);

            if ($course != null){
                // Course exists and user as write access to it
                $timestamp = time();
                $expires = strtotime($_POST["sendEmailsExpiration"]);

                $file = fopen($_FILES['uploadEmailList']['tmp_name'], "r");
                
                while (($fields = fgetcsv($file)) != false){
                    if (filter_var($fields[2], FILTER_VALIDATE_EMAIL)) {
                        $token = genAnswerAccessToken($course["_id"], $timestamp, $expires);
                        sendAnswerAccessMail($fields[0], $fields[1], $fields[2], $course["name"], $token);
                    }
                }

                fclose($file);
            }
        }

        try {
            unlink($_FILES['uploadEmailList']['tmp_name']);
        } catch (Exception $e) {
            error_log("Warning! File " . $_FILES['uploadEmailList']['tmp_name'] . " could not be deleted!");
        }
    }
?>
