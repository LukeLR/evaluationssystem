<?php
    function handleAddQuestionset(){
        addQuestionset(new MongoDB\BSON\ObjectId($_GET["id"]), $_POST["addQuestionsetName"], $_SESSION["_id"], $_SESSION["_id"]);
    }

    function addQuestionset($courseID, $name, $readAccess, $writeAccess){
        $questionsets = (new MongoDB\Client)->eva->questionsets;
        $insertQuestionsetResult = $questionsets->insertOne(
            [
                "courseID" => $courseID,
                "name" => $name,
                "readAccess" => [$readAccess],
                "writeAccess" => [$writeAccess]
            ]
        );
        return $insertQuestionsetResult->getInsertedId();
    }
?>
