<?php
    //namespace eva;
    
    require_once __DIR__ . '/../helpers/accountDataHelpers.php';
    require_once __DIR__ . '/../helpers/eMailHelpers.php';
    require_once __DIR__ . '/../../html/modals/duplicateAccountModal.html';
    require_once __DIR__ . '/../../html/modals/newAccountModal.html';
    
    function handleSignUp(){
        if (validateSignUpData($_POST)){
            try {
                $collection = (new MongoDB\Client)->eva->users;
                $insertOneResult = $collection->insertOne([
                    "title" => $_POST["signupTitle"],
                    "lang"  => $_POST["signupLanguage"],
                    "first" => $_POST["signupFirstName"],
                    "last"  => $_POST["signupLastName"],
                    "mail"  => $_POST["signupEmail"],
                    "mailverified" => false,
                    "pwd"   => hashPassword($_POST["signupPassword"])
                ]);
                //TODO: Differ between new account and E-Mail change in verifying E-Mails
                sendVerificationMail($_POST["signupEmail"], $_POST["signupEmail"]);
                echo '
                <script type="text/javascript">
                    showModal("#newAccountModal");
                </script>
                ';
            } catch (MongoDB\Driver\Exception\BulkWriteException $e){
                echo '
                <script type="text/javascript">
                    showModal("#duplicateAccountModal");
                </script>
                ';
            }
        }
    }
?>
