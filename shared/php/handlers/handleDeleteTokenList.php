<?php
    function handleDeleteTokenList(){
        deleteTokenList(new MongoDB\BSON\ObjectID($_POST["deleteTokenListCourseID"]), (int)$_POST["deleteTokenListID"]);
    }

    function deleteTokenList($courseID, $timestamp, $resetCounter = true){
        if ($resetCounter){
            require_once __DIR__ . '/../helpers/counterHelper.php';
            resetCounters("answerToken");
        }
        
        $courses = (new MongoDB\Client)->eva->courses;
        $updateCourseResult = $courses->updateOne(
            [
                "_id" => $courseID,
                "writeAccess" => $_SESSION["_id"]
            ],
            [
                '$pull' => [
                    "tokenLists" => [
                        "timestamp" => $timestamp
                    ]
                ]
            ]
        );
    
        if ($updateCourseResult->getModifiedCount() == 1){
            $accessTokens = (new MongoDB\Client)->eva->accessTokens;
            $deleteTokenResult = $accessTokens->deleteMany(
                [
                    "courseID" => $courseID,
                    "timestamp" => $timestamp
                ]
            );
            $_SESSION["deletedAnswerTokenCount"] += $deleteTokenResult->getDeletedCount();
        }

        if ($resetCounter){
            require_once __DIR__ . '/../helpers/loggingHelper.php';
            logDeletion("answerToken", $courseID->__toString() . "-" . $timestamp);
        }
    }
?>
