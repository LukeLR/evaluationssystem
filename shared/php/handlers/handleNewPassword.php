<?php
    require_once __DIR__ . '/../helpers/accountDataHelpers.php';
    require_once __DIR__ . '/../helpers/tokenHelpers.php';
    require_once __DIR__ . '/../../html/modals/passwordResetSuccessModal.html';
    require_once __DIR__ . '/../../html/modals/passwordResetTokenExpireModal.html';
    require_once __DIR__ . '/../../html/modals/unknownErrorModal.html';
    
    function handleNewPassword(){
        if (validatePassword($_POST['newPassword'], $_POST['newPasswordConfirm'])){
            $pwdocument = verifyResetPasswordToken($_POST['resetPasswordToken']);
            if ($pwdocument != NULL) {
                $usercollection = (new MongoDB\Client)->eva->users;
                $userdocument = $usercollection->findOne(['mail' => $pwdocument['mail']]);
                if ($userdocument != NULL){
                    # Update Password
                    $updateResult = $usercollection->updateOne(
                        ['mail' => $pwdocument['mail']],
                        ['$set' => ['pwd' => hashPassword($_POST["newPassword"])]]
                    );
                    if ($updateResult->getModifiedCount() != 1){
                        echo '
                        <script type="text/javascript">
                            showModal("#unknownErrorModal");
                        </script>
                        ';
                    } else {
                        echo '
                        <script type="text/javascript">
                            showModal("#passwordResetSuccessModal");
                        </script>
                        ';
                    }
                } else {
                    # No account with the mail address of the password reset token found
                    echo '
                    <script type="text/javascript">
                        showModal("#unknownErrorModal");
                    </script>
                    ';
                }
            } else {
                # Password reset token provided by user is unknown
                echo '
                <script type="text/javascript">
                    showModal("#passwordResetTokenExpireModal");
                </script>
                ';
            }
        }
    }
?>
