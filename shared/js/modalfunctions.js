shakeLoginModal = function () {
    $(document).ready(function(){
        $("#loginModal").modal("show");
        $("#loginModal").addClass("shake");
        $("#loginModalDialog").addClass("shake");
        setTimeout(function() {
            $("#loginModal").removeClass("shake");
            $("#loginModalDialog").removeClass("shake");
        }, 2000);
    });
}

showNewPasswordModal = function (passwordResetToken) {
    $(document).ready(function(){
        $("#resetPasswordToken").val(passwordResetToken);
        $("#newPasswordModal").modal("show");
    });
}

$(document).ready(function(){
    $('#editProfileButton').click(function() {
        $.ajax({
            dataType: "json",
            url: "api/getAccountDetails.php"
        }).done(function (data, textStatus, jqXHR) {
            $("#editProfileTitle").val(jqXHR.responseJSON.title);
            $("#editProfileFirstName").val(jqXHR.responseJSON.first);
            $("#editProfileLastName").val(jqXHR.responseJSON.last);
            $("#editProfileEmail").val(jqXHR.responseJSON.mail);
            $("#editProfileModal").modal("show");
        });
    });
});

showModal = function (modalID) {
    $(document).ready(function(){
        console.log("Showing " + modalID);
        $(modalID).modal("show");
    });
}

passSemesterTypeYear = function(targetButton, semesterType, semesterYear){
    $(document).ready(function(){
        $(targetButton).click(function(){
            $("#deleteSemesterType").val(semesterType);
            $("#deleteSemesterYear").val(semesterYear);
        });
    });
}

passQuestionsetID = function(targetButton, questionsetID){
    $(document).ready(function(){
        $(targetButton).click(function(){
            $("#deleteQuestionsetID").val(questionsetID);
        });
    });
}
