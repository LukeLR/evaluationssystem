getAnswersetsByQuestionsetAndDate = function(level, id, name){
    if (level == 'course'){
        console.log("grouping answersets for course " + id + " by date!");
        questionsetID = "All"; // For container div selection
        var request = $.ajax({
            url: "api/eval/groupAnswersetsByDate.php",
            method: "POST",
            dataType: "json",
            data: {
                "courseID": id
            }
        });
    } else if (level == 'questionset'){
        console.log("grouping answersets for questionset " + id + " by date!");
        var request = $.ajax({
            url: "api/eval/groupAnswersetsByQuestionsetAndDate.php",
            method: "POST",
            dataType: "json",
            data: {
                "questionsetID": id
            }
        });
    }
    
    request.done(function (data, textStatus, jqXHR) {
        series = prepareAnswersetDateDataForHeatmap(jqXHR.responseJSON);
        drawHeatmap("Answer submission times to " + name + ":", '#answersByTime' + questionsetID + 'Chart', 500, {
            yaxis: {
                labels: {
                    minWidth: 100,
                    formatter: function(value){
                        if(value instanceof Date){
                            return value.toLocaleDateString(undefined, {year: 'numeric', month: 'long'});
                        } else {
                            return value;
                        }
                    }
                }
            },
            xaxis: {
                labels: {
                    show: false
                },
                axisTicks: {
                    show: false
                }
            },
            tooltip: {
                x: {
                    show: true,
                    formatter: function(value){
                        if(value instanceof Date){
                            return value.toLocaleDateString(undefined, {year: 'numeric', month: 'long'});
                        } else {
                            try {
                                return new Date(value).toLocaleDateString(undefined, {year: 'numeric', month: 'long', day: 'numeric'});
                            } catch (e) {
                                return value;
                            }
                        }
                    }
                },
                y: {
                    title: {
                        formatter: function(value){
                            if(value instanceof Date){
                                return value.toLocaleDateString(undefined, {year: 'numeric', month: 'long'});
                            } else {
                                try {
                                    return new Date(value).toLocaleDateString(undefined, {year: 'numeric', month: 'long'});
                                } catch (e) {
                                    return value;
                                }
                            }
                        }
                    }
                }
            },
            grid: {
                show: true,
                strokeDashArray: 5,
                position: 'front',
                xaxis: {
                    lines: {
                        show: true,
                        offsetX: 10,
                        offsetY: 10
                    }
                },
                yaxis: {
                    lines: {
                        show: true,
                        offsetX: 10,
                        offsetY: 10
                    }
                }
            },
            colors: ["#008FFB"]
        }, series);
    });
    request.fail(function(jqXHR, textStatus) {
        console.log("Failed to get question: " + textStatus);
    });
}

getEmptyMonthSeries = function(year, month){
    // Generate empty data object
    data = [];
    month = month - 1; // Date month index is 0-based
    /* I want to check if the month of the resulting date object matches
     * the month that has been provided. That way, I can make sure that
     * date objects have only been generated for valid dates, e.g.
     * calling Date.UTC(2019, 11, 31) would result in a date object
     * Date(2019, 12, 01). In that case, the month mismatches, and I can
     * detect that an invalid date has been provided.
     *
     * There are, however, cases, in which this mismatch is desirable:
     * To fill the heatmap for a full year, I am generating empty datasets
     * for the previous and following months. If the last month was month 0
     * (January), the previous month would be -1, which should be mapped to
     * December of the previous year. In that case, both the month and the year
     * will change. Same applies for the following month of month 11 (December).
     * 
     * Therefore, I am checking the month of the date object against a checkmonth
     * variable, that maps all month to the range 0-11.
     *
     * Using modulo is not possible, since JavaScript modulo doesn't map
     * negative values to positive ones.
     */
    checkmonth = month;

    while (checkmonth < 0) checkmonth += 12;
    while (checkmonth > 11) checkmonth -= 12;

    for (day = 1; day <= 31; day++){
        date = Date.UTC(year, month, day);
        dateMonth = new Date(date).getMonth();
        if (dateMonth == checkmonth){
            data.push({
                x: date,
                y: 0
            });
        } else {
            // Date was invalid, do not add
        }
    }
    return data;
}

prepareAnswersetDateDataForHeatmap = function(dataset){
    series = [];

    for(i = 0; i < dataset.length; i++){
        year = dataset[i]._id.year;
        month = dataset[i]._id.month;
        data = getEmptyMonthSeries(year, month);
        for (j = 0; j < dataset[i].days.length; j++){
            data[dataset[i].days[j].day - 1].y = dataset[i].days[j].count; // Array index is 0-based
        }

        series.push({
            name: new Date(year, month - 1), // Month index is 0-based
            data: data
        });
    }

    // Padding with zero data to display a full year
    delta = 12 - series.length;
    for (i = 0; i < delta/2; i++){
        year = series[0].name.getFullYear();
        month = series[0].name.getMonth(); // 0-indexed
        // Expects 1-indexed months, so month is decremented by 1 internally
        data = getEmptyMonthSeries(year, month);
        series.unshift({
            name: new Date(year, month - 1), // Month index is 0-based
            data: data
        });

        year = series[series.length - 1].name.getFullYear();
        month = series[series.length - 1].name.getMonth(); // 0-indexed
        // Expects 1-indexed months, so month is decremented by 1 internally
        data = getEmptyMonthSeries(year, month + 2);
        series.push({
            name: new Date(year, month + 1), // Month index is 0-based
            data: data
        });
    }
    
    return series;
}

drawHeatmap = function(title, containerID, height, options, series){
    options['chart'] = {
        height: height,
        type: 'heatmap'
    };
    options['series'] = series;
    options['title'] = {
        text: title,
        style: {
            fontSize: "20px"
        }
    };

    chart = new ApexCharts(document.querySelector(containerID), options);
    chart.render();
}
