toggleDisplay = function(elementID){
    var element = document.getElementById(elementID);
    if (element.style.display !== 'none') {
        element.style.display = 'none';
    } else {
        element.style.display = 'block';
    }
}

addRotateHandler = function(rotateDiv, cardDiv, frontDiv, backDiv){
    $(document).ready(function(){
        $(rotateDiv).hover(function(){
            $(cardDiv).toggleClass('rotated');
            toggleDisplay(frontDiv);
            toggleDisplay(backDiv);
        });
    });
}

addRotateStopper = function(rotateDiv, stopElement){
    $(document).ready(function(){
        $(stopElement).click(function(){
            $(rotateDiv).off();
        });
    });
}

addRotateHandler('#addCardRotate','#addCard', 'addModuleFront', 'addModuleBack');
