passwordsMatchSignup = function (options, word, score) {
    if ($('#signupPassword').val() == $('#signupPasswordConfirm').val()) {
        return 0;
    } else {
        return score;
    }
}

passwordsMatchNew = function (options, word, score) {
    if ($('#newPassword').val() == $('#newPasswordConfirm').val()) {
        return 0;
    } else {
        return score;
    }
}

passwordsMatchEditProfile = function (options, word, score) {
    if ($('#editProfilePassword').val() == $('#editProfilePasswordConfirm').val()) {
        return 0;
    } else {
        return score;
    }
}

$(document).ready(function () {
    var options = {};
    options.common = {
        //debug: true,
        minChar: 8,
        maxChar: 50,
        onKeyUp: function (evt, data) {
            // Enable or disable submit buttons for respective password fields
            if (data.score >= 15){
                switch(evt.target.id){
                    case "signupPasswordConfirm": signupSubmitButton.disabled = false; break;
                    case "newPasswordConfirm": newPasswordSubmitButton.disabled = false; break;
                    case "editProfilePasswordConfirm": editProfileSubmitButton.disabled = false; break;
                    default: break;
                }
            } else {
                switch(evt.target.id){
                    case "signupPasswordConfirm": signupSubmitButton.disabled = true; break;
                    case "newPasswordConfirm": newPasswordSubmitButton.disabled = true; break;
                    case "editProfilePasswordConfirm": editProfileSubmitButton.disabled = true; break;
                    default: break;
                }
            }
            
            // Refresh "Confirm" password field on first password field change, to update passwords match
            switch(evt.target.id){
                case "signupPassword": $('#signupPasswordConfirm').pwstrength("forceUpdate"); break;
                case "newPassword": $('#newPasswordConfirm').pwstrength("forceUpdate"); break;
                case "editProfilePassword":
                    $('#editProfilePasswordConfirm').pwstrength("forceUpdate");
                    
                    // Enable submit button even if password is unset
                    if ($('#editProfilePassword').val().length == 0 && $('#editProfilePasswordConfirm').val().length == 0){
                        editProfileSubmitButton.disabled = false;
                    }
                    break;
                case "editProfilePasswordConfirm":
                    // Enable submit button even if password is unset
                    if ($('#editProfilePassword').val().length == 0 && $('#editProfilePasswordConfirm').val().length == 0){
                        editProfileSubmitButton.disabled = false;
                    }
                    break;
            }
        }
    };
    
    options.rules = {
        activated: {
            wordTwoCharacterClasses: true,
            wordRepetitions: true,
            wordMaxLength: true
        },
        scores: {
            wordMaxLength: -999999
        }
    };
    
    options.ui = {
        showErrors: true,
        showStatus: true,
        showVerdictsInsideProgressBar: true,
        useVerdictCssClass: true
    };
    
    options.i18n = {
        t: function(key) {
            return i18next.t(key);
        }
    }
    
    $('.passwordNew,.passwordConfirm').pwstrength(options);
    $('#signupPasswordConfirm').pwstrength("addRule", "passwordsMatch", passwordsMatchSignup, -10000, true);
    $('#newPasswordConfirm').pwstrength("addRule", "passwordsMatch", passwordsMatchNew, -10000, true);
    $('#editProfilePasswordConfirm').pwstrength("addRule", "passwordsMatch", passwordsMatchEditProfile, -10000, true);
    $("#editProfilePassword").pwstrength("forceUpdate"); // Automatically enable submit button on load if length is zero
});
