localize = function() {
    $('.translatable').localize(); 
    $('#signupPassword').pwstrength("forceUpdate");
    $('#signupPasswordConfirm').pwstrength("forceUpdate");
    $('.language-button').each(function() {
        if (i18next.language.startsWith($(this).attr("alt"))){
            $(this).addClass("btn-primary");
            $(this).removeClass("btn-secondary");
        } else {
            $(this).addClass("btn-secondary");
            $(this).removeClass("btn-primary");
        }
    });
    $('#signupLanguage').val(i18next.language);
    $('#editProfileLanguage').val(i18next.language);
}

publishLanguageSetting = function(){
    var request = $.ajax({
        url: "api/publishLanguageSettings.php",
        method: "POST",
        data: {
            lang: i18next.language
        }
    });
    request.done(function (data, textStatus, jqXHR) {
        console.log("Published language!");
    });
    request.fail(function(jqXHR, textStatus) {
        console.log("Failed to publish language: " + textStatus);
    });
}

$(document).ready(function() {
    i18next
    .use(i18nextXHRBackend)
    .use(i18nextBrowserLanguageDetector)
    .init({
        debug: true,
        backend: {
            loadPath: 'locales/{{lng}}/{{ns}}.json',
            addPath: 'locales/add/{{lng}}/{{ns}}'
        }
    }, function(err, t) {
        jqi18next = jqueryI18next.init(i18next, $);
        localize();
        $('.language-button').click(function() {
            i18next.changeLanguage(this.firstElementChild.alt).then(function(){
                localize();
                publishLanguageSetting();
            });
        });
    });
});
