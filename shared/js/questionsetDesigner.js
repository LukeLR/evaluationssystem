//TODO: Multiple choice question bar labels: display corresponding text instead of hash

createQuestion = function(mode, questionID, type, questionsetID, title, description, position, additionalData){
    elements = [];
    addElements = [];
    
    if (!position){
        lastPosition = Number($('.formContainer[data-questionsetid='+questionsetID+']').last().attr('data-position'));
        if (isNaN(lastPosition)){
            position = 0;
        } else {
            position = lastPosition + 1;
        }
    }

    formContainer = $('<div class="d-flex formContainer">');
    formContainer.attr('data-position', position);
    formContainer.attr('data-type', type);
    formContainer.attr('data-additionaldata', additionalData);
    
    formGroup = $('<div class="form-group">');
    formGroup.appendTo(formContainer);
    
    elements.push(formContainer, formGroup);

    if (mode == "edit" || mode == "view"){
        // Add MoveButtons
        formGroup.addClass("col-sm-11"); // Reserve space for arrow buttons
        arrowContainer = $('<div class="d-flex flex-column col-sm-1">');
        upButton = $('<button type="button" class="btn btn-secondary mb-2 moveQuestionButton" data-direction="up"><span class="oi oi-arrow-thick-top"></button>');
        upButton.appendTo(arrowContainer);
        downButton = $('<button type="button" class="btn btn-secondary mb-3 moveQuestionButton" data-direction="down"><span class="oi oi-arrow-thick-bottom"></button>');
        downButton.appendTo(arrowContainer);
        arrowContainer.appendTo(formContainer);

        elements.push(arrowContainer, upButton, downButton);
    } else {
        // Make form group full with
        formGroup.addClass("col-sm-12");
    }

    if (mode != 'eval'){
        // Add questionElement
        switch(type){
            case "textfield":
                questionElement = $('<input class="form-control translatable questionElement" type="text" data-i18n="[placeholder]courseDetails.questionsets.elements.textfield">');
                questionElement.attr('name', questionID);
                break;
            case "textarea":
                questionElement = $('<textarea class="form-control translatable questionElement" rows="4" data-i18n="[placeholder]courseDetails.questionsets.elements.textarea">');
                questionElement.attr('name', questionID);
                break;
            case "multiplechoice":
                questionElement = createMultipleChoiceQuestion('radio', questionID, questionsetID, mode, additionalData);
                break;
            case "multipleselect":
                questionElement = createMultipleChoiceQuestion('checkbox', questionID, questionsetID, mode, additionalData);
                break;
            case "slider":
                questionElement = $('<input type="range" class="form-control-range">');
                questionElement.attr('name', questionID);
                break;
            default:
                console.log("type not found: " + type);
                break;
        }

        if (questionElement){
            elements.push(questionElement);
            addElements.push(questionElement);
            questionElement.attr('id', questionID);
        }
    }

    buttonFloat = $('<div class="d-flex justify-content-between">');
    addElements.push(buttonFloat);

    if (mode == "edit" || mode == "view" || mode == "preview") {
        // Add saveQuestionButton
        saveQuestionButton = $('<button type="button" class="btn translatable mt-2 saveQuestionButton">');
        saveQuestionButton.appendTo(buttonFloat);
        elements.push(saveQuestionButton);

        if (mode == "edit") {
            saveQuestionButton.addClass("btn-primary");
            $('<span class="oi oi-file mr-2"></span><span class="translatable" data-i18n="courseDetails.questionsets.question.save"></span>').appendTo(saveQuestionButton);
        }
    
        if (mode == "view" || mode == "preview"){
            saveQuestionButton.addClass("btn-secondary");
            $('<span class="oi oi-pencil mr-2"></span><span class="translatable" data-i18n="courseDetails.questionsets.question.edit"></span>').appendTo(saveQuestionButton);
        }
    }

    if (mode == "edit"){
        // Add editLinksButton
        editLinksButton = $('<a type="button" class="btn btn-warning mt-2 editLinksButton" href="questionDetails.php?id='+questionID+'"><span class="oi oi-pencil mr-2"></span><span class="translatable" data-i18n="courseDetails.questionsets.question.editLinks"></span></button>');
        editLinksButton.appendTo(buttonFloat);
        elements.push(editLinksButton);
    }

    if (mode == "edit" || mode == "view" || mode == "preview"){
        // Add deleteQuestion- or deleteLinkButton
        deleteQuestionButton = $('<button type="button" class="btn btn-danger mt-2 deleteQuestionButton"><span class="oi oi-trash mr-2"></span><span class="translatable" data-i18n="courseDetails.questionsets.question.delete"></span></button>');
        deleteQuestionButton.appendTo(buttonFloat);
        elements.push(deleteQuestionButton);
    } else if (mode == "link") {
        deleteLinkButton = $('<button type="button" class="btn btn-warning mt-2 deleteLinkButton"><span class="oi oi-trash mr-2"></span><span class="translatable" data-i18n="courseDetails.questionsets.question.deleteLink"></span></button>');
        deleteLinkButton.appendTo(buttonFloat);
        elements.push(deleteLinkButton);
    }

    if (mode == "edit"){
        // Add questionset title and description (as text input or text string)
        questionDescription = $('<input class="form-control form-control-sm translatable questionDescription" type="text" data-i18n="[placeholder]courseDetails.questionsets.question.description">');
        questionDescription.val(description);
        elements.push(questionDescription);
        addElements.unshift(questionDescription);
        
        questionTitle = $('<input class="form-control form-control-lg translatable questionTitle" type="text" data-i18n="[placeholder]courseDetails.questionsets.question.title">');
        questionTitle.val(title);
        elements.push(questionTitle);
        addElements.unshift(questionTitle);
    } else if (mode == "view" || mode == "preview" || mode == "link" || mode == "answer" || mode == "eval") {
        questionDescriptionString = $('<p class="form-text text-muted small questionDescriptionString">');
        questionDescriptionString.text(description);
        elements.push(questionDescriptionString);
        addElements.unshift(questionDescriptionString);
        
        questionTitleString = $('<h4 class="questionTitleString">');
        questionTitleString.text(title);
        elements.push(questionTitleString);
        addElements.unshift(questionTitleString);
    }

    // Add attributes to elements
    for (i = 0; i < elements.length; i++){
        elements[i].attr('data-questionid', questionID);
        elements[i].attr('data-questionsetid', questionsetID);
    }
    
    // Add elements to DOM
    for(i = 0; i < addElements.length; i++){
        addElements[i].appendTo(formGroup);
    }

    // Localize
    try {
        formContainer.localize();
    } catch (err) {
        // Wait for jqueryI18Next to be ready
        //TODO: Find a better callback handler.
        setTimeout(formContainer.localize, 200);
    }
    
    return formContainer;
}

createMultipleChoiceQuestion = function(type, questionID, questionsetID, mode, additionalData){
    questionElement = $('<div class="multipleChoiceElement mt-3">');
    
    if (mode == "edit"){
        addMultipleChoiceOptionButton = $('<button type="button" class="btn btn-primary btn-sm col-sm-2 addMultipleChoiceOptionButton">');
        addMultipleChoiceOptionButton.attr("data-type", type);
        addMultipleChoiceOptionButton.append('<span class="oi oi-plus mr-2"></span><span class="translatable" data-i18n="courseDetails.questionsets.multiplechoice.addOption"></span>');
        addMultipleChoiceOptionInput = $('<input class="form-control form-control-sm col-sm-9 translatable mr-2 addMultipleChoiceOptionInput" type="text" data-i18n="[placeholder]courseDetails.questionsets.multiplechoice.optionPlaceholder">');
        questionElement.append($('<div class="form-check">')
            .append('<input class="form-check-input" type="' + type + '" name="addMultipleChoiceOptionRadio" id="addMultipleChoiceOptionRadio" value="addMultipleChoiceOptionRadio">')
            .append($('<div class="d-flex formContainer">').append(addMultipleChoiceOptionInput)
                .append(addMultipleChoiceOptionButton)));
        $.each([addMultipleChoiceOptionButton, addMultipleChoiceOptionInput], function(index, value){
            value.attr("data-questionid", questionID);
            value.attr("data-questionsetid", questionsetID);
        });
    }

    try {
        $.each(JSON.parse(additionalData).options, function(index, value){
            questionElement.append(createMultipleChoiceOption(type, value.text, value.hash, mode, questionID));
        });
    } catch (e) {
        console.log("Couldn't parse additional Data:", additionalData);
    }

    return questionElement;
}

addQuestionClick = function(eventObject){
    questionType = eventObject.target.dataset.questiontype;
    questionsetID = eventObject.target.dataset.questionsetid;
    questionID = "noid";
    
    container = createQuestion("edit", questionID, questionType, questionsetID);
    container.appendTo('#questionset' + questionsetID + 'Form');
    publishQuestion(questionID);
}

publishQuestion = function(questionID){
    container = $('.formContainer[data-questionid='+questionID+']');
    questionsetID = container.attr("data-questionsetid");
    titleField = $('.questionTitle[data-questionid='+questionID+']');
    if (titleField.length > 0){
        questionTitle = titleField.val();
    } else {
        titleString = $('.questionTitleString[data-questionid='+questionID+']');
        questionTitle = titleString.text();
    }

    descriptionField = $('.questionDescription[data-questionid='+questionID+']');
    if (descriptionField.length > 0){
        questionDescription = descriptionField.val();
    } else {
        descriptionString = $('.questionDescriptionString[data-questionid='+questionID+']');
        questionDescription = descriptionString.text();
    }

    type = container.attr("data-type");
    position = container.attr("data-position");
    additionalData = container.attr("data-additionaldata");

    if (additionalData){
        additionalData = JSON.parse(additionalData);
    }
    
    var request = $.ajax({
        url: "api/addQuestion.php",
        method: "POST",
        data: {
            "questionID": questionID,
            "questionsetID": questionsetID,
            "title": questionTitle,
            "description": questionDescription,
            "type": type,
            "position": position,
            "additionalData": additionalData
        }
    });
    request.done(function (data, textStatus, jqXHR) {
        console.log("Published Question: ", data);
        if (questionID == "noid"){
            $("[data-questionid="+questionID+"]").attr("data-questionid", data);
        }
    });
    request.fail(function(jqXHR, textStatus) {
        console.log("Failed to publish question: " + textStatus);
    });
}

moveQuestion = function(eventObject){
    questionID = eventObject.target.dataset.questionid;
    
    switch(eventObject.target.dataset.direction){
        case "up":
            first = $('.formContainer[data-questionid='+questionID+']');
            second = first.prev();
            firstDistance = second.outerHeight();
            secondDistance = first.outerHeight();
            break;
        case "down":
            second = $('.formContainer[data-questionid='+questionID+']');
            first = second.next();
            firstDistance = second.outerHeight();
            secondDistance = first.outerHeight();
    }

    firstPosition = first.attr("data-position");
    first.attr("data-position", second.attr("data-position"));
    second.attr("data-position", firstPosition);

    publishQuestion(first.attr("data-questionid"), null);
    publishQuestion(second.attr("data-questionid"), null);
    
    $.when(
        first.animate({
            top: '-=' + firstDistance
        }, 500),
        second.animate({
            top: '+=' + secondDistance
        }, 500)
    ).done(function(){
        first.css({top: '0'});
        second.css({top: '0'});
        first.insertBefore(second);
    });
}

fetchQuestions = function(eventObject){
    containerString = '#questionset' + eventObject.target.dataset.questionsetid + 'Form';
    if (!$(containerString).length){
        containerString = '#questionset' + eventObject.target.dataset.questionsetid + 'Div';
    }
    
    fetchQuestionset(eventObject.target.dataset.questionsetid, containerString, eventObject.target.dataset.viewmode);
}

fetchQuestionset = function(questionsetID, questionsetForm, viewtype, callback, callbackData){
    console.log("fetching questionset: " + questionsetID + " in mode " + viewtype);
    var request = $.ajax({
        url: "api/getQuestions.php",
        method: "POST",
        dataType: "json",
        data: {
            "questionsetID": questionsetID
        }
    });
    request.done(function (data, textStatus, jqXHR) {
        $.each(jqXHR.responseJSON, function(index, value){
            if(!$('[data-questionid='+value.questionID+']').length > 0){ // Question does not exist in DOM yet
                newQuestionContainer = createQuestion(viewtype, value.questionID, value.type, value.questionsetID, value.title, value.description, value.position, JSON.stringify(value.data));
                newQuestionContainer.appendTo(questionsetForm);
                if (viewtype == "eval"){
                    displayAnswersToQuestion(value.questionID, value.type, questionsetForm);
                }
            }
        });

        if (viewtype == "answer"){
            questionsetSubmitButton = $('<button type="submit" class="float-right btn btn-primary translatable" data-i18n="submit" name="questionsetSubmitButton" id="questionsetSubmitButton">');
            questionsetSubmitButton.appendTo(questionsetForm);
            questionsetIDInput = $('<input type="hidden" name="questionsetID" id="questionsetID">');
            questionsetIDInput.val(questionsetID);
            questionsetIDInput.appendTo(questionsetForm);

            $(document).on('change', '.questionsetForm', function(){
                $(window).on('beforeunload', function(){
                    return 'Any changes will be lost.';
                });
            });
            
            $(document).on('submit', '.questionsetForm', function(){
                $(window).off('beforeunload');
            });
        }

        if (callback) callback(callbackData);
    });
    request.fail(function(jqXHR, textStatus) {
        console.log("Failed to get question: " + textStatus);
    });
}

displayAnswersToQuestion = function(questionID, type, container){
    console.log("Displaying answers to " + type + "-question: " + questionID);
    switch(type){
        case "textfield":
            displayTextAnswers(questionID, container);
            break;
        case "textarea":
            displayTextAnswers(questionID, container);
            break;
        case "multiplechoice":
            displayValueAnswers(questionID, container);
            break;
        case "multipleselect":
            displayValueAnswers(questionID, container);
            //TODO: Evaluate multipleselect questions
            break;
        case "slider":
            //TODO: Evaluate slider questions
            break;
        default:
            break;
    }
}

displayValueAnswers = function(questionID, container){
    console.log("Fetching linked Questions to question: " + questionID);
    /* Creating divs for apexCharts must be outside a callback function.
     * Otherwise all divs for apexCharts will be appended when the call-
     * back completes (e.g. at the very end of the container).
     * 
     * The containers for multipleChoiceQuestion evaluations are created
     * here. This is a quick hack and should be done at a more adequate
     * place (since not all value questions need divs for multiplechoice-
     * questions).
     */
     //TODO: Fix this quick hack.

    chartDiv = $('<div class="row">');
    chartDiv.appendTo(container);

    semesterChartDiv = $('<div class="col-sm-6" id="question' + questionID + 'semesterChart" data-questionid="' + questionID + '">');
    semesterChartDiv.appendTo(chartDiv);

    answerChartDiv = $('<div class="col-sm-6" id="question' + questionID + 'answerChart" data-questionid="' + questionID + '">');
    answerChartDiv.appendTo(chartDiv);
    
    var request = $.ajax({
        url: "api/eval/getLinkedQuestionsToQuestion.php",
        method: "POST",
        dataType: "json",
        data: {
            "questionID": questionID
        }
    });
    request.done(function (data, textStatus, jqXHR) {
        console.log("Linked Question data:", jqXHR.responseJSON);
        semesterSeries = getSemesterSeries(jqXHR.responseJSON);
        console.log("semesterSeries:", semesterSeries);
        displaySemesterSeries(semesterSeries, container, questionID);
    });
    request.fail(function(jqXHR, textStatus) {
        console.log("Failed to get linked questions to question: " + textStatus);
    });
}

displaySemesterSeries = function(semesterSeries, container, questionID){
    console.log("displaying semester series");
    semesterOptions = {
        chart: {
            id: 'question' + questionID + 'semesterBarChart',
            type: 'bar'
        },
        plotOptions: {
            bar: {
                distributed: true,
                horizontal: true,
                endingShape: 'arrow',
                barHeight: '75%',
                dataLabels: {
                    position: 'bottom'
                }
            }
        },
        dataLabels: {
            enabled: true,
            textAnchor: 'start',
            style: {
                colors: ['#fff']
            },
            formatter: function(val, opt){
                return opt.w.globals.labels[opt.dataPointIndex]
            },
            offsetX: 0,
            dropShadow: {
                enabled: true
            }
        },
        series: [{
            data: semesterSeries
        }],
        states: {
            normal: {
                filter: {
                    type: 'desaturate'
                }
            },
            active: {
                allowMultipleDataPointsSelection: true,
                filter: {
                    type: 'darken',
                    value: 1
                }
            }
        },
        tooltip: {
            x: {
                show: false
            },
            y: {
                title: {
                    formatter: function(val, opts) {
                        return opts.w.globals.labels[opts.dataPointIndex]
                    }
                }
            }
        },
        title: {
            text: 'Semester:',
            offsetX: 15
        },
        subtitle: {
            text: '(Click on bar to see details)',
            offsetX: 15
        },
        yaxis: {
            labels: {
                show: false
            }
        },
    }

    semesterChart = new ApexCharts(document.querySelector("#question" + questionID + "semesterChart"), semesterOptions);
    semesterChart.render();

    var answerOptions = {
        chart: {
            id: 'question' + questionID + 'answerBarChart',
            /*height: 400,
            width: '100%',*/
            type: 'bar',
            stacked: true
        },
        plotOptions: {
            bar: {
                columnWidth: '50%',
                horizontal: false
            }
        },
        series: [
            {
                data: []
            }
        ],
        legend: {
            show: false
        },
        grid: {
            yaxis: {
                lines: {
                    show: false,
                }
            },
            xaxis: {
                lines: {
                    show: true,
                }
            }
        },
        yaxis: {
            labels: {
                show: false
            }
        },
        title: {
            text: 'Antworten:',
            offsetX: 10
        },
        tooltip: {
            x: {
                formatter: function(val, opts) {
                    return opts.w.globals.seriesNames[opts.seriesIndex]
                }
            },
            y: {
                title: {
                    formatter: function(val, opts) {
                        return opts.w.globals.labels[opts.dataPointIndex]
                    }
                }
            }
        }
    }
    
    var answerChart = new ApexCharts(document.querySelector("#question" + questionID + "answerChart"), answerOptions);
    answerChart.render();

    semesterChart.addEventListener('dataPointSelection', function (e, chart, opts) {
        console.log(chart.el.dataset.questionid);
        var answerChart = document.querySelector("#question" + chart.el.dataset.questionid + "answerChart");
        var semesterChart = document.querySelector("#question" + chart.el.dataset.questionid + "semesterChart");
        
        if (opts.selectedDataPoints[0].length === 1) {
            if(answerChart.classList.contains("active")) {
                updateSemesterChart(chart, 'question' + questionID + 'answerBarChart')
            } else {
                semesterChart.classList.add("chart-quarter-activated")
                answerChart.classList.add("active");
                updateSemesterChart(chart, 'question' + questionID + 'answerBarChart')
            }
        } else {
            updateSemesterChart(chart, 'question' + questionID + 'answerBarChart')
        }
        
        if (opts.selectedDataPoints[0].length === 0) {
            semesterChart.classList.remove("chart-quarter-activated")
            answerChart.classList.remove("active");
        }
    });
}

updateSemesterChart = function(sourceChart, destChartID) {
    var series = [];
    var seriesIndex = 0;
    //var colors = []
    
    if (sourceChart.w.globals.selectedDataPoints[0]) {
        var selectedPoints = sourceChart.w.globals.selectedDataPoints;
        for (var i = 0; i < selectedPoints[seriesIndex].length; i++) {
            var selectedIndex = selectedPoints[seriesIndex][i];
            var yearSeries = sourceChart.w.config.series[seriesIndex];
            series.push({
                name: yearSeries.data[selectedIndex].x,
                data: yearSeries.data[selectedIndex].answers
            })
            //colors.push(yearSeries.data[selectedIndex].color)
        }
        
        if(series.length === 0){
            series = [{
                data: []
            }];
        }
        
        return ApexCharts.exec(destChartID, 'updateOptions', {
            series: series/*,
            colors: colors,
            fill: {
                colors: colors
            }*/
        })
    }
}

getSemesterSeries = function(data){
    /* Extract answer statistics for each semester a question is linked
     * in from server api and prepare the data for display in a bar
     * chart using displaySemesterSeries
     */
    semesterSeries = [];
    for (i = 0; i < data["linkedQuestions"].length; i++){
        answers = [];
        for (j = 0; j < data["linkedQuestions"][i]["answers"]["answers"].length; j++){
            answers.push({
                x: data["linkedQuestions"][i]["answers"]["answers"][j]["_id"],
                y: data["linkedQuestions"][i]["answers"]["answers"][j]["count"]
            });
        }
        semesterSeries.push({
            x: data["linkedQuestions"][i]["semester"]["type"] + " " + data["linkedQuestions"][i]["semester"]["year"],
            y: data["linkedQuestions"][i]["answers"]["total"],
            answers: answers
        });
    }
    return semesterSeries;
}


displayTextAnswers = function(questionID, container){
    dataElement = $('<table id="' + questionID + 'table" data-toggle="table">');
    /* IMPORTANT: Table needs to be added to DOM before bootstrapTable()
     * is called. Otherwise, the table will display correctly, but will
     * not be interactive (e.g. sorting won't work)
     */
    dataElement.appendTo(container);
    dataElement.bootstrapTable({
        url: 'api/eval/getAnswersToQuestion.php',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        queryParams: function(params){
            params["questionID"] = questionID;
            return params;
        },
        sortable: true,
        sortName: 'timestamp',
        pagination: true,
        pageSize: 5,
        //search: true, // Need to build text index in mongoDB first
        showPaginationSwitch: true,
        showRefresh: true,
        showToggle: true,
        showFullscreen: false,
        pageList: '[5, 10, 15, 20, 25, 50, 100, 200, All]',
        theadClasses: 'thead-light',
        sidePagination: 'server',
        autoRefresh: true,
        autoRefreshInterval: 20,
        clickToSelect: true,
        maintainSelected: true,
        copyBtn: true,
        autoRefreshSilent: true,
        showExport: true,
        //exportDataType: 'all',
        exportTypes: ['json', 'xml', 'png', 'csv', 'txt', 'sql', 'doc', 'excel', 'xlsx', 'pdf'],
        showPrint: true,
        icons: {
            paginationSwitchDown: 'fa-caret-square-down',
            paginationSwitchUp: 'fa-caret-square-up',
            refresh: 'fa-sync',
            toggleOff: 'fa-toggle-off',
            toggleOn: 'fa-toggle-on',
            columns: 'fa-th-list',
            detailOpen: 'fa-plus',
            detailClose: 'fa-minus',
            fullscreen: 'fa-arrows-alt',
            print: 'fa-print',
            export: 'fa-file-export',
            autoRefresh: 'fa-clock'
        },
        columns: [
            {
                checkbox: true
            },
            {
                field: 'timestamp',
                title: 'Timestamp',
                sortable: true,
                formatter: function(value, row, index, field){
                    if (typeof value !== 'undefined'){
                        timestamp = Number(value["$date"]["$numberLong"]);
                        date = new Date(timestamp);
                        return date.toLocaleDateString(undefined, {year: 'numeric', month: 'long', day: 'numeric',  hour: 'numeric', minute: 'numeric', second: 'numeric'});;
                    } else {
                        return "";
                    }
                },
                printFormatter: function(value, row, index, field){
                    if (typeof value !== 'undefined'){
                        timestamp = Number(value["$date"]["$numberLong"]);
                        date = new Date(timestamp);
                        return date.toLocaleDateString(undefined, {year: 'numeric', month: 'long', day: 'numeric',  hour: 'numeric', minute: 'numeric', second: 'numeric'});;
                    } else {
                        return "";
                    }
                }
            },
            {
                field: 'answer',
                title: 'Answer',
                sortable: true,
                cellStyle: function(value, row, index, field){
                    if (value.match(/\r|\n/)){
                        // Contains line wraps. Manual line wrapping.
                        return {
                            classes: 'td-text' // Preserve line wraps
                        };
                    } else {
                        // Contains no line wraps. Automatic line wrapping (default).
                        return {
                            classes: ''
                        };
                    }
                }
            }
        ]
    });
    $('#copyBtn').click(function(){
        $('#copiedToast').toast('show');
    });
    $('.glyphicon').addClass('fas');
    $('.glyphicon').removeClass('glyphicon');
    $('.glyphicon-copy').removeClass('glyphicon-copy');
    $('.icon-pencil').addClass('fa-copy');
    $('.icon-pencil').removeClass('icon-pencil');
    
    $('.btn-default').addClass('btn-secondary');
    $('.btn-default').removeClass('btn-default');
}

fetchAnswerset = function(data){
    console.log("fetching anwers to questionset: " + data.questionsetID);
    var request = $.ajax({
        url: "api/getAnswers.php",
        method: "POST",
        dataType: "json",
        data: {
            "answerToken": data.answerToken,
            "questionsetID": data.questionsetID
        }
    });
    request.done(function (data, textStatus, jqXHR) {
        $.each(jqXHR.responseJSON.answers, function(index, value){
            questionElement = $('#' + value.questionID);
            if (questionElement.hasClass('multipleChoiceElement')){
                $.each(value.answer, function(index, value){
                    questionElement.find('input[value="' + value + '"]').prop('checked', true);
                });
            } else {
                questionElement.val(value.answer);
            }
        });
    });
    request.fail(function(jqXHR, textStatus) {
        console.log("Failed to get answers: " + textStatus);
    });
}

saveQuestion = function(eventObject){
    questionID = eventObject.target.dataset.questionid;
    container = $('.formContainer[data-questionid='+questionID+']');
    type = container.attr("data-type");
    questionsetID = container.attr("data-questionsetid");
    position = container.attr("data-position");
    additionalData = container.attr("data-additionaldata");
    
    questionTitleString = $('.questionTitleString[data-questionid='+questionID+']');
    questionTitle = $('.questionTitle[data-questionid='+questionID+']');
    questionDescriptionString = $('.questionDescriptionString[data-questionid='+questionID+']');
    questionDescription = $('.questionDescription[data-questionid='+questionID+']');
    saveQuestionButton = $('.saveQuestionButton[data-questionid='+questionID+']');

    if(questionTitle.length > 0){
        // We're switching from editing mode to static mode
        if (questionTitle.val() != ""){
            // Only do something if title is present
            newQuestionContainer = createQuestion("view", questionID, type, questionsetID, questionTitle.val(), questionDescription.val(), position, additionalData);
            container.replaceWith(newQuestionContainer);
            publishQuestion(questionID);
        }
    } else {
        // We're switching from static mode to editing mode
        newQuestionContainer = createQuestion("edit", questionID, type, questionsetID, questionTitleString.text(), questionDescriptionString.text(), position, additionalData);
        container.replaceWith(newQuestionContainer);
    }
}

deleteQuestion = function(eventObject){
    questionID = eventObject.target.dataset.questionid;
    console.log("deleting " + questionID);
    var request = $.ajax({
        url: "api/deleteQuestion.php",
        method: "POST",
        data: {
            "questionID": questionID
        }
    });
    request.done(function (data, textStatus, jqXHR) {
        console.log("Deleted question!" + data);
        container = $('.formContainer[data-questionid='+questionID+']');
        distance = container.outerHeight();
        container.hide(500, function(){
            container.addClass('hidden');
        });
    });
    request.fail(function(jqXHR, textStatus) {
        console.log("Failed to delete question: " + textStatus);
    });
}

deleteLink = function(eventObject){
    linkID = eventObject.target.dataset.questionid;
    questionID = findGetParameter("id");
    console.log("deleting " + linkID + " from " + questionID);
    var request = $.ajax({
        url: "api/deleteQuestionLink.php",
        method: "POST",
        data: {
            "questionID": questionID,
            "linkID": linkID
        }
    });
    request.done(function (data, textStatus, jqXHR) {
        console.log("Deleted link " + linkID + " from question" + questionID + ", return: " + data);
        container = $('.formContainer[data-questionid='+linkID+']');
        distance = container.outerHeight();
        container.hide(500, function(){
            container.addClass('hidden');
        });
    });
    request.fail(function(jqXHR, textStatus) {
        console.log("Failed to delete link: " + textStatus);
    });
}

findGetParameter = function(parameterName){
    url = window.location.search.substring(1);
    splitURL = url.split('&');

    for (i = 0; i < splitURL.length; i++){
        parameter = splitURL[i].split('=');

        if (parameter[0] == parameterName){
            if (parameter[1] === undefined){
                return true;
            } else {
                return parameter[1];
            }
        }
    }
}

addMultipleChoiceOption = function(eventObject){
    questionID = eventObject.target.dataset.questionid;
    questionsetID = eventObject.target.dataset.questionsetid;
    type = eventObject.target.dataset.type;
    questionElement = $('.multipleChoiceElement[data-questionid='+questionID+']');
    addMultipleChoiceOptionInput = $('.addMultipleChoiceOptionInput[data-questionid='+questionID+']');
    formContainer = $('.formContainer[data-questionid='+questionID+']');

    text = addMultipleChoiceOptionInput.val()
    md = forge.md.sha256.create();
    md.update(text);
    hash = md.digest().toHex();

    data = formContainer.attr("data-additionaldata");
    try {
        dataJSON = JSON.parse(data);
    } catch (e) {
        dataJSON = {options: []};
    }

    dataJSON.options.push({hash: hash, text: text});
    
    formContainer.attr("data-additionaldata", JSON.stringify(dataJSON));

    questionElement.append(createMultipleChoiceOption(type, text, hash, "edit", questionID));
}

createMultipleChoiceOption = function(type, text, hash, mode, questionID){
    optionDiv = $('<div class="form-check multipleChoiceOptionDiv">');
    optionSelect = $('<input class="form-check-input">');
    optionSelect.attr("type", type);
    optionSelect.attr("value", hash);
    optionSelect.attr("name", questionID + "[]");
    optionSelect.attr("id", hash);
    optionText = $('<label class="form-check-label">' + text + '</label>');
    optionText.attr("for", hash);
    optionDiv.append(optionSelect);
    optionDiv.append(optionText);
    newElements = [optionDiv, optionSelect, optionText];
    if (mode == "edit"){
        optionDelete = $('<a class="float-right badge badge-pill badge-danger deleteMultipleChoiceOptionButton" style="color: white;"></a>');
        trashSpan = $('<span class="oi oi-trash"></span>');
        optionDiv.append(optionDelete);
        optionDelete.append(trashSpan);
        newElements.push(optionDelete, trashSpan);
    }
    $.each(newElements, function(index, value){
        value.attr("data-hash", hash);
        value.attr("data-questionid", questionID);
    });
    return optionDiv;
}

deleteMultipleChoiceOption = function(eventObject){
    hash = eventObject.target.dataset.hash;
    questionID = eventObject.target.dataset.questionid;
    optionDiv = $('.multipleChoiceOptionDiv[data-hash=' + hash + ']');
    optionDiv.detach();

    formContainer = $('.formContainer[data-questionsetid='+questionsetID+']');
    data = formContainer.attr("data-additionaldata");
    if (data){
        dataJSON = JSON.parse(formContainer.attr("data-additionaldata"));
    } else {
        dataJSON = {options: []};
    }

    dataJSON.options = $.grep(dataJSON.options, function(value){
        return value.hash != hash;
    });

    formContainer.attr("data-additionaldata", JSON.stringify(dataJSON));
}

$(document).ready(function(){
    $('.addQuestionButton').click(addQuestionClick);
    $('.questionsetHeaderButton').click(fetchQuestions);
    $(document).on('click', '.moveQuestionButton', moveQuestion);
    $(document).on('click', '.saveQuestionButton', saveQuestion);
    $(document).on('click', '.deleteQuestionButton', deleteQuestion);
    $(document).on('click', '.deleteLinkButton', deleteLink);
    $(document).on('click', '.addMultipleChoiceOptionButton', addMultipleChoiceOption);
    $(document).on('click', '.deleteMultipleChoiceOptionButton', deleteMultipleChoiceOption);
});
