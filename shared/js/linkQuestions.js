loadQuestionsets = function(eventObject){
    var request = $.ajax({
        url: "api/getQuestionsets.php",
        method: "POST",
        dataType: "json",
        data: {
            "courseID": $("#" + eventObject.target.id).children("option:selected").attr("value")
        }
    });
    request.done(function (data, textStatus, jqXHR) {
        questionsetSelect = $('#module' + eventObject.target.dataset.moduleid + "questionsetselect");
        
        defaultOption = questionsetSelect.children().first();
        defaultOption.detach();
        questionsetSelect.empty();
        defaultOption.appendTo(questionsetSelect);
        
        $.each(jqXHR.responseJSON, function(index, value){
            $('<option value="' + value._id + '">' + value.name + '</option>').appendTo(questionsetSelect);
        });

        questionsetSelect.change();
    });
    request.fail(function(jqXHR, textStatus) {
        console.log("Failed to get questionsets: " + textStatus);
    });
}

loadQuestions = function(eventObject){
    var request = $.ajax({
        url: "api/getQuestions.php",
        method: "POST",
        dataType: "json",
        data: {
            "questionsetID": $("#" + eventObject.target.id).children("option:selected").attr("value")
        }
    });
    request.done(function (data, textStatus, jqXHR) {
        questionSelect = $('#module' + eventObject.target.dataset.moduleid + "questionselect");

        defaultOption = questionSelect.children().first();
        defaultOption.detach();
        questionSelect.empty();
        defaultOption.appendTo(questionSelect);
        
        $.each(jqXHR.responseJSON, function(index, value){
            $('<option value="' + value.questionID + '">' + value.title + '</option>').appendTo(questionSelect);
        });
    });
    request.fail(function(jqXHR, textStatus) {
        console.log("Failed to get question: " + textStatus);
    });
}
