#!/bin/bash
# This script creates a visual diff of the TeX file of the thesis based on a git revision

pwd=$(pwd)

rev1=$1
rev2=$2
olddir=/tmp/latexdiff-$rev
newdir=/tmp/latexdiff-$rev2

rm -rf $olddir
rm -rf $newdir

mkdir $olddir
mkdir $newdir
git --work-tree=$olddir checkout $rev1 -- .
git --work-tree=$newdir checkout $rev2 -- .

sed -i -e "s/inputchapter{/input\{chapters\//" $olddir/thesis/thesis.tex
sed -i -e "s/inputchapter{/input\{chapters\//" $olddir/thesis/res/titelmakros.tex
sed -i -e "s/inputchapter{/input\{chapters\//" $newdir/thesis/thesis.tex
sed -i -e "s/inputchapter{/input\{chapters\//" $newdir/thesis/res/titelmakros.tex

cd $olddir/thesis
latexpand $olddir/thesis/thesis.tex -o $olddir/thesis/thesis-expanded.tex

cd $newdir/thesis
latexpand $newdir/thesis/thesis.tex -o $newdir/thesis/thesis-expanded.tex

latexdiff -t UNDERLINE $olddir/thesis/thesis-expanded.tex $newdir/thesis/thesis-expanded.tex > $newdir/thesis/thesis-diff.tex

sed -i -e 's/\\uwave{#1}/#1/' $newdir/thesis/thesis-diff.tex

pdflatex -shell-escape -synctex=1 -interaction=nonstopmode $newdir/thesis/thesis-diff.tex
bibtex $newdir/thesis/thesis-diff.aux
bibtex $newdir/thesis/thesis-diff.aux
bibtex $newdir/thesis/thesis-diff.aux
pdflatex -shell-escape -synctex=1 -interaction=nonstopmode $newdir/thesis/thesis-diff.tex
pdflatex -shell-escape -synctex=1 -interaction=nonstopmode $newdir/thesis/thesis-diff.tex

/usr/bin/evince $newdir/thesis/thesis-diff.pdf
