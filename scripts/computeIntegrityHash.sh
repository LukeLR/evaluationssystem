#!/bin/bash

i=$1;
extension="${i##*.}"
if [ "$extension" = "css" ]; then
    echo -n '<link rel="stylesheet" href="'
    echo -n $i
    echo -n '" integrity="'
    echo -n sha384-
    openssl dgst -sha384 -binary $i | openssl base64 -A
    echo '">'
elif [ "$extension" = "js" ]; then
    echo -n '<script type="text/javascript" src="'
    echo -n $i
    echo -n '" integrity="'
    echo -n sha384-
    openssl dgst -sha384 -binary $i | openssl base64 -A
    echo '"></script>'
fi
