<?php
    require_once __DIR__ . "/../vendor/autoload.php";
    
    printf("Generating demo data for course %s...\n", ($argv[1]));

    $questionsetCollection = (new MongoDB\Client)->eva->questionsets;
    $questionsetCursor = $questionsetCollection->aggregate(
        [
            [
                '$match' => [
                    'courseID' => new MongoDB\BSON\ObjectId($argv[1])
                ]
            ],
            [
                '$lookup' => [
                    'from' => 'questions',
                    'localField' => '_id',
                    'foreignField' => 'questionsetID',
                    'as' => 'questions'
                ]
            ]
        ]
    );

    $answersets = [];
    foreach ($questionsetCursor as $qc){
        printf("QUESTIONSET: %s\n", $qc["name"]);
        for ($i = 0; $i < intval($argv[2]); $i++){
            $answerset = [
                "questionsetID" => $qc["_id"],
                "timestamp" => new MongoDB\BSON\UTCDateTime((time()-rand(0,14*24*3600))*1000)
            ];
            foreach ($qc["questions"] as $question){
                $questionID = $question["_id"]->__toString();
                //printf("%s: %s (%s)\n", $question["type"], $question["title"], $question["_id"]);
                switch($question["type"]){
                    case "slider":
                        $answerset[$questionID] = rand(0, 100);
                        break;
                    case "multiplechoice":
                        $options = $question["data"]["options"];
                        $numOptions = sizeof($options);
                        $answerset[$questionID] = $options[rand(0, $numOptions - 1)]["hash"];
                        break;
                    case "multipleselect":
                        //TODO: Select more options if allowed ("multipleselect"-questions)
                        $options = $question["data"]["options"];
                        $numOptions = sizeof($options);
                        $answerset[$questionID] = $options[rand(0, $numOptions - 1)]["hash"];
                        break;
                    default: break;
                }
            }
            array_push($answersets,$answerset);
        }
    }
    print_r($answersets);
    $answersetCollection = (new MongoDB\Client)->eva->answersets;
    $insertManyResult = $answersetCollection->insertMany($answersets);
    print_r($insertManyResult);
?>
