questionsets = db.getCollection("questionsets");
cursor = questionsets.find({courseID: ObjectId("5c83176862be8344085cdc6f")});

function getRandomInt(min, max){
    return Math.floor(min + Math.random() * (max - min));
}

while(cursor.hasNext()){
    id = cursor.next()._id;
    print(id);

    count = getRandomInt(15, 30);
    print(count);

    for (i = 0; i < count; i++){
        db.answersets.insertOne({questionsetID: id, debug: true});
    }
}
