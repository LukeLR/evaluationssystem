<!DOCTYPE html>
<html lang="de">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <meta charset="UTF-8"/>

    <?php require_once 'shared/php/header.php';?>

    <script type="text/javascript" src="shared/js/cardrotate.js"></script>
    <script type="text/javascript" src="node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" href="node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="shared/css/cardrotate.css">
    
    <?php
        Kopfwelt\i18next::init($_SESSION["lang"], 'locales/__lng__/');
        printf("<title>%s</title>", Kopfwelt\i18next::getTranslation('nav.moduleDetails'));
    ?>
</head>
<body>
    <!-- Navigationsleiste-->
    <?php require_once 'shared/php/navbar.php';?>
    
    <?php
        require_once 'shared/html/modals/deleteModuleModal.html';
        require_once 'shared/html/modals/deleteSemesterModal.html';
        
        $modules = (new MongoDB\Client)->eva->modules;
        $m = $modules->findOne(["_id" => new MongoDB\BSON\ObjectId($_GET["id"]), "readAccess" => $_SESSION["_id"]]);
        Kopfwelt\i18next::init($_SESSION["lang"], 'locales/__lng__/');
        if ($m != NULL){
            $name = $m["name"];
        } else if ($_SESSION["deletedModule"] == $_GET["id"]) {
            $name = Kopfwelt\i18next::getTranslation('moduleDetails.deleted.title');
        } else {
            $name = Kopfwelt\i18next::getTranslation('error');
        }
    ?>
    
    <!--Seiteninhalt-->
    <div class="container">
        <div class="page-header pt-3">
            <h1><span class="translatable mr-3" data-i18n="moduleDetails.title"></span><?php echo $name?></h1>
            <p class="translatable text-muted" data-i18n="moduleDetails.description"></p>
        </div>
        
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="home.php" class="translatable" data-i18n="nav.home"></a></li>
                <li class="breadcrumb-item"><a><span class="translatable mr-1" data-i18n="moduleDetails.title"></span><?php echo $name?></a></li>
            </ol>
        </nav>
        
        <?php
            if ($m != NULL){
                require_once 'shared/php/helpers/databaseHelpers.php';
                
                list ($earliestYear, $latestYear) = getModuleTimespan($m["_id"]);
                $courseCount = getModuleCourseCount($m["_id"], "course");
                $lectureCount = getModuleCourseCount($m["_id"], "lecture");
                $questionsetCount = getModuleQuestionsetCount($m["_id"]);
                $answersetCount = getModuleAnswersetCount($m["_id"]);
                
                printf('
                    <div class="card">
                        <div class="card-body">
                            <a class="full-card" href="moduleDetails.php?id=%s"></a>
                            <h5 class="card-title full-card-title">%s</h5>

                            <p class="text-muted small translatable" data-i18n="modules.module"></p>
                            
                            <p class="card-text"><span class="oi oi-book mr-2"></span>%d <span class="translatable" data-i18n="modules.lecture_s"></span></p>
                            <p class="card-text"><span class="oi oi-laptop mr-2"></span>%d <span class="translatable" data-i18n="modules.course_s"></span></p>
                            <p class="card-text"><span class="oi oi-clipboard mr-2"></span>%d <span class="translatable" data-i18n="modules.questionset_s"></span></p>
                            <p class="card-text"><span class="oi oi-check mr-2"></span>%d <span class="translatable" data-i18n="modules.answerset_s"></span></p>
                            <p class="card-text"><span class="oi oi-calendar mr-2"></span>%d-%d</p>
                        </div>
                    </div>
                ', $m["_id"], $m["name"], $lectureCount, $courseCount, $questionsetCount, $answersetCount, $earliestYear, $latestYear);

                echo '
                    <h2 class="translatable mt-3" data-i18n="moduleDetails.semesters.title"></h2>
                    <p class="translatable text-muted" data-i18n="moduleDetails.semesters.description"></p>
                    
                    <div class="accordion mb-3" id="semesterAccordion">
                        <div class="card">
                            <div class="card-header" id="addSemesterHeading">
                                <h2 class="mb-0">
                                    <button class="btn btn-link translatable" type="button" data-toggle="collapse" data-target="#addSemesterCollapse" aria-expanded="true" aria-controls="addSemesterCollapse" data-i18n="moduleDetails.semesters.addSemester.title"></button>
                                </h2>
                            </div>
                        
                            <div id="addSemesterCollapse" class="collapse show" aria-labelledby="addSemesterHeading" data-parent="#semesterAccordion">
                                <div class="card-body">
                                    <form method="post" name="newSemester">
                                        <p class="small text-muted m-0 translatable" data-i18n="moduleDetails.semesters.addSemester.description"></p>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <select id="addSemesterSemester" class="form-control translatable" name="addSemesterSemester">
                                                    <option value="Winter" selected data-i18n="moduleDetails.semesters.wintersemester"></option>
                                                    <option value="Summer" data-i18n="moduleDetails.semesters.summersemester"></option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <input type="number" class="form-control" data-provide="datepicker" data-date-format="yyyy" id="addSemesterYear" name="addSemesterYear" data-i18n="[placeholder]module.newModule.semester" value="2019">
                                                <script type="text/javascript">
                                                    $("#addSemesterYear").datepicker({
                                                        minViewMode: "decade",
                                                        maxViewMode: "decade"
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                        <div class="float-right">
                                            <button type="submit" id="addSemesterButton" name="addSemesterButton" class="btn btn-primary mb-3"><span class="oi oi-plus mr-2"></span><span class="translatable" data-i18n="create"></span></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        ';

                        foreach ($m["semesters"] as $s){
                            /* Value examples:
                             * Use for database access:
                             * - $s["type"] = ["Winter" | "Summer"]
                             * - $s["year"] = [2015 | 2016 | ...]
                             * Use for HTML IDs:
                             * - $semyear = ["Winter2015" | "Summer2016" | ...]
                             * Use for human readable display:
                             * - $type = ["Winter semester" | "Summer semester" | "Wintersemester" | "Sommersemester" | ...] (Depending on locale)
                             * - $year = ["2015/2016" | "2016/2017" | 2015 | 2016 | ...] (Depending on winter or summer semester)
                             */
                            
                            $semyear = $s["type"] . $s["year"];
                            
                            if ($s["type"] == "Winter"){
                                $type = Kopfwelt\i18next::getTranslation('moduleDetails.semesters.wintersemester');
                                $year = $s["year"] . "/" . (string)($s["year"] + 1);
                            } else {
                                $type = Kopfwelt\i18next::getTranslation('moduleDetails.semesters.summersemester');
                                $year = $s["year"];
                            }
                            
                            printf('
                            <div class="card">
                                <div class="card-header" id="semester%sHeading">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#semester%sCollapse" aria-expanded="false" aria-controls="semester%sCollapse">
                                        %s %s
                                        </button>
                                    </h2>
                                </div>
                                <div id="semester%sCollapse" class="collapse collapsed" aria-labelledby="semester%sHeading" data-parent="#semesterAccordion">
                                    <div class="card-body">
                                        <h5>%s %s</h5>
                                        <p class="translatable text-muted" data-i18n="moduleDetails.semesters.semesterdescription"></p>
            
                                        <div class="card-columns">
                                            <div class="rotate" id="addCourse%sCardRotate">
                                                <div class="card border-bottom" id="addCourse%sCard" style="height: 300px">
                                                    <!-- TODO: Find out why border-bottom is needed (and card doesnt display bottom border on its own), and fix it! -->
                                                    <div class="face front" id="addCourse%sFront">
                                                        <div class="d-flex justify-content-center" style="height: 300px">
                                                            <img src="img/add.svg" alt="add" width="100">
                                                        </div>
                                                    </div>
                                                    <!--class="face back" ...-->
                                                    <div class="back" id="addCourse%sBack" style="display: none">
                                                        <div class="card-body mb-4">
                                                            <h5 class="modal-title translatable" id="addCourse%sLabel" data-i18n="moduleDetails.courses.addCourse.title"></h5>
                                                            <form method="post" name="addCourse">
                                                                <div class="form-row">
                                                                    <div class="form-group col-md-12">
                                                                        <input type="text" class="form-control translatable" id="addCourse%sName" name="addCourseName" data-i18n="[placeholder]moduleDetails.courses.addCourse.name">
                                                                    </div>
                                                                </div>
                                                                <div class="form-row">
                                                                    <select id="addCourse%sCourseType" class="form-control translatable col-md-12 mb-2" name="addCourseCourseType">
                                                                        <option value="lecture" selected data-i18n="modules.lecture"></option>
                                                                        <option value="course" data-i18n="modules.course"></option>
                                                                    </select>
                                                                </div>
                                                                <div class="float-right">
                                                                    <input type="hidden" id="addCourseSemesterType" name="addCourseSemesterType" value="%s">
                                                                    <input type="hidden" id="addCourseSemesterYear" name="addCourseSemesterYear" value="%s">
                                                                    <button type="submit" id="addCourse%sButton" name="addCourseButton" class="btn btn-primary"><span class="oi oi-plus mr-2"></span><span class="translatable" data-i18n="create"></span></button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                                addRotateHandler("#addCourse%sCardRotate", "#addCourse%sCard", "addCourse%sFront", "addCourse%sBack");
                                                addRotateStopper("#addCourse%sCardRotate", "#addCourse%sCourseType");
                                            </script>', $semyear, $semyear, $semyear, $type, $year, $semyear, $semyear, $type, $year, $semyear, $semyear, $semyear, $semyear, $semyear, $semyear, $semyear, $s["type"], $s["year"], $semyear, $semyear, $semyear, $semyear, $semyear, $semyear, $semyear);
            
                            $coursesCollection = (new MongoDB\Client)->eva->courses;
                            $courses = $coursesCollection->find(
                                [
                                    'moduleID' => new MongoDB\BSON\ObjectId($_GET['id']),
                                    'readAccess' => $_SESSION['_id'],
                                    'semester.type' => $s['type'],
                                    'semester.year' => $s['year']
                                ],
                                [
                                    'sort' => [
                                        'name' => 1
                                    ]
                                ]
                            );
            
                            foreach($courses as $c){
                                if ($c["type"] == "lecture"){
                                    $ctype = 'modules.lecture';
                                } else {
                                    $ctype = 'modules.course';
                                }

                                $questionsetCount = getCourseQuestionsetCount($c["_id"]);
                                $answersetCount = getCourseAnswersetCount($c["_id"]);
                                
                                printf('
                                    <div class="card border-bottom">
                                        <!-- TODO: Find out why border-bottom is needed (and card doesnt display bottom border on its own), and fix it! -->
                                        <div class="card-body">
                                            <a class="full-card" href="courseDetails.php?id=%s"></a>
                                            <h5 class="card-title full-card-title">%s</h5>
                                            <p class="card-text small text-muted translatable" data-i18n="%s"></p>
                                            
                                            <p class="card-text"><span class="oi oi-clipboard mr-2"></span>%d <span class="translatable" data-i18n="modules.questionset_s"></span></p>
                                            <p class="card-text"><span class="oi oi-check mr-2"></span>%d <span class="translatable" data-i18n="modules.answerset_s"></span></p>
                                        </div>
                                    </div>
                                ', $c["_id"], $c["name"], $ctype, $questionsetCount, $answersetCount);
                            }
            
                            printf('
                                        </div>
                                        
                                        <div class="float-right">
                                            <button id="deleteSemester%sButton" type="button" class="btn btn-danger mb-3" data-toggle="modal" data-target="#deleteSemesterModal"><span class="oi oi-trash mr-2"></span><span class="translatable" data-i18n="moduleDetails.semesters.delete.button"></span></button>
                                        </div>
                                        <script type="text/javascript">
                                            passSemesterTypeYear("#deleteSemester%sButton", "%s", "%s");
                                        </script>
                                    </div>
                                </div>
                            </div>
                            ', $semyear, $semyear, $s["type"], $s["year"]);
                        }
                        
                        echo '
                        </div>
                        <div class="float-right">
                            <button type="button" class="btn btn-danger mb-3" data-toggle="modal" data-target="#deleteModuleModal"><span class="oi oi-trash mr-2"></span><span class="translatable" data-i18n="moduleDetails.delete.button"></span></button>
                        </div>
                    </div>
                    ';
            } else if ($_SESSION["deletedModule"] == $_GET["id"]) {
                printf('
                    <div class="alert alert-success" role="alert">%s</div>
                ', sprintf(Kopfwelt\i18next::getTranslation('moduleDetails.deleted.text'), $_SESSION["deletedCourseCount"], $_SESSION["deletedQuestionsetCount"], $_SESSION["deletedAnswerTokenCount"]));
            } else {
                echo '
                <div class="alert alert-danger translatable" role="alert" data-i18n="moduleDetails.error.text"></div>
                ';
            }
        ?>
</body>
</html>
